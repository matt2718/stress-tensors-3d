{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DefaultSignatures    #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE DerivingStrategies   #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE MultiWayIf           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.TSig3d
  ( tsBuildChain
  , tsFetchConfig
  , TSig3d(..)
  , Objective(..)
  ) where

import Blocks                         (BlockFetchContext, Coordinate (XT),
                                       CrossingMat, Delta (..), Derivative (..),
                                       TaylorCoeff (..), Taylors, runTagged)
import Blocks.Blocks3d                (ConformalRep (..), Parity (..))
import Blocks.Blocks3d                qualified as B3d
import Blocks.Blocks3d.Build          (block3dBuildLink)
import Blocks.Blocks3d.CompositeBlock (CompositeBlockKey,
                                       compositeBlockBuildLink,
                                       readCompositeBlockTable)
import Bootstrap.Bounds               (BoundDirection (..),
                                       FourPointFunctionTerm, HasBlockParams,
                                       HasConstraintConfig (..), HasRep (..),
                                       OPECoefficient, OPECoefficientExternal,
                                       Spectrum, addOPECoeffExt, boundDirSign,
                                       crossingMatrix, crossingMatrixExternal,
                                       defaultConstraintConfig, mapBlocks)
import Bootstrap.Bounds               qualified as Bounds
import Bootstrap.Build                (BuildChain, FChain (..),
                                       FetchConfig (..), HasForce,
                                       SomeBuildChain (..), SumDropVoid,
                                       addBuildChain, noDeps)
import Bootstrap.Math.FreeVect        (FreeVect)
import Bootstrap.Math.HalfInteger     (HalfInteger)
import Bootstrap.Math.Linear          (toV)
import Bootstrap.Math.VectorSpace     (Tensor, zero, (*^))
import Bootstrap.Math.VectorSpace     qualified as VS
import Control.DeepSeq                (NFData)
import Control.Monad.IO.Class         (MonadIO, liftIO)
import Data.Aeson                     (FromJSON, ToJSON)
import Data.Binary                    (Binary)
import Data.Data                      (Typeable)
import Data.Maybe                     (maybeToList)
import Data.Proxy                     (Proxy (..))
import Data.Reflection                (Reifies, reflect)
import Data.Tagged                    (Tagged)
import GHC.Generics                   (Generic)
import GHC.TypeNats                   (KnownNat)
import Hyperion                       (Dict (..), Job, Static (..))
import Hyperion.Bootstrap.Bound       (BoundConfig, BoundFiles, BoundKeyVals,
                                       SDPFetchBuildConfig (..), ToSDP (..),
                                       blockDir)
import Linear.V                       (V)
import SDPB qualified
import StressTensors3d.CrossingEqs    (crossingEqsStructSet, structsSSSS,
                                       structsTTSS, structsTTTT)
import StressTensors3d.TSigEpsSetup   (ExternalDims (..), ExternalOp (..),
                                       SymmetrySector (..), TSMixedBlock,
                                       TSMixedStruct, TSMixedStructLabel (..),
                                       lambda_B, lambda_F, listSpectrum, opeSS,
                                       opeTS, opeTT, so3, symmetrySectorLabel,
                                       toTSMixedBlock, wardSST)
import StressTensors3d.TTOStructLabel (TTOStructLabel (..))
import StressTensors3d.TTTT3d         (withNonEmptyVec)
import StressTensors3d.Z2Rep          (Z2Rep (..))

data TSig3d = TSig3d
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum SymmetrySector
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  , numPoles     :: Maybe Int
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, HasBlockParams B3d.Block3dParams)

data Objective
  = Feasibility (Maybe (V 2 Rational))
  | StressTensorOPEBound (V 2 Rational) BoundDirection
  | TSigGffNavigator  (Maybe (V 2 Rational))
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance HasConstraintConfig TSigNumCrossing TSig3d where
  type Deriv TSig3d = TaylorCoeff (Derivative 'XT)
  getConstraintConfig = defaultConstraintConfig (Proxy @B3d.Block3dParams) crossingEqs

type TSigNumCrossing = 28

crossingEqs
  :: forall s a b . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V TSigNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqs = crossingEqsStructSet
  [ ((T,T,T,T),         structsTTTT)
  , ((T,T,Sig,Sig),     structsTTSS)
  , ((Sig,T,Sig,T),     structsTTSS)
  , ((Sig,Sig,Sig,Sig), structsSSSS)
  ]

ope
  :: (Fractional a, Eq a, Reifies s TSig3d)
  => ConformalRep Delta
  -> Parity
  -> Z2Rep
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
ope r parity z2 = case z2 of
  Z2Even -> opeTT r parity ++ opeSS Sig r parity
  Z2Odd  -> opeTS Sig r parity

gff
  :: forall a s. (Eq a, Floating a, Reifies s TSig3d)
  => (Tagged s `Tensor` CrossingMat 1 TSigNumCrossing TSMixedBlock) a
gff = VS.sum $
  map (crossingMatTSig3d . toV . uncurry Bounds.scaleOPECoeff) opeData
  where
    dSig = (rep (Sig @s)).delta
    identicalS2 delta' = let delta = fromRational delta' in
      (sqrt (2/3)) * delta * (sqrt ((2 * (delta + 1))/((2*delta + 1))))
    opeData =
      [ (sqrt 2,           Bounds.opeCoeffIdentical_ Sig (ConformalRep (2*dSig) 0)   (so3 0 0))
      , (identicalS2 dSig, Bounds.opeCoeffIdentical_ Sig (ConformalRep (2*dSig+2) 2) (so3 0 2))
      ]

unit
  :: forall s a . (Reifies s TSig3d, Fractional a, Eq a)
  => (Tagged s `Tensor` CrossingMat 1 TSigNumCrossing B3d.IdentityBlock3d) a
unit = Bounds.tagVec $ B3d.identityCrossingMat (crossingEqs @s)

opeExt
  :: forall a s . (Eq a, Fractional a, Reifies s TSig3d)
  => V 2 (OPECoefficientExternal (ExternalOp s) TSMixedStruct a)
opeExt = toV
  ( lambda_B `addOPECoeffExt` wardSST Sig
  , lambda_F `addOPECoeffExt` wardSST Sig
  )

ext
  :: forall a s . (Fractional a, Eq a,  Reifies s TSig3d)
  => (Tagged s `Tensor` CrossingMat 2 TSigNumCrossing TSMixedBlock) a
ext =
  Bounds.tagVec $ mapBlocks toTSMixedBlock $
  crossingMatrixExternal (opeExt @a @s) crossingEqs [T,Sig]

crossingMatTSig3d
  :: forall s j a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) TSMixedStruct a)
  -> (Tagged s `Tensor` CrossingMat j TSigNumCrossing TSMixedBlock) a
crossingMatTSig3d channel =
  Bounds.tagVec $ mapBlocks toTSMixedBlock $
  crossingMatrix channel (crossingEqs @s)

bulk
  :: forall a s m.
     ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m
     , BlockFetchContext TSMixedBlock a m, Reifies s TSig3d)
  => [Tagged s (SDPB.PositiveConstraint m a)]
bulk = do
  let key = reflect @s Proxy
  (delta, range, sym@(SymmetrySector j parity z2)) <- listSpectrum key.spins key.spectrum
  let
    opeCoeffs = ope (ConformalRep delta j) parity z2
    label = symmetrySectorLabel sym
  maybeToList $ withNonEmptyVec opeCoeffs $
    Bounds.constraint range label . crossingMatTSig3d

tSig3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m, BlockFetchContext TSMixedBlock a m)
  => TSig3d
  -> SDPB.SDP m a
tSig3dSDP key = runTagged key $
  case key.objective of
    Feasibility mLambda ->
      Bounds.sdp (zero `asTypeOf` unit) unit (stressConstraint : bulk)
      where
        stressConstraint = case mLambda of
          Just lambda -> Bounds.constConstraint "ext" (Bounds.pair lambda ext)
          Nothing     -> Bounds.constConstraint "ext" ext
    StressTensorOPEBound lambda direction ->
      Bounds.sdp unit lambdaExt bulk
      where
        lambdaExt = boundDirSign direction *^ Bounds.pair lambda ext
    TSigGffNavigator mLambdaTTT ->
      Bounds.sdp unit ((-1) *^ gff) (stressConstraint : bulk)
      where
        stressConstraint = case mLambdaTTT of
          Just lambdaTTT -> Bounds.constConstraint "ext" (Bounds.pair lambdaTTT ext)
          Nothing        -> Bounds.constConstraint "ext" ext

instance ToSDP TSig3d where
  type SDPFetchKeys TSig3d = '[ B3d.BlockTableKey
                              , CompositeBlockKey TTOStructLabel
                              , CompositeBlockKey TSMixedStructLabel ]
  toSDP = tSig3dSDP

tsFetchConfig
  :: (KnownNat p, MonadIO m)
  => BoundFiles
  -> FetchConfig m (BoundKeyVals TSig3d p)
tsFetchConfig boundFiles =
  liftIO . B3d.readBlockTable (blockDir boundFiles) :&:
  readCompositeBlockTable boundFiles.blockDir :&:
  readCompositeBlockTable boundFiles.blockDir :&:
  FetchNil

-- TODO: Right now, the pure Block3d's are computed at the same
-- stage as the CompositeBlocks, before the CompositeBlocks. We might
-- consider changing this: for example, the pure Block3d's could be
-- computed at the earlier stage where the dependencies of the
-- CompositeBlocks are built, or we could do things in a different
-- order.
tsBuildChain
  :: BoundConfig
  -> BoundFiles
  -> SomeBuildChain Job (SumDropVoid (SDPFetchKeys TSig3d))
tsBuildChain bConfig boundFiles =
  SomeBuildChain $
  addBuildChain (>>) b3dChain $
  addBuildChain (>>) ttttBlockChain generalBlockChain
  where
    generalBlockChain :: BuildChain Job '[CompositeBlockKey TSMixedStructLabel, B3d.BlockTableKey]
    generalBlockChain =
      compositeBlockBuildLink boundFiles.blockDir :< b3dChain

    ttttBlockChain :: BuildChain Job '[CompositeBlockKey TTOStructLabel, B3d.BlockTableKey]
    ttttBlockChain =
      compositeBlockBuildLink boundFiles.blockDir :< b3dChain

    b3dChain :: BuildChain Job '[B3d.BlockTableKey]
    b3dChain = noDeps $ block3dBuildLink bConfig boundFiles

instance SDPFetchBuildConfig TSig3d where
  sdpFetchConfig _ _ = tsFetchConfig
  sdpDepBuildChain _ = tsBuildChain

instance Static (Binary TSig3d)              where closureDict = static Dict
instance Static (Show TSig3d)                where closureDict = static Dict
instance Static (ToSDP TSig3d)               where closureDict = static Dict
instance Static (ToJSON TSig3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TSig3d) where closureDict = static Dict
