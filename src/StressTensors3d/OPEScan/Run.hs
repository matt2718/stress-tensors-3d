{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

-- | This file is essentially a copy of
-- Hyperion.Bootstrap.OPESearch.Run, only it is modified to build
-- TSigEps SDPs using the Scheduler. Eventually, we would like to
-- generalize it.

module StressTensors3d.OPEScan.Run where

import Bootstrap.Math.Util                        (dot)
import Control.Concurrent.Memoize                 (memoize)
import Control.Monad                              (when)
import Control.Monad.IO.Class                     (liftIO)
import Data.Aeson                                 (ToJSON)
import Data.Matrix.Static                         (Matrix)
import Data.Ratio                                 (approxRational)
import Data.Set                                   qualified as Set
import Data.Time.Clock                            (UTCTime, addUTCTime,
                                                   getCurrentTime)
import Data.Vector                                (Vector)
import GHC.TypeNats                               (KnownNat, Nat)
import Hyperion                                   (Dict (..), Job)
import Hyperion.Bootstrap.Bound                   (Bound (..),
                                                   BoundFileTreatment (..),
                                                   BoundFiles (..),
                                                   FileTreatment (..),
                                                   Precision, defaultBoundFiles,
                                                   getBoundObject, keepAllFiles,
                                                   reflectBound, reifyPrecision)
import Hyperion.Bootstrap.OPESearch.BilinearForms (BilinearForms (..),
                                                   addBilinearForm,
                                                   feasibleVector,
                                                   setFeasibleVector)
import Hyperion.Bootstrap.OPESearch.Types         (OPESearchConfig (..),
                                                   OPESearchData (..),
                                                   OPESearchResult (..))
import Hyperion.Database                          qualified as DB
import Hyperion.Log                               qualified as Log
import Hyperion.Util                              (hashTruncateFileName, minute,
                                                   sanitizeFileString)
import Linear.V                                   (V)
import SDPB                                       (BigFloat)
import SDPB qualified
import StressTensors3d.Scheduler.TSigEps          (computeBound)
import StressTensors3d.TSigEps3d                  (TSigEps3d)
import System.Directory                           (createDirectoryIfMissing)
import System.FilePath.Posix                      ((</>))

bounds :: DB.KeyValMap (Bound Int b) SDPB.Output
bounds = DB.KeyValMap "computations"

boundOutFiles :: DB.KeyValMap (Bound Int b) FilePath
boundOutFiles = DB.KeyValMap "outFiles"

bilinearFormsMap :: DB.KeyValMap (Bound Int b) (BilinearForms j)
bilinearFormsMap = DB.KeyValMap "bilinearForms"

opeSearchCheckpointMap :: DB.KeyValMap (Bound Int b) (Maybe FilePath)
opeSearchCheckpointMap = DB.KeyValMap "opeSearchCheckpoint"

saveBilinearForms :: (KnownNat j, ToJSON b, KnownNat p) => Bound (Precision p) b -> BilinearForms j -> Job ()
saveBilinearForms bound forms =
  DB.insert bilinearFormsMap (reflectBound bound) forms

saveResult :: forall (p :: Nat) b . (ToJSON b, KnownNat p) => Bound (Precision p) b -> BoundFiles -> SDPB.Output -> Job ()
saveResult bound files result = do
  DB.insert bounds bound' result
  DB.insert boundOutFiles bound' (outDir files)
  where
    bound' = reflectBound @p bound

opeNetSearch
  :: forall j .
     Dict (KnownNat j)
  -> OPESearchConfig TSigEps3d j
  -> [FilePath]
  -> OPESearchData TSigEps3d j
  -> Job (OPESearchResult TSigEps3d j)
opeNetSearch Dict config statFiles searchData = do
  Log.info "OPE search" (bound searchData, workDir searchData)
  endTime <- liftIO $ fmap (addUTCTime (maxDuration searchData)) getCurrentTime

  -- We want to save the path to the new checkpoint file precisely
  -- once, after the first run of SDPB finishes. Thus, we memoize this
  -- call.
  saveCheckpointPath :: FilePath -> Job () <- liftIO $ memoize $
    DB.insert opeSearchCheckpointMap (bound searchData) . Just

  -- It is possible that opeNetSearch could be retried in case of a
  -- RemoteEvalError. Thus, we should check the database in case
  -- initialCheckpoint and initialBilinearForms need to be updated.
  initialCheckpoint' <-
    DB.lookupDefault opeSearchCheckpointMap (initialCheckpoint searchData) (bound searchData)
  initialBilinearForms' <-
    DB.lookupDefault bilinearFormsMap (initialBilinearForms searchData) (bound searchData)

  liftIO $ createDirectoryIfMissing True (workDir searchData)
  let defaultFiles = defaultBoundFiles (workDir searchData)
  reifyPrecision (precision (bound searchData)) $
    runOPESearch endTime saveCheckpointPath defaultFiles statFiles config
    (searchData { initialCheckpoint    = initialCheckpoint'
                , initialBilinearForms = initialBilinearForms'
                })

setEndTime :: UTCTime -> Bound prec b -> Bound prec b
setEndTime t b =
  b { solverParams = (solverParams b) { SDPB.maxRuntime = SDPB.TerminateByTime t } }

setWriteDualVector :: Bound prec b -> Bound prec b
setWriteDualVector b = b
  { solverParams = (solverParams b)
    { SDPB.writeSolution = Set.insert SDPB.DualVector_y (SDPB.writeSolution (solverParams b)) }
  }

runOPESearch
  :: forall p j . (KnownNat j, KnownNat p)
  => UTCTime
  -> (FilePath -> Job ())
  -> BoundFiles
  -> [FilePath]
  -> OPESearchConfig TSigEps3d j
  -> OPESearchData TSigEps3d j
  -> Precision p
  -> Job (OPESearchResult TSigEps3d j)
runOPESearch endTime saveCheckpointPath initialBoundFiles statFiles searchConfig searchData prec = do
  -- Memoize so that we only compute the OPE matrix once, but delay
  -- computing it until after the bound is computed, so that the
  -- blocks will already be ready.
  go Nothing (initialCheckpoint searchData) (initialBilinearForms searchData)
  where
    cftB = (bound searchData) { precision = prec }
    getOpeMatrix = getBoundObject cftB initialBoundFiles (toOPEMatrix searchConfig)
    workCheckpoint = checkpointDir initialBoundFiles
    resolution = realToFrac (bfResolution (initialBilinearForms searchData))

    -- On expanse, removing these files is actualy incredibly slow
    -- (1.5 hours at nmax=22). We are better off doing it another time
    -- (outside of a job). So we just print them to the log file so an
    -- external process can clean them up.
    cleanupBlockAndJsonDirs = do
      -- liftIO $ mapM_ removePathForcibly [initialBoundFiles.blockDir, initialBoundFiles.jsonDir]
      Log.info "Can delete" initialBoundFiles.blockDir
      Log.info "Can delete" initialBoundFiles.jsonDir

    go
      :: Maybe (Matrix j j (Vector (BigFloat p)))
      -> Maybe FilePath
      -> BilinearForms j
      -> Job (OPESearchResult TSigEps3d j)
    go maybeOpeMatrix maybeCheckpoint bilinearForms = case feasibleVector bilinearForms of
      Just lambda -> do
        saveBilinearForms cftB bilinearForms
        searchWithLambda maybeOpeMatrix lambda maybeCheckpoint bilinearForms
      Nothing -> liftIO (queryAllowed searchConfig bilinearForms) >>= \case
        Just lambda -> do
          let newBilinearForms = setFeasibleVector (Just lambda) bilinearForms
          saveBilinearForms cftB newBilinearForms
          searchWithLambda maybeOpeMatrix lambda maybeCheckpoint newBilinearForms
        Nothing     -> do
          Log.text "All lambdas disallowed."
          saveBilinearForms cftB bilinearForms
          cleanupBlockAndJsonDirs
          return (Disallowed bilinearForms maybeCheckpoint)

    searchWithLambda
      :: Maybe (Matrix j j (Vector (BigFloat p)))
      -> V j Rational
      -> Maybe FilePath
      -> BilinearForms j
      -> Job (OPESearchResult TSigEps3d j)
    searchWithLambda maybeOpeMatrix lambda maybeCheckpoint bilinearForms = do
      let cftBLambda =
            setWriteDualVector .
            setEndTime endTime .
            setOPE searchConfig lambda $
            cftB
          lambdaFile = hashTruncateFileName (sanitizeFileString ("lambda=" ++ show lambda))
          files = initialBoundFiles
            { outDir = (workDir searchData) </> lambdaFile ++ "_out"
            , initialCheckpointDir = maybeCheckpoint
            }
      -- Automatically remove the sdp directory when finished
      -- TODO: Right now the reportInterval is 1-minute. Make it
      -- configurable at some point.
      (sdpbOutput, _) <-
        computeBound Dict (1*minute) statFiles
        (keepAllFiles { sdpDirTreatment = RemoveFile })
        Nothing
        Nothing
        cftBLambda
        files
      saveCheckpointPath workCheckpoint
      when (SDPB.isFinished sdpbOutput) (saveResult cftBLambda files sdpbOutput)
      if | SDPB.isPrimalFeasible sdpbOutput -> do
             Log.info "Found allowed lambda" lambda
             -- cleanup blockDir and jsonDir. At this point, the only
             -- remaining files should be outDir and checkpointDir.
             cleanupBlockAndJsonDirs
             return (FoundAllowedPoint bilinearForms (Just workCheckpoint) lambda)
         | SDPB.isDualFeasible sdpbOutput -> do
             alpha <- liftIO $ SDPB.readFunctional (outDir files)
             opeMatrix <- maybe getOpeMatrix pure maybeOpeMatrix
             let m = flip approxRational resolution . dot alpha <$> opeMatrix
             go (Just opeMatrix) (Just workCheckpoint) (addBilinearForm m bilinearForms)
         | SDPB.isUnfinished sdpbOutput -> do
             let currentData = searchData
                   { initialCheckpoint = Just workCheckpoint
                   , initialBilinearForms = bilinearForms
                   }
             return (SearchingPoint currentData)
         | otherwise -> Log.throwError $ "Unexpected SDPB output:" ++ show sdpbOutput
