{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}

module StressTensors3d.Scheduler.RunTasks where

import Control.Concurrent.STM                     (TQueue, TVar, atomically,
                                                   check, flushTQueue,
                                                   modifyTVar, newTQueueIO,
                                                   newTVarIO, readTQueue,
                                                   readTVar, readTVarIO,
                                                   writeTQueue)
import Control.Distributed.Process                (ProcessId, kill, spawnLocal)
import Control.Monad                              (forM_, forever)
import Control.Monad.IO.Class                     (liftIO)
import Control.Monad.Reader                       (ask, asks, lift, local,
                                                   runReaderT)
import Data.Aeson                                 (ToJSON)
import Data.Binary                                (Binary)
import Data.List.NonEmpty                         (NonEmpty (..))
import Data.List.NonEmpty                         qualified as NonEmpty
import Data.Map.Strict                            (Map)
import Data.Map.Strict                            qualified as Map
import Data.Maybe                                 (catMaybes)
import Data.Time.Clock                            (NominalDiffTime, UTCTime,
                                                   addUTCTime, diffUTCTime,
                                                   getCurrentTime)
import Data.Tree                                  (Tree)
import GHC.Generics                               (Generic)
import Hyperion                                   (Closure, Job, Process,
                                                   Static, jobNodeCpus,
                                                   jobWorkerLauncherMap,
                                                   remoteEvalOnWorker,
                                                   setTaskCpus)
import Hyperion                                   qualified as Hyp
import Hyperion.Log                               qualified as Log
import StressTensors3d.Scheduler.NodeStatus       (NodeStatus)
import StressTensors3d.Scheduler.NodeStatus       qualified as NodeStatus
import StressTensors3d.Scheduler.ProgressMap      (ProgressMap)
import StressTensors3d.Scheduler.ProgressMap      qualified as ProgressMap
import StressTensors3d.Scheduler.TaskDistribution (CPUAllocation, Node (..),
                                                   allocateCpusToTasks,
                                                   distributeTasksToNodesWithScores)
import StressTensors3d.Scheduler.TaskGraph        (TaskGraph)
import StressTensors3d.Scheduler.TaskGraph        qualified as TaskGraph
import StressTensors3d.Scheduler.TaskInfo         (HasTaskInfo, NumCPUs,
                                                   RunStage (..), taskMemory, 
                                                   taskMinThreads)
import StressTensors3d.Scheduler.TPrioQueue       (TPrioQueue)
import StressTensors3d.Scheduler.TPrioQueue       qualified as TPrioQueue
import Type.Reflection                            (Typeable)

type Bytes = Int

-- | The return value should be the memory usage in Bytes, if it is
-- known.
class CanRemoteRunTask a where
  remoteRunTask :: Node -> NumCPUs -> a -> Job (Maybe Bytes)

-- | Unit is sometimes useful as a top-level placeholder.
instance CanRemoteRunTask () where
  remoteRunTask _ _ _ = pure Nothing

-- | A helper function for defining instances of
-- CanRemoteRunTask. Sets numCpus and runs the given Closure on the
-- Node.
remoteRunOnNode :: (Typeable a, Static (Binary a)) => Node -> NumCPUs -> Closure (Process a) -> Job a
remoteRunOnNode node numCpus closure =
    local (setTaskCpus (Hyp.NumCPUs numCpus)) $
    remoteEvalOnWorker node.address closure

type TaskPriority = (Int, Int)

-- | Our default choice of priority for tasks. Higher depth means a
-- longer chain of reverse dependencies for this task. After taking
-- depth into account, we prioritize tasks with higher memory
-- requirement.
--
-- TODO: The choice of priority function seems potentially
-- important. Should we instead use some kind of total cost of reverse
-- dependencies?
taskPriority :: (HasTaskInfo a, Ord a) => TaskGraph a -> a -> TaskPriority
taskPriority taskGraph t = (taskGraph.depths Map.! t, taskMemory t)

-- | Handle tasks for a single node. When the NodeStatus in
-- 'nodeStatusVar' changes, dequeue as many tasks as will fit given
-- memory and CPU constraints, and run them on the node, updating
-- NodeStatus to account for the new tasks.
--
-- Should we do this atomically?  Well let's imagine that the
-- NodeStatus changes while we are dequeuing tasks. Then we should
-- just dequeue more tasks. We update the NodeStatus when we start the
-- tasks. So, no. We do not need to do things atomically.
runNodeLoop
  :: forall a . (HasTaskInfo a, Ord a, CanRemoteRunTask a)
  => Node
  -> TVar NodeStatus
  -> TPrioQueue TaskPriority a
  -> TQueue a
  -> TQueue (TaskStat a)
  -> CPUAllocation a
  -> Job ()
runNodeLoop node nodeStatusVar taskQueue finishedTaskQueue taskStatsQueue initialAllocation = do
  runRemoteTasks initialAllocation
  forever getAndRunTasks
  where
    -- Block until the node has free CPUs, then block until the first
    -- element of the taskQueue has small enough memory and minThreads
    -- to fit on the node (because either other tasks have finished,
    -- or other nodes have taken things from the taskQueue). Finally,
    -- return that first element.
    blockUntilNewTask :: Job a
    blockUntilNewTask = liftIO $ atomically $ do
      status <- readTVar nodeStatusVar
      check $ node.cpus > status.cpusInUse
      newTask <- TPrioQueue.read taskQueue
      check $
        node.memory >= taskMemory newTask + status.memoryInUse &&
        node.cpus   >= taskMinThreads InProgressRun newTask + status.cpusInUse
      pure newTask

    -- Remove a task from the taskQueue if it has memory less than
    -- freeMem. Nothing indicates either the taskQueue is empty or the
    -- tasks are too big.
    dequeueTask :: Int -> Int -> Job (Maybe a)
    dequeueTask freeMem freeCpus = liftIO $ atomically $
      TPrioQueue.tryPeek taskQueue >>= \case
      Just t
        | taskMemory t <= freeMem && taskMinThreads InProgressRun t <= freeCpus ->
            Just <$> TPrioQueue.read taskQueue
        | otherwise -> pure Nothing
      Nothing -> pure Nothing

    -- Grow newTasks by repeatedly dequeueing from the taskQueue until
    -- we have no more free cpus or memory. This operation is not
    -- atomic, so it could happen that the NodeStatus is updated by
    -- other threads while 'getMoreTasks' is running. This could
    -- potentially allow us to get even more tasks, so it is a good
    -- thing.
    getMoreTasks :: NonEmpty a -> Job (NonEmpty a)
    getMoreTasks newTasks = do
      status <- liftIO $ readTVarIO nodeStatusVar
      let
        freeMem  = node.memory - status.memoryInUse - sum (fmap taskMemory newTasks)
        freeCpus = node.cpus   - status.cpusInUse   - sum (fmap (taskMinThreads InProgressRun) newTasks)
      dequeueTask freeMem freeCpus >>= \case
        Just t -> getMoreTasks (NonEmpty.cons t newTasks)
        Nothing -> pure newTasks

    -- Block until there are available CPUs and the first task in the
    -- queue can fit in memory. When those conditions are met, dequeue
    -- all the tasks that can fit on the available resources.
    getNewTasks :: Job (NonEmpty a)
    getNewTasks = do
      newTask <- blockUntilNewTask
      getMoreTasks (NonEmpty.singleton newTask)

    runRemoteTasks :: CPUAllocation a -> Job ()
    runRemoteTasks allocation = do
      let allocList = Map.toList allocation
      -- Add all the tasks to NodeStatus
      liftIO $ atomically $ modifyTVar nodeStatusVar $
        \s -> foldr NodeStatus.addTask s allocList
      mapM_ (spawnLocalJob . remoteRunAndUpdateNodeStatus) allocList

    -- Run a RemoteTask and update NodeStatus when the task is
    -- finished.
    remoteRunAndUpdateNodeStatus :: (a, NumCPUs) -> Job ()
    remoteRunAndUpdateNodeStatus t@(task, numCpus) = do
      start <- liftIO getCurrentTime
      mem <- remoteRunTask node numCpus task
      end <- liftIO getCurrentTime
      liftIO $ atomically $ modifyTVar nodeStatusVar $ NodeStatus.removeTask t
      liftIO $ atomically $ writeTQueue finishedTaskQueue task
      liftIO $ atomically $ writeTQueue taskStatsQueue MkTaskStat
        { task        = task
        , taskStart   = start
        , taskRuntime = diffUTCTime end start
        , taskMemory  = mem
        , taskNode    = node
        , taskNumCPUs = numCpus
        }

    getAndRunTasks :: Job ()
    getAndRunTasks = do
      newTasks <- getNewTasks
      status <- liftIO $ readTVarIO nodeStatusVar
      let
        freeCpus = node.cpus - status.cpusInUse
        allocation = allocateCpusToTasks InProgressRun freeCpus (NonEmpty.toList newTasks)
      runRemoteTasks allocation

-- | Continually read tasks from 'finishedTaskQueue' and update the
-- ProgressMap and TaskGraph, returning when all tasks are
-- completed. Report progress whenever an update comes in, provided it
-- has been at least 'reportInterval' since the last progress
-- report. For each task, when all the dependencies of a task have
-- finished, enqueue the task in the 'taskQueue'.
monitorProgressAndDeps
  :: (HasTaskInfo a, Ord a)
  => NominalDiffTime
  -> TPrioQueue TaskPriority a
  -> TQueue a
  -> TaskGraph a
  -> Map Node (TVar NodeStatus)
  -> ProgressMap
  -> Job ()
monitorProgressAndDeps reportInterval taskQueue finishedTaskQueue taskGraph nodeStatusMap initProgressMap = do
  Log.info "Building" (catMaybes (Map.keys initProgressMap))
  report initProgressMap
  start <- liftIO getCurrentTime
  let initDepCounts = TaskGraph.dependencyCounts taskGraph
  go start initDepCounts initProgressMap
  finish <- liftIO getCurrentTime
  Log.info "Finished" ( catMaybes (Map.keys initProgressMap)
                      , diffUTCTime finish start
                      )
  where
    -- TODO: Order the tags appropriately
    -- TODO: Properly indent the progress bars
    report progressMap = do
      ProgressMap.displayLog progressMap
      nodeStatuses <- liftIO $ traverse readTVarIO nodeStatusMap
      Log.text $ "Nodes: \n" <> NodeStatus.display (Map.elems nodeStatuses)

    reportIfAfterInterval lastReportTime progressMap = do
      now <- liftIO getCurrentTime
      if now > addUTCTime reportInterval lastReportTime
        then report progressMap >> pure now
        else pure lastReportTime

    go lastReportTime depCounts progressMap
      | ProgressMap.isFinished progressMap = pure ()
      | otherwise = do
          finishedTask <- liftIO $ atomically $ readTQueue finishedTaskQueue
          let
            progressMap' = ProgressMap.update finishedTask progressMap
            (depCounts', newTasks) = TaskGraph.decrementReverseDependencies taskGraph depCounts finishedTask
          mapM_ (TPrioQueue.write taskQueue) newTasks
          lastReportTime' <- reportIfAfterInterval lastReportTime progressMap'
          go lastReportTime' depCounts' progressMap'

data TaskStat a = MkTaskStat
  { task        :: a
  , taskStart   :: UTCTime
  , taskRuntime :: NominalDiffTime
  , taskMemory  :: Maybe Int
  , taskNode    :: Node
  , taskNumCPUs :: NumCPUs
  } deriving (Eq, Ord, Show, Generic, ToJSON, Functor)

type TaskStats a = [TaskStat a]

-- | Run the given tasks on the given list of nodes by initially
-- distributing largest memory tasks to nodes until we can't fit any
-- more. As tasks finish on a node, we start new tasks on that node,
-- given the newly available memory and cpus.
--
-- TODO: Check that no tasks require more memory than an entire node.
runTasksOnNodes
  :: (HasTaskInfo a, CanRemoteRunTask a, Ord a)
  => NominalDiffTime
  -> [Node]
  -> Tree a
  -> Job (TaskStats a)
runTasksOnNodes reportInterval nodes taskTree = do
  let
    taskGraph = TaskGraph.fromTree taskTree
    (initialAllocs, moreIndependentTasks) =
      --distributeTasksToNodes nodes (TaskGraph.independentKeys taskGraph)
      distributeTasksToNodesWithScores 0.75 nodes (TaskGraph.independentKeys taskGraph)
  taskQueue <- TPrioQueue.new (taskPriority taskGraph)
  finishedTaskQueue <- liftIO $ newTQueueIO
  taskStatsQueue <- liftIO $ newTQueueIO
  mapM_ (TPrioQueue.write taskQueue) moreIndependentTasks
  nodeLoopMap :: Map Node (TVar NodeStatus, ProcessId) <- flip Map.traverseWithKey initialAllocs $
    \node alloc -> do
      nodeStatusVar <- liftIO (newTVarIO NodeStatus.empty)
      loopProcessId <- spawnLocalJob $
        runNodeLoop node nodeStatusVar taskQueue finishedTaskQueue taskStatsQueue alloc
      pure (nodeStatusVar, loopProcessId)
  let
    nodeLoopPidMap = fmap snd nodeLoopMap
    nodeStatusMap  = fmap fst nodeLoopMap
  monitorProgressAndDeps
    reportInterval
    taskQueue
    finishedTaskQueue
    taskGraph
    nodeStatusMap
    (ProgressMap.fromTasksTodo (TaskGraph.keys taskGraph))
  forM_ nodeLoopPidMap $ \pid -> lift $ kill pid "All tasks completed"
  liftIO . atomically $ flushTQueue taskStatsQueue

-- | Run a Job computation in a separate thread.
spawnLocalJob :: Job () -> Job ProcessId
spawnLocalJob j = do
  jobEnv <- ask
  lift $ spawnLocal $ runReaderT j jobEnv

-- | Get all the nodes accessible to a Job
getJobNodes :: Job [Node]
getJobNodes = do
  addrs <- asks (Map.keys . jobWorkerLauncherMap)
  Hyp.NumCPUs cpusPerNode <- asks jobNodeCpus
  -- TODO: Determine this dynamically instead of hard-coding it!
  let memPerNode = 240*1024*1024*1024
  pure $ do
    addr <- addrs
    pure $ MkNode
      { memory  = memPerNode
      , cpus    = cpusPerNode
      , address = addr
      }

runTasks
  :: (HasTaskInfo a, CanRemoteRunTask a, Ord a)
  => NominalDiffTime
  -> Tree a
  -> Job (TaskStats a)
runTasks reportInterval taskTree = do
  nodes <- getJobNodes
  runTasksOnNodes reportInterval nodes taskTree
