{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}

-- | This module contains experimental code for Rajeev's suggestion of
-- computing the critical path, and then adding a CPU to the best job
-- along the critical path, and then repeating.

module StressTensors3d.Scheduler.CriticalPath where

import Control.Monad                              (guard)
import Data.List.Extra                            (maximumOn)
import Data.Map.Strict                            (Map)
import Data.Map.Strict                            qualified as Map
import Data.Maybe                                 (listToMaybe, mapMaybe)
import Data.PQueue.Prio.Max                       (MaxPQueue)
import Data.PQueue.Prio.Max                       qualified as MaxPQueue
import Data.PQueue.Prio.Min                       (MinPQueue)
import Data.PQueue.Prio.Min                       qualified as MinPQueue
import Data.Set                                   qualified as Set
import Data.Time.Clock                            (NominalDiffTime)
import Data.Vector                                qualified as Vector
import Debug.Trace                                qualified as Debug
import StressTensors3d.Scheduler.NodeStatus       (NodeStatus)
import StressTensors3d.Scheduler.NodeStatus       qualified as NodeStatus
import StressTensors3d.Scheduler.RunTasks         (TaskPriority, taskPriority)
import StressTensors3d.Scheduler.TaskDistribution (Node (..))
import StressTensors3d.Scheduler.TaskGraph        (TaskGraph)
import StressTensors3d.Scheduler.TaskGraph        qualified as TaskGraph
import StressTensors3d.Scheduler.TaskInfo         (HasTaskInfo, NumCPUs,
                                                   taskMaxThreads, taskMemory,
                                                   taskMinThreads, taskRuntime, RunStage(..))

data SimulationState a = MkSimulationState
  { time         :: NominalDiffTime
  , readyTasks   :: MaxPQueue TaskPriority (a, NumCPUs)
  , nodeStatuses :: Map Node NodeStatus
  , runningTasks :: MinPQueue NominalDiffTime (TaskStory a, Node)
  , history      :: [TaskStory a]
  , depCounts    :: Map a Int
  }

data TaskStory a = MkTaskStory
  { task        :: a
  , predecessor :: Maybe a
  , numCpus     :: NumCPUs
  , endTime     :: NominalDiffTime
  } deriving (Eq, Ord, Show)

data RunTaskResult a
  = NoMoreTasks
  | NoCandidateNodes
  | RanTask (SimulationState a)

simulateRunTasks
  :: forall a . (HasTaskInfo a, Ord a)
  => [Node]
  -> TaskGraph a
  -> Map a NumCPUs
  -> Maybe [TaskStory a]
simulateRunTasks nodes taskGraph cpuAllocation = go initialState
  where
    insertTasks
      :: [a]
      -> MaxPQueue TaskPriority (a, NumCPUs)
      -> MaxPQueue TaskPriority (a, NumCPUs)
    insertTasks ts queue = foldr insert queue ts
      where
        insert t = MaxPQueue.insert (taskPriority taskGraph t) (t, cpuAllocation Map.! t)

    initialState = MkSimulationState
      { time         = 0
      , readyTasks   = insertTasks (Set.toList (TaskGraph.independentKeys taskGraph)) MaxPQueue.empty
      , nodeStatuses = Map.fromList [(node, NodeStatus.empty) | node <- nodes]
      , runningTasks = MinPQueue.empty
      , history      = []
      , depCounts    = TaskGraph.dependencyCounts taskGraph
      }

    waitForRunningTask :: SimulationState a -> Maybe (SimulationState a)
    waitForRunningTask state = case MinPQueue.minView state.runningTasks of
      Nothing -> Nothing
      Just ((finishedStory, node), runningTasks') -> Just $
        let
          (depCounts', newReadyTasks) =
            TaskGraph.decrementReverseDependencies taskGraph state.depCounts finishedStory.task
        in
          state
          { time         = finishedStory.endTime
          , readyTasks   = insertTasks newReadyTasks state.readyTasks
          , nodeStatuses = Map.adjust (NodeStatus.removeTask (finishedStory.task, finishedStory.numCpus))
                           node state.nodeStatuses
          , history      = finishedStory : state.history
          , runningTasks = runningTasks'
          , depCounts    = depCounts'
          }

    runNewTask :: SimulationState a -> RunTaskResult a
    runNewTask state = case MaxPQueue.maxView state.readyTasks of
      Nothing -> NoMoreTasks
      Just (t@(task, taskCpus), readyTasks') ->
        let
          taskStory = MkTaskStory
            { task        = task
            , predecessor = case state.history of
                []                 -> Nothing
                latestFinished : _ -> Just latestFinished.task
            , numCpus     = taskCpus
            , endTime     = state.time + taskRuntime task taskCpus
            }
          candidateNodes = do
            (node, nodeStatus) <- Map.toList state.nodeStatuses
            guard $
              nodeStatus.cpusInUse   + taskCpus        <= node.cpus &&
              nodeStatus.memoryInUse + taskMemory task <= node.memory
            pure node
        in case candidateNodes of
          [] -> NoCandidateNodes
          node : _ -> RanTask $
            state
            { readyTasks   = readyTasks'
            , nodeStatuses = Map.adjust (NodeStatus.addTask t) node state.nodeStatuses
            , runningTasks = MinPQueue.insert taskStory.endTime (taskStory, node) state.runningTasks
            }

    go :: SimulationState a -> Maybe [TaskStory a]
    go state = case runNewTask state of
      NoMoreTasks -> case waitForRunningTask state of
        Nothing     -> Just state.history
        Just state' -> go state'
      NoCandidateNodes -> case waitForRunningTask state of
        Nothing     -> Nothing
        Just state' -> go state'
      RanTask state' -> go state'

-- | Given a list of stories in reverse order, compute the longest
-- chain of tasks and their predecessors.
criticalPath :: Ord a => [TaskStory a] -> [(a, NumCPUs)]
criticalPath history = go (listToMaybe history) []
  where
    storyMap = Map.fromList [(story.task, story) | story <- history]
    go Nothing acc = acc
    go (Just story) acc =
      go (fmap (storyMap Map.!) story.predecessor) ((story.task, story.numCpus) : acc)

-- | Given a list of tasks, with CPU amounts, find the task such that
-- adding one additional CPU (if it is allowed, given maxThreads)
-- decreases its runtime by the most.
bestTimeDecrease :: HasTaskInfo a => [(a, NumCPUs)] -> Maybe (a, NumCPUs)
bestTimeDecrease alloc = case timeDecreases of
  [] -> Nothing
  _  -> Just $ snd (maximumOn fst timeDecreases)
  where
    timeDecrease (task, numCpus)
      | numCpus >= taskMaxThreads InitialRun task = Nothing
      | otherwise =
        Just (taskRuntime task numCpus - taskRuntime task (numCpus + 1), (task, numCpus + 1))
    timeDecreases = mapMaybe timeDecrease alloc

-- | Add 1 cpu to the task along the critical path with the best time decrease.
refineAllocationHistory
  :: (HasTaskInfo a, Ord a)
  => [Node]
  -> TaskGraph a
  -> (Map a NumCPUs, [TaskStory a])
  -> Maybe (Map a NumCPUs, [TaskStory a])
refineAllocationHistory nodes taskGraph (alloc, history) = do
  (task, newNumCpus) <- bestTimeDecrease (criticalPath history)
  let newAlloc = Map.insert task newNumCpus alloc
  newHistory <- simulateRunTasks nodes taskGraph newAlloc
  pure (newAlloc, newHistory)

makespan :: [TaskStory a] -> NominalDiffTime
makespan []          = error "Empty tasks"
makespan (story : _) = story.endTime

-- | Repeatedly add 1 cpu to the task along the critical path with the
-- best time decrease, keeping track of the allocation with the best
-- makespan. Terminates if we are no longer to actually run jobs due
-- to insufficient resources (for example if a task gets more than 128
-- cores), or if we are no longer able to reduce the length of the
-- critical path because all tasks along it have reached maxThreads.
--
-- TODO: Handle empty [TaskHistory a]
refineAllocationCriticalPath
  :: forall a . (HasTaskInfo a, Ord a)
  => [Node]
  -> TaskGraph a
  -> Map a NumCPUs
  -> Int
  -> ([NominalDiffTime], Maybe (Map a NumCPUs, [TaskStory a]))
refineAllocationCriticalPath nodes taskGraph initialAlloc nSteps =
  case simulateRunTasks nodes taskGraph initialAlloc of
    Nothing -> ([], Nothing)
    Just initialHistory ->
      let initial = (initialAlloc, initialHistory)
      in go [makespan initialHistory] initial initial nSteps
  where
    go makespans best _ 0 = (reverse makespans, Just best)
    go makespans best@(_, bestHistory) current n =
      case refineAllocationHistory nodes taskGraph current of
        Nothing -> (reverse makespans, Just best)
        Just new@(newAlloc, newHistory) ->
          let
            best' = if makespan newHistory < makespan bestHistory
                    then (newAlloc, newHistory)
                    else best
          in
            Debug.traceShow (makespan newHistory) $
            go (makespan newHistory : makespans) best' new (n-1)

minThreadsAlloc :: (HasTaskInfo a, Ord a) => TaskGraph a -> Map a NumCPUs
minThreadsAlloc taskGraph = Map.fromList
  [ (t, taskMinThreads InitialRun t) | t <- Vector.toList taskGraph.tasks ]
