{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE UndecidableInstances  #-}

module StressTensors3d.Scheduler.MonadPathExists where

import Control.Monad.IO.Class  (MonadIO, liftIO)
import Control.Monad.Reader    (Reader, ReaderT, ask, asks, lift, runReader,
                                runReaderT)
import Data.Functor.Identity   (Identity)
import Data.IORef              (IORef, atomicModifyIORef', newIORef, readIORef)
import Data.Map.Strict         (Map)
import Data.Map.Strict         qualified as Map
import System.Directory.OsPath qualified as Directory
import System.OsPath           (OsPath)
import System.OsPath           qualified as OsPath

class Monad m => MonadPathExists m where
  doesPathExist :: OsPath -> m Bool

instance {-# Overlappable #-} (Monad m, MonadIO m) => MonadPathExists m where
  doesPathExist = liftIO . Directory.doesPathExist

instance MonadPathExists Identity where
  doesPathExist = const (pure False)

-- | Helper function for converting a FilePath to an OsPath. Ideally
-- eventually we will convert everything to OsPath's. TODO: in a
-- future version, replace this with OsPath.unsafeEncodeUtf
unsafeEncodeOsPath :: FilePath -> OsPath.OsPath
unsafeEncodeOsPath = maybe (error "Couldn't encode filepath") id . OsPath.encodeUtf

-- | An implementation of MonadPathExists in terms of a pure
-- function. This is for testing purposes.
newtype PurePathExists a = MkPurePathExists (Reader (OsPath -> Bool) a)
  deriving newtype (Functor, Applicative, Monad)

instance MonadPathExists PurePathExists where
  doesPathExist p = MkPurePathExists (asks ($ p))

runPurePathExists :: (OsPath -> Bool) -> PurePathExists a -> a
runPurePathExists f (MkPurePathExists m) = runReader m f

type PathMap = Map OsPath Bool

newtype MemoizedPathExists a = MkMemoizedPathExists (ReaderT (IORef PathMap) IO a)
  deriving newtype (Functor, Applicative, Monad)

instance MonadPathExists MemoizedPathExists where
  doesPathExist path = MkMemoizedPathExists $ do
    pathMapRef <- ask
    pathMap <- lift $ readIORef pathMapRef
    case Map.lookup path pathMap of
      Nothing -> lift $ do
        pathExists <- Directory.doesPathExist path
        atomicModifyIORef' pathMapRef (\m -> (Map.insert path pathExists m, ()))
        pure pathExists
      Just pathExists -> pure pathExists

runMemoizedPathExists :: MonadIO m => MemoizedPathExists a -> m a
runMemoizedPathExists (MkMemoizedPathExists m) = liftIO $ do
  pathMapRef <- newIORef Map.empty
  runReaderT m pathMapRef
