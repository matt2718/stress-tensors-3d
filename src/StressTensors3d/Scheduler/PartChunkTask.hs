{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Scheduler.PartChunkTask where

import Bootstrap.Build                           (SumDropVoid, dependencies)
import Control.DeepSeq                           (NFData)
import Data.Aeson                                (FromJSON, FromJSONKey, ToJSON,
                                                  ToJSONKey)
import Data.Binary                               (Binary)
import Data.ByteString.Char8                     qualified as ByteString
import Data.Set                                  qualified as Set
import Data.Text                                 (Text)
import GHC.Generics                              (Generic)
import GHC.Records                               (HasField)
import GHC.TypeNats                              (KnownNat)
import Hyperion                                  (Static, cAp, cPure,
                                                  closureDict)
import Hyperion.Bootstrap.Bound                  (Bound (..), BoundFiles (..),
                                                  Precision (..),
                                                  SDPFetchBuildConfig,
                                                  SDPFetchKeys, ToSDP,
                                                  boundToSDPDeps,
                                                  reflectPrecision,
                                                  writePartChunk)
import SDPB                                      (Label (..), PartChunk,
                                                  PartType)
import SDPB qualified
import StressTensors3d.Scheduler.MonadPathExists (MonadPathExists (..),
                                                  unsafeEncodeOsPath)
import StressTensors3d.Scheduler.PartTask        (gb)
import StressTensors3d.Scheduler.RunTasks        (CanRemoteRunTask (..),
                                                  remoteRunOnNode)
import StressTensors3d.Scheduler.Task            (HasTaskHash (..),
                                                  afterReturnMemory)
import StressTensors3d.Scheduler.TaskInfo        (HasTaskInfo (..),
                                                  TaskInfo (..))
import StressTensors3d.Scheduler.TaskLink        (TaskLink (..))
import Type.Reflection                           (Typeable, typeOf)

data PartChunkTask p b = MkPartChunkTask
  { bound      :: Bound (Precision p) b
  , boundFiles :: BoundFiles
  , partChunk  :: SDPB.PartChunk
  } deriving (Generic, Binary)

instance (KnownNat p, ToJSON b) => ToJSON (PartChunkTask p b)

-- | We identify a PartChunkTask by the name of the file it will create
instance HasTaskHash (PartChunkTask p b) where
  taskHash task = ByteString.pack $ SDPB.partChunkPath task.boundFiles.jsonDir task.partChunk

instance HasTaskInfo (PartChunkTask p b) where
  taskInfo _ = MkTaskInfo
    { memory     = 1*gb
    , runtime    = const 20
    , maxThreads = const 1
    , minThreads = const 1
    , tag        = Just "SDPB.PartChunk"
    }

instance
  ( Static (ToSDP b)
  , Static (SDPFetchBuildConfig b)
  , Static (Binary b)
  , Typeable b
  , KnownNat p
  ) => CanRemoteRunTask (PartChunkTask p b) where
  remoteRunTask node numCpus task =
    remoteRunOnNode node numCpus $
    afterReturnMemory $
    static writePartChunk
    `cAp` closureDict
    `cAp` cPure task.bound
    `cAp` cPure task.boundFiles
    `cAp` cPure task.partChunk

sdpPartChunkTaskLink
  :: ( ToSDP b
     , SDPFetchBuildConfig b
     , KnownNat p
     , MonadPathExists m
     )
  => Bound (Precision p) b
  -> BoundFiles
  -> TaskLink m SDPB.PartChunk (SumDropVoid (SDPFetchKeys b)) (PartChunkTask p b)
sdpPartChunkTaskLink bound boundFiles = MkTaskLink
  { dependencies = Set.fromList . SDPB.withPartChunk (boundToSDPDeps bound) dependencies
  , checkCreated = doesPathExist . unsafeEncodeOsPath . SDPB.partChunkPath boundFiles.jsonDir
  , toTask       = MkPartChunkTask bound boundFiles
  }

data PartChunkStatKey params = MkPartChunkStatKey
  { partChunk_ty               :: PartType
  , partChunk_label_name       :: Text
  , partChunk_chunkIndex       :: Int
  , bound_precision            :: Int
  , bound_boundKey_blockParams :: params
  , bound_type                 :: String
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey, NFData)

toPartChunkStatKey
  :: (Typeable b, KnownNat p, HasField "blockParams" b params)
  => PartChunkTask p b
  -> PartChunkStatKey params
toPartChunkStatKey task = MkPartChunkStatKey
  { partChunk_ty               = task.partChunk.ty
  , partChunk_label_name       = task.partChunk.label.name
  , partChunk_chunkIndex       = task.partChunk.chunkIndex
  , bound_precision            = reflectPrecision task.bound.precision
  , bound_boundKey_blockParams = task.bound.boundKey.blockParams
  , bound_type                 = show (typeOf task.bound)
  }

