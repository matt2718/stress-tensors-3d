{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Scheduler.Stats where

import Control.DeepSeq                    (NFData)
import Data.Aeson                         (FromJSON, ToJSON)
import Data.List.Extra                    (maximumOn, minimumOn)
import Data.List.NonEmpty                 (NonEmpty, nonEmpty)
import Data.List.NonEmpty                 qualified as NonEmpty
import Data.Map.Strict                    (Map)
import Data.Map.Strict                    qualified as Map
import Data.Time.Clock                    (NominalDiffTime)
import GHC.Generics                       (Generic)
import Prelude                            hiding ((^))
import Prelude qualified
import StressTensors3d.Scheduler.RunTasks (TaskStat (..))
import StressTensors3d.Scheduler.TaskInfo (NumCPUs)

(^) :: Num a => a -> Int -> a
(^) = (Prelude.^)

-- TODO: Maybe we don't need all of these quantities
data Trials a = MkTrials
  { mean      :: a
  , min       :: a
  , max       :: a
  , variance  :: a -- ^ biased variance, i.e. sqrt (<x^2> - <x>^2)
  , numTrials :: Int
  } deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON, NFData)

singleTrial :: Num a => a -> Trials a
singleTrial x = MkTrials x x x 0 1

instance (Floating a, Ord a) => Semigroup (Trials a) where
  t1 <> t2 = MkTrials
    { mean      = (t1.mean*n1 + t2.mean*n2)/n12
    , min       = min t1.min t2.min
    , max       = max t1.max t2.max
    , variance  = sqrt $ (t1.mean-t2.mean)^2*n1*n2/(n12^2) + (n1*t1.variance^2 + n2*t2.variance^2)/n12
    , numTrials = t1.numTrials + t2.numTrials
    }
    where
      n1 = fromIntegral t1.numTrials
      n2 = fromIntegral t2.numTrials
      n12 = n1 + n2

data TaskResources a = MkTaskResources
  { runtime :: a
  , memory  :: a
  } deriving (Eq, Ord, Show, Generic, FromJSON, ToJSON, NFData, Functor)

instance Semigroup a => Semigroup (TaskResources a) where
  MkTaskResources r1 m1 <> MkTaskResources r2 m2 = MkTaskResources (r1<>r2) (m1<>m2)

instance Monoid a => Monoid (TaskResources a) where
  mempty = MkTaskResources mempty mempty

newtype TaskResourceMap = MkTaskResourceMap (Map NumCPUs (TaskResources (Trials Double)))
  deriving stock (Eq, Ord, Show)
  deriving newtype (FromJSON, ToJSON, NFData)

instance Semigroup TaskResourceMap where
  MkTaskResourceMap m1 <> MkTaskResourceMap m2 = MkTaskResourceMap (Map.unionWith (<>) m1 m2)

instance Monoid TaskResourceMap where
  mempty = MkTaskResourceMap Map.empty

statsToResourceMap :: TaskStat a -> TaskResourceMap
statsToResourceMap stats = MkTaskResourceMap $
  Map.singleton stats.taskNumCPUs $ MkTaskResources
  { runtime = singleTrial (realToFrac stats.taskRuntime)
  , memory  = singleTrial (realToFrac (maybe (error "No memory!") id stats.taskMemory))
  }

maxMemory :: TaskResourceMap -> Maybe Int
maxMemory (MkTaskResourceMap m) =
  fmap (round . maximum) $ nonEmpty [r.memory.max | r <- Map.elems m]

-- | Approximates speedup (defined as the un-normalized inverse time)
-- as a function of 'n', given the sample data. If n is to the left of
-- all of the sample points, we do a linear interpolation through the
-- origin and the leftmost point. If n is to the right of all the
-- sample points, we use Amdahl's law with some pre-specified p \in
-- [0,1]. Otherwise, we do linear interpolation in the interval where
-- n lives.
--
-- Edit: Actually, turning off Amdahl's law for now, since it seems to
-- lead to worse builds (expensive block_3d computations get WAY too
-- many cores).
fitSpeedup :: Double -> NonEmpty (NumCPUs, Double) -> NumCPUs -> Double
fitSpeedup _p speedups' = speedup
  where
    speedups = NonEmpty.toList speedups'

    (minN, minS) = minimumOn fst speedups
    (maxN, maxS) = maximumOn fst speedups

    speedup n
      | n <= minN = minS * (fromIntegral n / fromIntegral minN)
      | n >= maxN =
          -- amdahlFit p (maxN, maxS) n
          maxS * (fromIntegral n / fromIntegral maxN)
      -- In this case, there must be at least two data points, with n
      -- lying between them
      | otherwise = go speedups
      where
        go ((n1,s1) : (n2,s2) : ss)
          | n >= n1 && n <= n2 =
            (fromIntegral (n-n1)*s2 + fromIntegral (n2-n)*s1) / fromIntegral (n2 - n1)
          | otherwise = go ((n2,s2) : ss)
        go _ = error $ concat
          [ "Couldn't find a pair of data points that n lies between"
          , "\nspeedups: ", show speedups
          , "\nn: ", show n
          ]

-- | Given a data point (n0,s0), where n0 = numThreads, and s0 is the
-- speedup (inverse time), and the proportion p in amdahl's law, find
-- the predicted speedup with n threads.
amdahlFit :: Double -> (NumCPUs, Double) -> NumCPUs -> Double
amdahlFit p (n0, s0) n = (1 - p + p/fromIntegral n0) / (1 - p + p/fromIntegral n) * s0

-- | Here, p is the parameter in Amdahl's law.
fitRuntime
  :: Double
  -> NonEmpty (NumCPUs, TaskResources (Trials Double))
  -> NumCPUs
  -> NominalDiffTime
fitRuntime p points = runtime
  where
    speedups = fmap (\(n,r) -> (n, 1 / r.runtime.mean)) points
    speedup = fitSpeedup p speedups
    runtime n = realToFrac (1 / speedup n)

-- | Here, p is the parameter in Amdahl's law, used when the number of
-- threads is larger than any given data points.
approxRuntime :: Double -> TaskResourceMap -> Maybe (NumCPUs -> NominalDiffTime)
approxRuntime p (MkTaskResourceMap m) = fmap (fitRuntime p) $ nonEmpty (Map.toList m)
