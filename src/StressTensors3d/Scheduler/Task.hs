{-# LANGUAGE DefaultSignatures   #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE StaticPointers      #-}

module StressTensors3d.Scheduler.Task
  ( Task
  , mkTask
  , mkTaskWithInfo
  , mkTaskWithStats
  , HasTaskHash(..)
  , matchTask
  , DummyTask(..)
  , unDummyTask
  , afterReturnMemory
  ) where

import Control.Concurrent                 (threadDelay)
import Control.Monad.IO.Class             (MonadIO, liftIO)
import Data.Aeson                         (ToJSON (..))
import Data.Binary                        (Binary)
import Data.BinaryHash                    (hashBase64SafeByteString)
import Data.ByteString                    (ByteString)
import Data.ByteString.Char8              qualified as ByteString
import Data.Map.Strict                    (Map)
import Data.Map.Strict                    qualified as Map
import Data.Maybe                         (fromMaybe)
import Data.Text                          qualified as Text
import GHC.Generics                       (Generic)
import Hyperion                           (Closure, Process, cAp)
import Hyperion.Util                      (nominalDiffTimeToMicroseconds)
import StressTensors3d.Scheduler.RunTasks (Bytes, CanRemoteRunTask (..))
import StressTensors3d.Scheduler.Stats    (TaskResourceMap, approxRuntime,
                                           maxMemory)
import StressTensors3d.Scheduler.TaskInfo (HasTaskInfo (..), TaskInfo (..),
                                           taskMemory, taskRuntime)
import System.RUsage                      qualified as RUsage
import Type.Reflection                    (Typeable, eqTypeRep, typeOf, typeRep,
                                           (:~~:) (HRefl))

-- | A general container for an instance of HasTaskInfo and
-- CanRemoteRunTask. We include a ByteString hash for quick
-- comparisons, so that data structures like Set's and Map's are
-- reasonably performant.
--
-- WARNING: If two different tasks ever have the same hash, this could
-- lead to undefined behavior.
data Task where
  MkTask :: (Typeable a, CanRemoteRunTask a, ToJSON a) => ByteString -> TaskInfo -> a -> Task

instance Eq Task where
  MkTask h1 _ _ == MkTask h2 _ _ = h1 == h2

instance Ord Task where
  compare (MkTask h1 _ _) (MkTask h2 _ _) = compare h1 h2

instance Show Task where
  show (MkTask h i _) = concat
    [ "MkTask "
    , case i.tag of
        Nothing -> "Nothing"
        Just t' -> Text.unpack t'
    , " "
    , show (i.runtime 1)
    , " \""
    , ByteString.unpack h
    , "\""
    ]

instance ToJSON Task where
  toJSON (MkTask _ _ t) = toJSON t

instance HasTaskInfo Task where
  taskInfo (MkTask _ i _) = i

instance CanRemoteRunTask Task where
  remoteRunTask node numCpus (MkTask _ _ t) = remoteRunTask node numCpus t

class HasTaskHash a where
  taskHash :: a -> ByteString
  default taskHash :: (Typeable a, Binary a) => a -> ByteString
  taskHash = hashBase64SafeByteString

instance HasTaskHash ()

-- | A smart constructor for a Task.
mkTask :: (HasTaskInfo a, CanRemoteRunTask a, Typeable a, HasTaskHash a, ToJSON a) => a -> Task
mkTask t = mkTaskWithInfo (taskInfo t) t

-- | A smart constructor for a Task.
mkTaskWithInfo :: (CanRemoteRunTask a, Typeable a, HasTaskHash a, ToJSON a) => TaskInfo -> a -> Task
mkTaskWithInfo i t = MkTask (taskHash t) i t

-- | Helper function to match a Task and extract the value if the type
-- matches.
matchTask :: forall a. Typeable a => Task -> Maybe a
matchTask (MkTask _ _ val) =
  case eqTypeRep (typeRep @a) (typeOf val) of
    Just HRefl -> Just val
    Nothing    -> Nothing

afterReturnMemoryM :: MonadIO m => m () -> m (Maybe Bytes)
afterReturnMemoryM go = do
  go
  let kilobytesToBytes k = 1024 * fromIntegral k
  rSelf     <- liftIO $ RUsage.get RUsage.Self
  rChildren <- liftIO $ RUsage.get RUsage.Children
  pure $ Just $ kilobytesToBytes $ max rSelf.maxResidentSetSize rChildren.maxResidentSetSize

afterReturnMemory :: Closure (Process ()) -> Closure (Process (Maybe Bytes))
afterReturnMemory = cAp (static afterReturnMemoryM)

-- | Create a task whose memory and runtime are estimted with the
-- given 'TaskResourceMap's.
--
-- Right now, we assume Amdahl's law fit to blocks_3d
-- performance. TODO: Make configurable!
mkTaskWithStats
  :: (HasTaskInfo a, CanRemoteRunTask a, Typeable a, HasTaskHash a, ToJSON a, Ord k)
  => (a -> k)
  -> Map k TaskResourceMap
  -> a
  -> Task
mkTaskWithStats toKey statsMap task =
  let
    maybeTaskResourceMap = Map.lookup (toKey task) statsMap
    p = 0.971371 -- Amdahl's law for blocks_3d
    runtime = fromMaybe (taskRuntime task) (maybeTaskResourceMap >>= approxRuntime p)
    memory  = fromMaybe (taskMemory task)  (maybeTaskResourceMap >>= maxMemory)
    info = (taskInfo task)
      { runtime = runtime
      , memory  = memory
      }
  in
    mkTaskWithInfo info task

-- | A task with a trivial 'CanRemoteRunTask' instance that just does
-- a threadDelay, sped up by the given Int factor. This is for testing
-- purposes.
data DummyTask a = MkDummyTask Int a
  deriving (Eq, Ord, Show, Generic, ToJSON)

unDummyTask :: DummyTask a -> a
unDummyTask (MkDummyTask _ x) = x

instance HasTaskHash a => HasTaskHash (DummyTask a) where
  taskHash = taskHash . unDummyTask

instance HasTaskInfo a => HasTaskInfo (DummyTask a) where
  taskInfo = taskInfo . unDummyTask

instance HasTaskInfo a => CanRemoteRunTask (DummyTask a) where
  remoteRunTask _ numCpus (MkDummyTask speedup t) = do
    liftIO $ threadDelay (nominalDiffTimeToMicroseconds (taskRuntime t numCpus) `div` speedup)
    pure (Just (taskMemory t))

