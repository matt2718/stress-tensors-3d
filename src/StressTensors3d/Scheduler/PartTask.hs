{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Scheduler.PartTask where

import Control.DeepSeq                           (NFData)
import Data.Aeson                                (FromJSON, FromJSONKey, ToJSON,
                                                  ToJSONKey)
import Data.Binary                               (Binary)
import Data.ByteString.Char8                     qualified as ByteString
import Data.Set                                  qualified as Set
import Data.Text                                 (Text)
import GHC.Generics                              (Generic)
import GHC.Records                               (HasField)
import GHC.TypeNats                              (KnownNat)
import Hyperion                                  (Static, cAp, cPure,
                                                  closureDict)
import Hyperion.Bootstrap.Bound                  (Bound (..), BoundFiles (..),
                                                  Precision (..), ToSDP,
                                                  boundToSDPDeps,
                                                  reflectPrecision, writePart)
import SDPB                                      (Label (..), Part, PartType)
import SDPB qualified
import StressTensors3d.Scheduler.MonadPathExists (MonadPathExists (..),
                                                  unsafeEncodeOsPath)
import StressTensors3d.Scheduler.RunTasks        (Bytes, CanRemoteRunTask (..),
                                                  remoteRunOnNode)
import StressTensors3d.Scheduler.Task            (HasTaskHash (..),
                                                  afterReturnMemory)
import StressTensors3d.Scheduler.TaskInfo        (HasTaskInfo (..),
                                                  TaskInfo (..))
import StressTensors3d.Scheduler.TaskLink        (TaskLink (..))
import Type.Reflection                           (Typeable, typeOf)

gb :: Bytes
gb = 1024 * 1024 * 1024

data PartTask p b = MkPartTask
  { bound      :: Bound (Precision p) b
  , boundFiles :: BoundFiles
  , part       :: SDPB.Part
  } deriving (Generic, Binary)

instance (KnownNat p, ToJSON b) => ToJSON (PartTask p b)

-- | We identify a PartTask by the name of the file it will create
instance HasTaskHash (PartTask p b) where
  taskHash task = ByteString.pack $ SDPB.partPath task.boundFiles.jsonDir task.part

instance HasTaskInfo (PartTask p b) where
  taskInfo _ = MkTaskInfo
    { memory     = 16*gb
    , runtime    = const 20
    , maxThreads = const 1
    , minThreads = const 1
    , tag        = Just "SDPB.Part"
    }

instance
  ( Static (ToSDP b)
  , Static (Binary b)
  , Typeable b
  , KnownNat p
  ) => CanRemoteRunTask (PartTask p b) where
  remoteRunTask node numCpus task =
    remoteRunOnNode node numCpus $
    afterReturnMemory $
    static writePart
    `cAp` closureDict
    `cAp` cPure task.bound
    `cAp` cPure task.boundFiles
    `cAp` cPure task.part

sdpPartTaskLink
  :: ( ToSDP b
     , KnownNat p
     , MonadPathExists m
     )
  => Bound (Precision p) b
  -> BoundFiles
  -> TaskLink m SDPB.Part SDPB.PartChunk (PartTask p b)
sdpPartTaskLink bound boundFiles = MkTaskLink
  { dependencies = Set.fromList . SDPB.partDependencies (boundToSDPDeps bound)
  , checkCreated = doesPathExist . unsafeEncodeOsPath . SDPB.partPath boundFiles.jsonDir
  , toTask       = MkPartTask bound boundFiles
  }

data PartStatKey params = MkPartStatKey
  { part_ty                    :: PartType
  , part_label_name            :: Text
  , bound_precision            :: Int
  , bound_boundKey_blockParams :: params
  , bound_type                 :: String
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey, NFData)

toPartStatKey
  :: (Typeable b, KnownNat p, HasField "blockParams" b params)
  => PartTask p b
  -> PartStatKey params
toPartStatKey task = MkPartStatKey
  { part_ty                    = task.part.ty
  , part_label_name            = task.part.label.name
  , bound_precision            = reflectPrecision task.bound.precision
  , bound_boundKey_blockParams = task.bound.boundKey.blockParams
  , bound_type                 = show (typeOf task.bound)
  }

