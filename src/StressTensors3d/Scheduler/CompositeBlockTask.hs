{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE UndecidableInstances  #-}

module StressTensors3d.Scheduler.CompositeBlockTask where

import Blocks                                    (Block (..), Coordinate)
import Blocks.Blocks3d                           (Block3dParams,
                                                  ConformalRep (..), Q4Struct)
import Blocks.Blocks3d                           qualified as B3d
import Blocks.Blocks3d.CompositeBlock            (CompositeBlockKey (..),
                                                  ToSO3Structs,
                                                  compositeBlockDependencies,
                                                  compositeBlockMemoryEstimate,
                                                  writeCompositeBlockTable)
import Blocks.Blocks3d.ThreePtStruct             (ThreePtStruct (..))
import Control.DeepSeq                           (NFData)
import Data.Aeson                                (FromJSON, FromJSONKey, ToJSON,
                                                  ToJSONKey)
import Data.Binary                               (Binary)
import Data.ByteString.Char8                     qualified as ByteString
import Data.Set                                  qualified as Set
import GHC.Generics                              (Generic)
import Hyperion                                  (Static, cAp, cPure,
                                                  closureDict)
import Hyperion.Util.ToPath                      (ToPath (..))
import StressTensors3d.Scheduler.MonadPathExists (MonadPathExists (..),
                                                  unsafeEncodeOsPath)
import StressTensors3d.Scheduler.RunTasks        (CanRemoteRunTask (..),
                                                  remoteRunOnNode)
import StressTensors3d.Scheduler.Task            (HasTaskHash (..),
                                                  afterReturnMemory)
import StressTensors3d.Scheduler.TaskInfo        (HasTaskInfo (..),
                                                  TaskInfo (..))
import StressTensors3d.Scheduler.TaskLink        (TaskLink (..))
import Type.Reflection                           (Typeable)

data CompositeBlockTask t = MkCompositeBlockTask
  { key      :: CompositeBlockKey t
  , blockDir :: FilePath
  } deriving (Generic, Binary, ToJSON)

instance ToPath (CompositeBlockKey t) => HasTaskHash (CompositeBlockTask t) where
  taskHash task = ByteString.pack $ toPath task.blockDir task.key

instance ToSO3Structs t => HasTaskInfo (CompositeBlockTask t) where
  taskInfo task = MkTaskInfo
    { memory     = compositeBlockMemoryEstimate task.key
    , runtime    = const 100
    , maxThreads = const 1
    , minThreads = const 1
    , tag        = Just "CompositeBlock"
    }

instance
  ( Typeable t
  , Static (Binary t)
  , Static (ToSO3Structs t)
  , Static (Show t)
  , Static (ToPath (CompositeBlockKey t))
  ) => CanRemoteRunTask (CompositeBlockTask t) where
  remoteRunTask node numCpus task =
    remoteRunOnNode node numCpus $
    afterReturnMemory $
    static writeCompositeBlockTable
    `cAp` closureDict
    `cAp` cPure task.blockDir
    `cAp` cPure task.key

compositeBlockTaskLink
  :: (MonadPathExists m, ToSO3Structs t , ToPath (CompositeBlockKey t))
  => FilePath
  -> TaskLink m (CompositeBlockKey t) B3d.BlockTableKey (CompositeBlockTask t)
compositeBlockTaskLink blockTableDir = MkTaskLink
  { dependencies = Set.fromList . compositeBlockDependencies
  , checkCreated = doesPathExist . unsafeEncodeOsPath . toPath blockTableDir
  , toTask       = \c -> MkCompositeBlockTask c blockTableDir
  }

data CompositeBlockStatKey t = MkCompositeBlockStatKey
  { struct12_rep1    :: ConformalRep ()
  , struct12_rep2    :: ConformalRep ()
  , struct12_rep3    :: ConformalRep ()
  , struct12_label   :: t
  , struct43_rep1    :: ConformalRep ()
  , struct43_rep2    :: ConformalRep ()
  , struct43_rep3    :: ConformalRep ()
  , struct43_label   :: t
  , fourPtFunctional :: Q4Struct
  , coordinate       :: Coordinate
  , params           :: Block3dParams
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey, NFData)

voidRep :: ConformalRep a -> ConformalRep ()
voidRep r = r { delta = () }

toCompositeBlockStatKey :: CompositeBlockTask t -> CompositeBlockStatKey t
toCompositeBlockStatKey task = MkCompositeBlockStatKey
  { struct12_rep1    = voidRep task.key.block.struct12.rep1
  , struct12_rep2    = voidRep task.key.block.struct12.rep2
  , struct12_rep3    = task.key.block.struct12.rep3
  , struct12_label   = task.key.block.struct12.label
  , struct43_rep1    = voidRep task.key.block.struct43.rep1
  , struct43_rep2    = voidRep task.key.block.struct43.rep2
  , struct43_rep3    = task.key.block.struct43.rep3
  , struct43_label   = task.key.block.struct43.label
  , fourPtFunctional = task.key.block.fourPtFunctional
  , coordinate       = task.key.coordinate
  , params           = task.key.params
  }
