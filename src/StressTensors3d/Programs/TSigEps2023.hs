{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PatternSynonyms     #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE StaticPointers      #-}

module StressTensors3d.Programs.TSigEps2023 where

import Blocks                                      (Delta (..))
import Blocks.Blocks3d                             qualified as B3d
import Bootstrap.Bounds                            (BoundDirection (..))
import Bootstrap.Bounds.Spectrum                   qualified as Spectrum
import Bootstrap.Math.AffineTransform              qualified as Affine
import Bootstrap.Math.Linear                       (fromV, pattern V2,
                                                    pattern V6, snoc, toV)
import Bootstrap.Math.Util                         (dot)
import Bootstrap.Math.VectorSpace                  ((*^))
import Control.Exception                           (handle)
import Control.Monad                               (foldM, void)
import Control.Monad.IO.Class                      (liftIO)
import Control.Monad.Reader                        (asks, local)
import Data.Aeson                                  (ToJSON)
import Data.Binary                                 (Binary)
import Data.List.NonEmpty                          (NonEmpty)
import Data.Map                                    qualified as Map
import Data.Matrix.Static                          (Matrix)
import Data.Scientific                             (Scientific)
import Data.Scientific                             qualified as Scientific
import Data.Text                                   (Text)
import Data.Time.Clock                             (NominalDiffTime)
import GHC.Generics                                (Generic)
import GHC.TypeNats                                (KnownNat)
import Hyperion
import Hyperion.Bootstrap.BinarySearch             (BinarySearchConfig (..),
                                                    Bracket (..))
import Hyperion.Bootstrap.Bound                    (Bound (..), CheckpointMap,
                                                    LambdaMap, Precision (..))
import Hyperion.Bootstrap.Bound                    qualified as Bound
import Hyperion.Bootstrap.DelaunaySearch           (DelaunayConfig,
                                                    delaunaySearchRegionPersistent)
import Hyperion.Bootstrap.Main                     (unknownProgram)
import Hyperion.Bootstrap.OPESearch                (BilinearForms (..),
                                                    HessianLineSearchError,
                                                    OPESearchConfig (..))
import Hyperion.Bootstrap.OPESearch                qualified as OPE
import Hyperion.Bootstrap.Params                   qualified as Params
import Hyperion.Bootstrap.SDPDeriv                 qualified as SDPDeriv
import Hyperion.Bootstrap.SDPDeriv.BFGS            qualified as BFGS
import Hyperion.Database                           qualified as DB
import Hyperion.Log                                qualified as Log
import Hyperion.Util                               (hour, minute)
import Linear.V                                    (V)
import Numeric.Rounded                             (Rounded, RoundingMode (..))
import SDPB qualified
import StressTensors3d.Programs.Defaults           (defaultBoundConfig,
                                                    defaultDelaunayConfig,
                                                    defaultQuadraticNetConfig)
import StressTensors3d.Programs.SingleNodeScan     qualified as SNS
import StressTensors3d.Programs.TSig2023           (z2EvenParityEvenScalar,
                                                    z2EvenParityOddScalar,
                                                    z2OddParityEvenScalar)
import StressTensors3d.Programs.TSigEps2023Data    (allowedPointsNmax10,
                                                    allowedPointsNmax14,
                                                    allowedPointsNmax6,
                                                    disallowedPointsNmax10,
                                                    initialDisallowedPtsNmax10,
                                                    initialDisallowedPtsNmax14,
                                                    isingAffineNmax6,
                                                    isingSigEpsAffineNmax22,
                                                    nmax14AllowedPoint,
                                                    opeEllipseEvenTNmax6,
                                                    opeEllipseNmax10,
                                                    opeEllipseNmax14)
import StressTensors3d.Programs.TSigEps2023DataMSM qualified as MSM
import StressTensors3d.Scheduler.TSigEps           (buildBounds)
import StressTensors3d.TSigEps3d                   (TSigEps3d (..))
import StressTensors3d.TSigEps3d                   qualified as TSigEps3d
import StressTensors3d.TSigEpsSetup                (ExternalDims (..))
import StressTensors3d.TSigEpsSetup                qualified as TSigEpsSetup
import StressTensors3d.Z2Rep                       (Z2Rep (..))

checkBoundOutput :: Cluster (SDPB.Output) -> Cluster Bool
checkBoundOutput = fmap ((==SDPB.PrimalFeasibleJumpDetected) . SDPB.terminateReason)

z2EvenParityEvenSpin2 :: TSigEpsSetup.SymmetrySector
z2EvenParityEvenSpin2 = TSigEpsSetup.SymmetrySector 2 B3d.ParityEven Z2Even

deltaVToExternalDims :: V 2 Rational -> ExternalDims
deltaVToExternalDims (V2 deltaSig deltaEps) =
  ExternalDims { deltaSig = deltaSig, deltaEps = deltaEps }

externalDimsToDeltaV :: ExternalDims -> V 2 Rational
externalDimsToDeltaV dims = V2 dims.deltaSig dims.deltaEps

boundDeltaV :: Bound prec TSigEps3d -> V 2 Rational
boundDeltaV bound = externalDimsToDeltaV bound.boundKey.externalDims

boundDeltaVDO :: Bound prec TSigEps3d -> V 3 Rational
boundDeltaVDO bound =
  boundDeltaV bound `snoc`
  case Spectrum.deltaGap bound.boundKey.spectrum z2EvenParityOddScalar of
    Fixed r -> r
    _       -> undefined

boundDeltaOOIso :: Bound prec TSigEps3d -> V 2 Rational
boundDeltaOOIso bound = toV (dOdd, dOddPrime)
  where
    dOddPrime = case Spectrum.deltaGap bound.boundKey.spectrum z2EvenParityOddScalar of
      Fixed r -> r
      _       -> undefined
    dOdd = case foldr1 const (Spectrum.deltaIsolated bound.boundKey.spectrum z2EvenParityOddScalar) of
      Fixed r -> r
      _       -> undefined

tSigEpsNoGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsNoGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  , numPoles     = Nothing
  }

tSigEpsFeasibleDefaultGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsFeasibleDefaultGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  , numPoles     = Nothing
  }

tSigEpsFeasibleOddGaps :: V 3 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsFeasibleOddGaps deltaExtOddGap mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, dOdd )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  , numPoles     = Nothing
  }
  where
    deltaExt = toV (dSig, dEps)
    (dSig, dEps, dOdd) = fromV deltaExtOddGap

tSigEpsFeasibleOddGapsIso :: Rational -> V 3 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsFeasibleOddGapsIso oddGap deltaExtOddGap mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, oddGap )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.addIsolated z2EvenParityOddScalar dOdd
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  , numPoles     = Nothing
  }
  where
    deltaExt = toV (dSig, dEps)
    (dSig, dEps, dOdd) = fromV deltaExtOddGap

tSigEpsNavigatorDefaultGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsNavigatorDefaultGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.NavigatorBound mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  , numPoles     = Nothing
  }

remoteTSigEpsOPESearch
  :: CheckpointMap TSigEps3d
  -> LambdaMap TSigEps3d (V 5 Rational)
  -> BilinearForms 5
  -> Bound Int TSigEps3d
  -> Cluster Bool
remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms =
  OPE.remoteOPESearch (static hackOPESearchCfg) checkpointMap lambdaMap initialBilinearForms

-- It seems like quadratic net is causing some kind of crash. Turn it off for now
-- OPE.queryAllowedMixed qmConfig
hackOPESearchCfg :: OPESearchConfig TSigEps3d 5
hackOPESearchCfg = OPESearchConfig setLambda TSigEps3d.getMatExt $
  handle queryErrorHandler . -- to deal with UnexpectedBoolFunction
  OPE.queryAllowedDescent
  qmConfig.qmDescentConfig
  qmConfig.qmPrecision
  qmConfig.qmResolution
  qmConfig.qmHessianLineSteps
  qmConfig.qmHessianLineAverage
  where
    queryErrorHandler :: HessianLineSearchError -> IO (Maybe a)
    queryErrorHandler ex = do
      Log.warn "OPE search query caught exception" ex
      Log.warn "returning" ("Nothing" :: String)
      return Nothing
    setLambda lambda bound = bound
      { boundKey = bound.boundKey
        { TSigEps3d.objective = TSigEps3d.Feasibility (Just lambda) }
      }
    qmConfig = OPE.QueryMixedConfig
      { OPE.qmQuadraticNetConfig = defaultQuadraticNetConfig
      , OPE.qmDescentConfig      = OPE.defaultDescentConfig
        { OPE.maxDescentRuns = 10 }
      , OPE.qmResolution         = 1e-32
      , OPE.qmPrecision          = 384
      , OPE.qmHessianLineSteps   = 200
      , OPE.qmHessianLineAverage = 10
      }

data TSigEps3dBinarySearch = TSigEps3dBinarySearch
  { tse_bs_bound    :: Bound Int TSigEps3d
  , tse_bs_config   :: BinarySearchConfig Rational
  , tse_bs_dualPt   :: V 2 Rational
  , tse_bs_primalPt :: V 2 Rational
  } deriving (Show, Generic, Binary, ToJSON)

remoteTSigEps3dBinarySearch :: TSigEps3dBinarySearch -> Cluster (Bracket Rational)
remoteTSigEps3dBinarySearch tse_bs = Bound.remoteBinarySearchBoundWithHotStarting Bound.MkBinarySearchBound
  { Bound.bsBoundClosure      = static mkBound `cAp` cPure tse_bs.tse_bs_bound `cAp` cPure tse_bs.tse_bs_dualPt `cAp` cPure tse_bs.tse_bs_primalPt
  , Bound.bsConfig            = tse_bs.tse_bs_config
  , Bound.bsResultBoolClosure = static getBool
  }
  where
    mkBound bound dual primal fraction =
      bound { boundKey = (boundKey bound) { externalDims = deltaVToExternalDims $ (1-fraction) *^ primal + fraction *^ dual } }
    getBool _ _ = pure <$> SDPB.isPrimalFeasible

remoteGapBinarySearchSameCheckpoint :: TSigEps3dBinarySearch -> Cluster (Bracket Rational)
remoteGapBinarySearchSameCheckpoint tse_bs =
  Bound.remoteBinarySearchBoundWithFileTreatmentAndHotStarting
  Bound.HotStarting
  Bound.keepOutAndCheckpoint
  Bound.defaultBoundFiles -- (addCk . Bound.defaultBoundFiles)
  Bound.MkBinarySearchBound
  { Bound.bsBoundClosure      = static mkBound `cAp` cPure tse_bs.tse_bs_bound `cAp` cPure tse_bs.tse_bs_dualPt `cAp` cPure tse_bs.tse_bs_primalPt
  , Bound.bsConfig            = tse_bs.tse_bs_config
  , Bound.bsResultBoolClosure = static getBool
  }
  where
    mkBound bound dual primal fraction = bound {
      boundKey = (boundKey bound)
        { spectrum = let (dOdd,dOddGap) = fromV ((1-fraction) *^ primal + fraction *^ dual) in
                       Spectrum.setGaps [ (z2EvenParityEvenScalar, 3 ) , (z2OddParityEvenScalar, 3 )
                                        , (z2EvenParityOddScalar, dOddGap ), (z2EvenParityEvenSpin2, 4) ]
                       $ Spectrum.addIsolated z2EvenParityOddScalar dOdd
                       $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
        }
      }
    getBool _ _ = pure <$> SDPB.isPrimalFeasible
    --addCk bfiles = bfiles { Bound.initialCheckpointDir = initCk }

-- for testing purposes
myMkBound :: (V 2 Rational, V 5 Rational) -> Rational -> Rational -> Bound Int TSigEps3d
myMkBound (dExt, fixedLambda) dgap iso =
  Bound
  { boundKey =
      (tSigEpsFeasibleOddGapsIso dgap (toV (dSig,dEps, iso)) (Just fixedLambda) paramNmax)
      { blockParams  = (Params.block3dParamsNmax paramNmax)
                       { B3d.precision = 960, B3d.nmax = nmax, B3d.keptPoleOrder = 32, B3d.order = 80 }
      }
  , precision = (Params.block3dParamsNmax nmax).precision
  , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
  , boundConfig = defaultBoundConfig
  }
  where
    prec = 960
    nmax = 14
    paramNmax = 18
    (dSig, dEps) = fromV dExt

-- | These functions compute binary searches for sequentially increasing gaps, each of which
-- intelligently incorporates the result of the previous binary search.
-- They work, but are a mess and need to be refactored.
remoteComputeBSShrinking
  :: Bound.BoundFileTreatment
  -> (FilePath -> Bound.BoundFiles)
  -> Maybe FilePath
  -> Rational
  -> (V 2 Rational, V 5 Rational)
  -> [(Rational, (Rational, Rational))]
  -> Cluster (Bound.BoundFiles)
remoteComputeBSShrinking treatment mkFiles initCk tolerance extData bracketList = do
  let (gap0, (iso0a, _)) = head bracketList
      bound0 = myMkBound extData gap0 iso0a
  workDir <- newWorkDir bound0
  let boundFiles = mkFiles workDir
  remoteEvalJob $ static computeBSShrinking
    `cAp` cPure treatment
    `cAp` cPure initCk
    `cAp` cPure tolerance
    `cAp` cPure extData
    `cAp` cPure bracketList
    `cAp` cPure boundFiles
  --need to clean since we're modifying the treatment in job
  Log.info "Cleaning files" boundFiles
  Bound.cleanFilesWithTreatment treatment boundFiles
  return boundFiles

computeBSShrinking
  :: Bound.BoundFileTreatment
  -> Maybe FilePath
  -> Rational
  -> (V 2 Rational, V 5 Rational)
  -> [(Rational, (Rational, Rational))]
  -> Bound.BoundFiles
  -> Job ()
computeBSShrinking treatment initCk tolerance extData bracketList files =
  void $ foldM binSearchWrapper (True, snd $ head bracketList) bracketList
  where
    mkBound = myMkBound extData
    treatment' = treatment { Bound.blockDirTreatment = Bound.KeepFile
                           , Bound.jsonDirTreatment  = Bound.KeepFile }
    files' = files { Bound.initialCheckpointDir = initCk }

    compSingle changeCk bound = do
      solverParams' <- liftIO $ Bound.setFixedTimeLimit (22.5*hour) (solverParams bound)
      let filesForThis = if changeCk then files' else files
      let bound' = bound { solverParams = solverParams' }
      -- FOR NORMAL COMPUTATION:
      result <- Bound.computeWithFileTreatment treatment' bound' filesForThis
      DB.insert (DB.KeyValMap "computations") bound result
      -- FOR OPE SEARCH:
      -- let opeConfig = hackOPESearchCfg
      -- progInfo <- asks jobProgramInfo
      -- let searchData = OPE.OPESearchData -- HARD CODED; change as needed
      --       bound'
      --       ((reverse . drop 4 . reverse) (Bound.sdpDir filesForThis))
      --       (Bound.initialCheckpointDir filesForThis)
      --       (BilinearForms 1e-32 [ (Nothing, opeEllipseNmax14) ]) -- changed from larger (see Mathematica)
      --       progInfo
      --       (22.5*hour)
      -- endTime <- liftIO $ fmap (addUTCTime (OPE.maxDuration searchData)) getCurrentTime
      -- let saveCheckpointPath = const (return ())
      -- -- :: FilePath -> Job () <- liftIO $ memoize $
      -- --   DB.insert opeSearchCheckpointMap (bound searchData) . Just
      -- -- initialCheckpoint' <-
      -- --   DB.lookupDefault opeSearchCheckpointMap (initialCheckpoint searchData) (bound searchData)
      -- -- initialBilinearForms' <-
      -- --   DB.lookupDefault bilinearFormsMap (initialBilinearForms searchData) (bound searchData)
      -- -- result will be saved in computations for us
      -- reifyPrecision (precision bound') $ OPE.runOPESearch endTime saveCheckpointPath filesForThis opeConfig searchData
      -- result <- DB.lookupDefault (DB.KeyValMap "computations") undefined bound'
      --  -- (searchData { initialCheckpoint    = initialCheckpoint'
      --  --             , initialBilinearForms = initialBilinearForms'
      --  --             })
      -- FOR MOCK
      --result <- mockResult bound' changeCk
      --DB.insert (DB.KeyValMap "computations") bound result
      return result

    checkResult :: SDPB.Output -> a -> a -> a
    checkResult r xp xd =
      case SDPB.terminateReason r of
        SDPB.PrimalFeasibleJumpDetected -> xp
        SDPB.DualFeasibleJumpDetected   -> xd
        _                               -> error "SDPB returned something weird"

    doBinSearch :: Rational -> (Rational, Rational) -> Job (Rational, Rational)
    doBinSearch gap (aLast, dLast) =
      if abs (aLast - dLast) <= tolerance
      then return (aLast, dLast)
      else do
        let mid = (aLast + dLast) / 2
        midResult <- compSingle False (mkBound gap mid)
        doBinSearch gap $ checkResult midResult (mid, dLast) (aLast, mid)

    binSearchWrapper :: (Bool, (Rational, Rational)) -> (Rational,(Rational, Rational))
                     -> Job (Bool, (Rational, Rational))
    binSearchWrapper (changeCk, (aLast, dLast)) (gap, (aCur, dCur)) = do
      let dCur' = (if aCur < dCur then min else max) dLast dCur
      let mid = (aCur + dCur') / 2
      let mid' =
            if aCur < dCur then
              (if aLast < dCur' then max mid (dCur' + 4*(aLast - dCur')) else mid)
            else
              (if aLast > dCur' then min mid (dCur' + 4*(aLast - dCur')) else mid)
      midResult <- compSingle changeCk (mkBound gap mid')
      bFinal <- doBinSearch gap $ checkResult midResult (mid',dCur') (aCur, mid')
      return (False, bFinal)

getExtMat :: Bound Int TSigEps3d -> Bound.BoundFiles -> Job (Matrix 5 5 Scientific)
getExtMat bound' files =
  Bound.reifyBound bound' $ \(bound :: Bound (Bound.Precision p) TSigEps3d) -> do
  opeMatrix <- Bound.getBoundObject bound files TSigEps3d.getMatExt
  alpha <- liftIO $ SDPB.readFunctional @(Bound.BigFloat p) files.outDir
  pure $
    fmap Scientific.fromFloatDigits $
    fmap (alpha `dot`) opeMatrix

logExtMatLocal :: Bound Int TSigEps3d -> Bound.BoundFiles -> Cluster ()
logExtMatLocal bound files = do
  progInfo <- asks clusterProgramInfo
  staticConfig <- asks clusterStaticConfig
  extMat <- liftIO $ runJobLocal staticConfig progInfo $ getExtMat bound files
  Log.info "External matrix" extMat

remoteBuildBoundsWithScheduler
  :: KnownNat p
  => NominalDiffTime
  -> [FilePath]
  -> NonEmpty (Bound (Precision p) TSigEps3d)
  -> Cluster FilePath
remoteBuildBoundsWithScheduler reportInterval statFiles bounds = do
  workDir <- newWorkDir bounds
  remoteEvalJob $
    static buildBoundsWithScheduler
    `cAp` closureDict
    `cAp` cPure workDir
    `cAp` cPure reportInterval
    `cAp` cPure statFiles
    `cAp` cPure bounds

buildBoundsWithScheduler
  :: forall p . Dict (KnownNat p)
  -> FilePath
  -> NominalDiffTime
  -> [FilePath]
  -> NonEmpty (Bound (Precision p) TSigEps3d)
  -> Job FilePath
buildBoundsWithScheduler Dict workDir reportInterval statFiles bounds =
  buildBounds reportInterval statFiles $ do
  bound <- bounds
  pure (bound, boundFiles)
  where
    boundFiles = Bound.defaultBoundFiles workDir

--coerceBase :: DR.DampedRational base1 f a -> DR.DampedRational base2 f a
--coerceBase DR.DampedRational{..} = DR.DampedRational{..}
--
--getEpsFunctional :: Bound Int TSigEps3d -> Bound.BoundFiles -> Job (DR.DampedRational (BlockBase TSigEpsSetup.TSMixedBlock Scientific) (Matrix 3 3) Scientific)
--getEpsFunctional bound' files = untag @Job $
--  Bound.reifyBoundWithContext bound' $ \(bound :: Bound (Proxy p) TSigEps3d) -> do
--  epsMatrix <- Bound.getBoundObject bound files TSigEps3d.getMatEps
--  norm <- Bound.getBoundObject bound files $
--    SDPB.normalizationVector . SDPB.normalization . Bound.toSDP
--  alpha <- liftIO $ SDPB.readFunctional @(Bound.BigFloat p) norm files.outDir
--  let
--    constAlpha = fmap Polynomial.constant alpha
--    aDotF :: DR.DampedRational (BlockBase TSigEpsSetup.TSMixedBlock (Bound.BigFloat p)) (Matrix 3 3) (Bound.BigFloat p)
--    aDotF = DR.mapNumerator ((fmap (constAlpha `dot`)) . getCompose) epsMatrix
--  pure $
--    DR.mapNumerator (fmap $ Polynomial.mapCoefficients Scientific.fromFloatDigits) (coerceBase aDotF)
--
--
--logFunctionalLocal :: Bound Int TSigEps3d -> Bound.BoundFiles -> Cluster ()
--logFunctionalLocal bound files = do
--  progInfo <- asks clusterProgramInfo
--  functional <- liftIO $ runJobLocal progInfo $ getEpsFunctional bound files
--  Log.info "Functional" (DR.numerator functional)


delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

mkPointMap :: Ord v => [v] -> [v] -> [v] -> Map.Map v (Maybe Bool)
mkPointMap initialAllowed initialDisallowed initialUnknown = Map.fromList $
  [(p, Just True)  | p <- initialAllowed   ] ++
  [(p, Just False) | p <- initialDisallowed] ++
  [(p, Nothing)    | p <- initialUnknown   ]

nmax10TightDelaunay3d :: TSigEpsSetup.SymmetrySector -> Rational -> Rational
                      -> DelaunayConfig -> [V 3 Rational]
                      -> Cluster ()
nmax10TightDelaunay3d gapChannel gapL gapU delaunayConfig initialSearch = do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaVDO initialCheckpoint
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    -- actual params:
    initialAllowed = []
    initialDisallowed = ( Affine.apply affine <$> [ toV (x,y,z) | x <- [-1,1], y <- [-1,1], z <- [-1,1] ] )
    --
    nmax = 10
    paramNmax = 10
    --
    maxSpin = 60 :: Int
    order = 60
    numPoles = 20
    prec = 512
    affine = Affine.AffineTransform
      { affineShift  = toV (0.5181489155405881, 1.4126252760007485, (gapU + gapL) / 2)
      , affineLinear = toV ( toV (2.122968878496495e-6, 0.000023298734093743966, 0)
                           , toV (-2.627338872511758e-7, 2.3940179055068215e-8 , 0)
                           , toV (0, 0, (gapU - gapL) / 2)
                           )
      }
    initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/DEIJy/Object_E0c2DM6XUUb0clRDoR1tvyMLCiLJZzAZACW5nu38-Dk/ck"
      -- Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/AVHjm/Object_Mo0QJokUgNwVbIq4IlF4PgDCDlaWYFN-oIKcSJpkXw8/out"
    fixedLambda = Just $ snd $ MSM.redMid
    bound deltas = Bound
      { boundKey = addGap dGap $
        (tSigEpsFeasibleDefaultGaps deltaExts fixedLambda paramNmax)
        { blockParams  = B3d.Block3dParams
          { -- turn off pole shifting by setting keptPoleOrder = order
            B3d.keptPoleOrder = order
          , B3d.order         = order
          , B3d.precision     = prec
          , B3d.nmax          = nmax
          }
        , numPoles = Just numPoles
        , spins = [ fromRational (dx + toRational x) | x <- [0..maxSpin], dx <- [0,1/2]]
        }
      , precision = prec
      , solverParams = (Params.jumpFindingParams nmax)
        { SDPB.precision = prec
        --, SDPB.writeSolution = SDPB.allSolutionParts
        }
      , boundConfig = defaultBoundConfig
      }
      where
        deltaExts = toV (dSig, dEps)
        (dSig, dEps, dGap) = fromV deltas
    addGap dGap tse3d = tse3d { spectrum = Spectrum.setGap (gapChannel) dGap (spectrum tse3d) }
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed ] ++
      [(p, Just False) | p <- initialDisallowed ] ++
      [(p, Nothing)    | p <- initialSearch ]

nmax10TightDelaunay2d :: TSigEpsSetup.SymmetrySector -> Rational
                      -> DelaunayConfig -> [V 2 Rational]
                      -> Cluster ()
nmax10TightDelaunay2d gapChannel dGap delaunayConfig initialSearch = do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    --(checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    -- actual params:
    initialAllowed = []
    initialDisallowed = ( Affine.apply affine <$> [ toV (x,y) | x <- [-1,1], y <- [-1,1] ] )
    --
    nmax = 10
    paramNmax = 10
    --
    maxSpin = 60 :: Int
    order = 60
    numPoles = 20
    prec = 512
    affine = Affine.AffineTransform
      { affineShift  = toV (0.5181489155405881, 1.4126252760007485)
      , affineLinear = toV ( toV (2.122968878496495e-6, 0.000023298734093743966)
                           , toV (-2.627338872511758e-7, 2.3940179055068215e-8)
                           )
      }
    initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/DEIJy/Object_E0c2DM6XUUb0clRDoR1tvyMLCiLJZzAZACW5nu38-Dk/ck"
      -- Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/AVHjm/Object_Mo0QJokUgNwVbIq4IlF4PgDCDlaWYFN-oIKcSJpkXw8/out"
    initialLambda = Just $ snd $ MSM.redMid
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    bound deltaExts = Bound
      { boundKey = addGap dGap $
        (tSigEpsFeasibleDefaultGaps deltaExts initialLambda paramNmax)
        { blockParams  = B3d.Block3dParams
          { -- turn off pole shifting by setting keptPoleOrder = order
            B3d.keptPoleOrder = order
          , B3d.order         = order
          , B3d.precision     = prec
          , B3d.nmax          = nmax
          }
        , numPoles = Just numPoles
        , spins = [ fromRational (dx + toRational x) | x <- [0..maxSpin], dx <- [0,1/2]]
        }
      , precision = prec
      , solverParams = (Params.jumpFindingParams nmax)
        { SDPB.precision = prec
        --, SDPB.writeSolution = SDPB.allSolutionParts
        }
      , boundConfig = defaultBoundConfig
      }
    addGap dGap' tse3d = tse3d { spectrum = Spectrum.setGap (gapChannel) dGap' (spectrum tse3d) }
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed ] ++
      [(p, Just False) | p <- initialDisallowed ] ++
      [(p, Nothing)    | p <- initialSearch ]

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_test_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteComputeKeepFiles . bound)
  [ (toV (0.5181489, 1.412625), Just lambda)
  --, (toV (0.518144590625, 1.41266015625), Just lambda)
  ]
  where
    lambda = toV
      ( (-38757737) / 88786972
      , (-82533) / 13425418
      , (-111357690) / 245817499
      , (-64848956) / 98255647
      , (-54187949) / 131974577
      )
    nmax = 6
    paramNmax = 8
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_Navigator_test_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteCompute . bound)
--    [ (toV delta, Just $ toV lambda) | (delta, lambda) <- nmax6DisallowedOPEincluded]
    [ (delta, Just $ lambda) | (delta, lambda) <- allowedPointsNmax6]
  where
    nmax = 6
    paramNmax = 8
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14}
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_Navigator_BFGS_nmax6" =
  local (setJobType (MPIJob 8 120) . setJobTime (24*hour) . setJobMemory "90G" ) $
  do
    result :: (V 6 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
    bbMax = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
    bfgsConfig = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-20
      , BFGS.stopOnNegative      = False
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.5181489, 1.412625, 5115035946052249/4811183910600428, 10892257763541/727495865887682,14696384033447130/13320346099119551, 1222630505284516/760610284085429)
      , checkpointMapName = Nothing
      }
    mkBound :: V 6 Rational -> Bound Int TSigEps3d
    mkBound (V6 dSig dEps tttB tttF sse eee) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024}
      , boundConfig = defaultBoundConfig
      }
      where
        nmax = 6
        paramNmax = 8
        dExtInit = toV (dSig, dEps)
        lambdaInit = toV (tttB, tttF, sse, eee, 1)

boundsProgram "TSigEps_Navigator_Extremize_nmax6" =
  local (setJobType (MPIJob 8 120) . setJobTime (24*hour) . setJobMemory "90G" ) $
  do
    result :: ([V 6 Rational], BFGS.BFGSData n (Rounded 'TowardZero 200)) <-
      SDPDeriv.remoteBFGSExtremize extConfig bjConfig directions
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
    bbMax = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
    bfgsConfig' = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-20
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    extConfig = BFGS.MkIslandExtConfig
      { BFGS.bfgsConfig = bfgsConfig'
      , BFGS.goalToleranceObj = 1e-10
      , BFGS.goalToleranceGrad = 1e-15
      , BFGS.lineSearchDecrease = 0.7
      , BFGS.maxIters = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.5180, 1.411, 5115035946052249/4811183910600428, 10892257763541/727495865887682,14696384033447130/13320346099119551, 1222630505284516/760610284085429)
      , checkpointMapName = Nothing
      }
    directions = [ (V6 1 0 0 0 0 0)
                 , (V6 (-1) 0 0 0 0 0)
                 , (V6 0 1 0 0 0 0)
                 , (V6 0 (-1) 0 0 0 0)
                 ]
    mkBound :: V 6 Rational -> Bound Int TSigEps3d
    mkBound (V6 dSig dEps tttB tttF sse eee) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024}
      , boundConfig = defaultBoundConfig
      }
      where
        nmax = 6
        paramNmax = 8
        dExtInit = toV (dSig, dEps)
        lambdaInit = toV (tttB, tttF, sse, eee, 1)

boundsProgram "TSigEps_OPEScan_test_nmax6" =
  local (setJobType (MPIJob 1 127) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "shared") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 6
    paramNmax = 6
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

-- | Note: running this with 64 threads and 128G of memory ran into OOM issues
boundsProgram "TSigEps_Island_OPESearch_nmax6" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (8*hour) . setJobMemory "0G") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine boundDeltaV initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    nmax = 6
    paramNmax = 6
    affine = isingAffineNmax6
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = Affine.apply affine <$> [toV (x,y) | x <- [-1,1], y <- [-1,1]]
        initialAllowed = [toV (0.5181489, 1.412625)]

boundsProgram "TSigEps_island_fixedOPE_nmax6" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (8*hour) . setJobMemory "0G") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    nmax = 6
    paramNmax = 6
    affine = isingAffineNmax6
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 80
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = Affine.apply affine <$> [toV (x,y) | x <- [-1,1], y <- [-1,1]]
        initialAllowed = [toV (0.5181489, 1.412625)]

boundsProgram "TSigEps_ExtOPEBound_nmax6" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" ) $
  mapConcurrently_ (Bound.remoteComputeKeepFiles . bound)
  [ (toV (0.5181489, 1.412625),  toV ( (-39353909) / 90158408
                            , (-350635) / 56507009
                            , (-136589237) / 301491712
                            , (-104165758) / 157848095
                            , (-51410107) / 125167736 ))
  ]
  where
    nmax = 6
    paramNmax = 8
    -- precision = 2048
    bound (dExt, lambda) = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt (Just lambda) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        , objective    = TSigEps3d.ExternalOPEBound lambda UpperBound
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024, SDPB.writeSolution = SDPB.allSolutionParts }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_grid_test_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ toV (5181966689/10000000000, 14126542907/10000000000)
    , toV (41451765811/80000000000, 113010682093/80000000000)
    , toV (13267002881/25600000000, 180948376313/128000000000)
    , toV (10609713092177/20480000000000, 28906067058241/20480000000000)
    , toV (21222570145561/40960000000000, 57833842903433/40960000000000)
    , toV (530685745021/1024000000000, 1447619160393/1024000000000)
    , toV (21228365841927/40960000000000, 57904604103991/40960000000000)
    , toV (169812973654061/327680000000000, 463033775321053/327680000000000)
    , toV (1358568682526427/2621440000000000, 3705127289264811/2621440000000000)
    , toV (339538024043827/655360000000000, 925104881197891/655360000000000)
    , toV (169824674275653/327680000000000, 463279921698609/327680000000000)
    , toV (339602187057741/655360000000000, 925778037760893/655360000000000)
    , toV (135858502394443/262144000000000, 370540672496667/262144000000000)
    , toV (339652346194739/655360000000000, 926576487178547/655360000000000)
    , toV (84913457184483/163840000000000, 231639617077539/163840000000000)
    , toV (67915400618423/131072000000000, 185098210918983/131072000000000)
    , toV (1358383459400409/2621440000000000, 3702834299714377/2621440000000000)
    , toV (5434135901285043/10485760000000000, 14818705405476099/10485760000000000)
    , toV (169827020817161/327680000000000, 463273404357833/327680000000000)
    , toV (271686430145519/524288000000000, 29626978635863/20971520000000)
    , toV (1358310919047503/2621440000000000, 3702053474451039/2621440000000000)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 6
    paramNmax = 8
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_peninsula_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "compute") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ toV (x,y) | x <- [0.8,0.9..1.4], y <- [1.8,1.9..3]
  ]
  where
    nmax = 6
    paramNmax = 6
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt Nothing paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 960, SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_continent_binary_nmax6" =
  mapConcurrently_ search $
  [ (dualPt, toV (1,2)) | dualPt <- dualPts ]
  where
    jobType = MPIJob 1 128
    jobTime = 6*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 6
    search (dualPt, primalPt) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
      remoteTSigEps3dBinarySearch $
      TSigEps3dBinarySearch
      { tse_bs_bound = Bound
        { boundKey = tSigEpsFeasibleDefaultGaps (toV (1,2)) Nothing nmax
        , precision = prec
        , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = prec , SDPB.findPrimalFeasible = False }
        , boundConfig = defaultBoundConfig
        }
      , tse_bs_dualPt = dualPt
      , tse_bs_primalPt = primalPt
      , tse_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 0
          , falsePoint = 1
          }
        , threshold  = 1e-5
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }
    lowerPts = [toV (x,1) | x <- [0.7,0.75..1]]
    leftPts = [toV (0.7,y) | y <- [1.025,1.05..2.975]]
    upperPts = [toV (x,3) | x <- [0.7,0.75..1]]
    dualPts = lowerPts ++ leftPts ++ upperPts

boundsProgram "TSigEps_continent_kink_nmax6" =
  mapConcurrently_ search $
  [ (toV (x,2.5), toV (x,1.75)) | x <- [0.8,0.81..1] ]
  where
    jobType = MPIJob 1 128
    jobTime = 6*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 6
    search (dualPt, primalPt) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
      remoteTSigEps3dBinarySearch $
      TSigEps3dBinarySearch
      { tse_bs_bound = Bound
        { boundKey = tSigEpsFeasibleDefaultGaps (toV (1,2)) Nothing nmax
        , precision = prec
        , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = prec , SDPB.findPrimalFeasible = False }
        , boundConfig = defaultBoundConfig
        }
      , tse_bs_dualPt = dualPt
      , tse_bs_primalPt = primalPt
      , tse_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 0
          , falsePoint = 1
          }
        , threshold  = 1e-5
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }

boundsProgram "TSigEps_island3d_oddGap_OPESearch_nmax6" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (8*hour) . setJobMemory "0G") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaVDO initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine boundDeltaVDO initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    nmax = 6
    paramNmax = 8
    affine = Affine.AffineTransform
      { affineShift  = toV (0.51814861, 1.41261880, 10)
      , affineLinear = toV ( toV (0.000305562, 0.00349606, 0)
                           , toV (-0.0000734378, 6.4186e-6, 0)
                           , toV (0, 0, 1.95) )
      }
    initialBilinearForms = BilinearForms 1e-32 [ (Nothing, opeEllipseEvenTNmax6) ]
    initialCheckpoint = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    bound deltas = Bound
      { boundKey = (tSigEpsFeasibleOddGapsIso 12 deltas initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 10 100
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed] ++
      [(p, Nothing)    | p <- initialSearch]
      where
        initialAllowed = [ ]
        initialDisallowed = Affine.apply affine <$>
          [ toV (x,y,z) | x <- [-1,1], y <- [-1,1], z <- [-1,1] ]
        initialSearch = [toV (0.51814861, 1.41261880, dOdd) | dOdd <- [10.7, 10.8, 9, 10, 10.5, 10.6, 10.9, 11, 11.6] ]

---------------
--- nmax=10 ---
---------------

boundsProgram "TSigEps_OPEScan_test_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "large-shared") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    -- , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 10
    paramNmax = nmax
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_island_nmax10_higher_spin" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPoints
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, opeEllipseEvenTNmax6)]
    initialCheckpoint = Just $ "/expanse/lustre/scratch/dsd/temp_project/data/2024-01/jvTit/Object_3duA2V4370JJuDAobTMkWnPRKFzz29DrUZr_F1wdfFM/ck"
    initialLambda = Just $ snd $ head $ allowedPointsNmax14
    initialPoints = mkPointMap (fmap fst allowedPointsNmax14) initialDisallowedPtsNmax10 []
    delaunayConfig = defaultDelaunayConfig 8 200
    affine = isingAffineNmax6
    nmax = 10
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 832
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_oddGap_test_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625, dOdd) | dOdd <- [10,11,12]
      --  this point should be disallowed
    -- , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 10
    paramNmax = nmax
    bound deltas = Bound
      { boundKey = (tSigEpsFeasibleOddGaps deltas initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_island_fixedOPE_nmax10" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (24*hour) . setJobMemory "0G") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    nmax = 10
    paramNmax = 10
    affine = Affine.AffineTransform
      { affineShift  = toV (0.518171,1.41269)
      , affineLinear = toV ( toV (0.000305562/2, 0.00349606/2), toV (-0.0000734378/2, 6.4186e-6))
      }
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 80
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = Affine.apply affine <$> [toV (x,y) | x <- [-1,1], y <- [-1,1]]
        initialAllowed = [toV (0.5181489, 1.412625)]

boundsProgram "TSigEps_test_nmax6_sample_points" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G") $
  mapConcurrently_ Bound.remoteComputeKeepFiles $ do
  testPoint <- disallowedPoints
  nPoles <- [40]
  pure $ go nPoles testPoint
  where
    disallowedPoints = [ (delta, Just lambda) | (delta, lambda) <- (take 1 disallowedPointsNmax10)]
    nmax = 6
    order = 80
    paramNmax = nmax + 4
    go nPoles (dExt, mLambda)  = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax)
          {  -- turn off pole shifting by setting keptPoleOrder = order
            B3d.keptPoleOrder = order
          , B3d.order         = order
          , B3d.precision     = 1024
          }
        , numPoles     = Just nPoles
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax)
        { SDPB.precision           = 1024
        , SDPB.dualityGapThreshold = 1e-30
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_test_nmax6_scheduler" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G") $
  void $ remoteBuildBoundsWithScheduler (1*minute) [] (pure myBound)
  where
    myBound :: Bound (Precision 768) TSigEps3d
    myBound = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt (Just lambda) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax)
          {  -- turn off pole shifting by setting keptPoleOrder = order
            B3d.keptPoleOrder = order
          , B3d.order         = order
          , B3d.precision     = 1024
          }
        , numPoles     = Just nPoles
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = MkPrecision
      , solverParams = (Params.jumpFindingParams nmax)
        { SDPB.precision           = 1024
        , SDPB.dualityGapThreshold = 1e-30
        }
      , boundConfig = defaultBoundConfig
      }
      where
        (dExt, lambda) = head disallowedPointsNmax10
        nmax      = 6
        order     = 80
        nPoles    = 40
        paramNmax = nmax + 4


boundsProgram "TSigEps_test_nmax10_sample_point" =
  local (setJobType (MPIJob 2 128) . setJobTime (48*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteComputeKeepFiles) $ do
  testPoint <- disallowedPoints
  nPoles <- [40]
  pure $ go nPoles testPoint
  where
    disallowedPoints = [ (delta, Just $ lambda) | (delta, lambda) <- (take 1 disallowedPointsNmax10)]
    -- allowedPoints =  [ (delta, Just $ lambda) | (delta, lambda) <- (take 1 allowedPointsNmax10)]
    nmax = 10
    order = 80
    prec = 512
    paramNmax = nmax + 4
    go nPoles (dExt, mLambda)  = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax)
          {  -- turn off pole shifting by setting keptPoleOrder = order
            B3d.keptPoleOrder = order
          , B3d.order         = order
          , B3d.precision     = 1280
          }
        , numPoles     = Just nPoles
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax)
        { SDPB.precision           = prec
        , SDPB.dualityGapThreshold = 1e-30
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_Navigator_test_nmax10_sample_point" =
  local (setJobType (MPIJob 2 128) . setJobTime (48*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteComputeKeepFiles) $ do
  testPoint <- (allowedPoints)
  nPoles <- [40]
  pure $ go nPoles testPoint
  where
    -- disallowedPoints = [ (delta, Just lambda) | (delta, lambda) <- ( take 1 disallowedPointsNmax10)]
    allowedPoints =  [ (delta, Just $ lambda) | (delta, lambda) <- (take 1 allowedPointsNmax10)]
    nmax = 10
    order = 80
    prec = 512
    paramNmax = nmax + 4
    go nPoles (dExt, mLambda) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax)
          {  -- turn off pole shifting by setting keptPoleOrder = order
            B3d.keptPoleOrder = order
          , B3d.order         = order
          , B3d.precision     = 1280
          }
        , numPoles     = Just nPoles
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
        { SDPB.precision           = prec
        , SDPB.dualityGapThreshold = 1e-30
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_Navigator_test_nmax10_pole_shift" =
  local (setJobType (MPIJob 2 128) . setJobTime (24*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteCompute . bound) $
    [ (delta, Just $ lambda) | (delta, lambda) <- ( take 1 disallowedPointsNmax10) ]
--    ++ [ (delta, Just $ lambda) | (delta, lambda) <- (take 1 allowedPointsNmax10) ]
  where
    nmax = 10
    paramNmax = nmax+4
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_island3d_tightEllipse_odd_nmax10" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (6*hour) . setJobMemory "0G") $
  nmax10TightDelaunay3d z2EvenParityOddScalar 2.5 8 (defaultDelaunayConfig 8 80) [fst (MSM.redMid) `snoc` x | x <- [2.5, 2.9, 3.5, 4, 6]]

boundsProgram "TSigEps_island3d_tightEllipse_evenSpin2_nmax10" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (9*hour) . setJobMemory "0G") $
  nmax10TightDelaunay3d z2EvenParityEvenSpin2 3 6 (defaultDelaunayConfig 5 60) [fst (MSM.redMid) `snoc` 4]


boundsProgram "TSigEps_island2d_2.5_tightEllipse_odd_nmax10" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (6*hour) . setJobMemory "0G") $
  nmax10TightDelaunay2d z2EvenParityOddScalar 2.5 (defaultDelaunayConfig 8 100) [fst (MSM.redMid)]

boundsProgram "TSigEps_island2d_4_tightEllipse_odd_nmax10" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (6*hour) . setJobMemory "0G") $
  nmax10TightDelaunay2d z2EvenParityOddScalar 4 (defaultDelaunayConfig 8 100) [fst (MSM.redMid)]



---------------
--- nmax=14 ---
---------------

boundsProgram "TSigEps_OddOddPrime_OPE_nmax14_TEXTCK" =
  local (setJobType (MPIJob 2 128) . setJobTime (5*hour) . setJobMemory "0" . setSlurmPartition "compute") $ do
  --local (setJobType (MPIJob 1 120) . setJobTime (6*hour) . setJobMemory "240G" . setSlurmPartition "shared") $ do
  --local (setJobType (MPIJob 1 128) . setJobTime (8*hour) . setJobMemory "240G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaOOIso initialCheckpoint
--  lambdaMap     <- Bound.newLambdaMap affine boundDeltaOOIso initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
--    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    nmax = 14
    paramNmax = nmax+4
    affine = Affine.AffineTransform
      { affineShift  = toV ( 7.5, 11.0 )
      , affineLinear = toV ( toV (4.5, 0)
                           , toV (0, 2)
                           )
      }
    --initialBilinearForms = BilinearForms 1e-32 [ (Nothing, opeEllipseEvenTNmax6) ]--opeEllipseNmax10)
    initialCheckpoint =
      Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/tPQCc/Object_HWJ9YArDTKLPtcKmH-eeEaeSNjZcMOnRIGCis8TPSuI/ck" -- no iso
      --Just $ Config.config.dataDir </> "saved_checkpoints/beeJn/Object_aPF9dfZMlYyyhuXlMZopdnB2-IYxOo7CURY_Rku1_-c/out" --normal
      -- Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/bbofe/Object_EIM792Wm-kNe8g33JwKxdFltVPd7OwO1mh4bbC8Vur4/ck" --16
      -- Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/VrWdI/Object_Cxd3IvSEihMK3lwqB5d68qax4yghowzqHB09p9-Kme0/ck" --20
      -- Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/KOjga/Object_SI7pJ-5nUTRGvw74Cfta5IUHKuUItZQM9F10_Zjcpro/ck" --16
      -- Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/KOjga/Object_ELeXgXDzW3KESJ4GoXQcAeGfxMxy4iHgrZsJ4muluVg/ck" --16
    initialLambda = Just $ toV
      ( (-18534117475225766) / 42455600393522181
      , (-38922784302309   ) / 6356008549584481
      , (-6728941691801907 ) / 14853724237980973
      , (-6295478437004153 ) / 9538698838866702
      , (-283976754988745  ) / 691657451630358
      )
    bound deltaOdds = Bound
      { boundKey =
--        (tSigEpsFeasibleOddGapsIso dOddPrime (toV (32384295926001/62500000000000, 565049721695571/400000000000000, dOdd))
--          initialLambda paramNmax) -- Nothing paramNmax)
        (tSigEpsFeasibleOddGaps (toV (32384295926001/62500000000000, 565049721695571/400000000000000, dOdd))
          initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.precision = 960, B3d.nmax = nmax, B3d.keptPoleOrder = 32, B3d.order = 80 }
        } --{ B3d.keptPoleOrder = 14 }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { --paramNmax) { --SDPB.precision = 960 ,
          SDPB.writeSolution = SDPB.allSolutionParts }
      , boundConfig = defaultBoundConfig
      }
      where
        (dOdd, _) = fromV deltaOdds
    delaunayConfig = defaultDelaunayConfig 2 2
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed] ++
      [(p, Nothing)    | p <- initialSearch]
      where
        initialAllowed = [ ]
        initialDisallowed = []
        initialSearch = [toV (x,y) | x <- [10.85, 10.9], y <- [10.95, 11] ]
          --[toV (x,y) | x <- [10.92, 10.85, 10.94], y <- [11, 10.95, 11.2] ]
          -- [toV (5, 11.4), toV (5, 11.4)]
          -- [toV (x, y) | x <- [10.5, 10.6 .. 12], y <- [5] ]
          --
          --fmap (Affine.apply affine . toV) [ (x,y) | x <- [-1,1], y <- [-1,1] ]
          -- [ toV (x,y) | x <- [10.7,11] , y <- [11.25, 11.5 .. 12.5] ]

boundsProgram "TSigEps_OddOddPrime_binary_OPESearch_nmax14" =
  mapConcurrently_ search $
  --[ (toV (do1, dgap), toV (do2, dgap)) | (do1,do2) <- [(8, 10.9)], dgap <- [11] ]--, 11.2 .. 11.6] ] -- TODO
  [ (toV (do1, dgap), toV (do2, dgap)) | (do1,do2) <- [(10.2, 10.9)], dgap <- [11.2, 11.4, 11.6, 11.8, 12] ] --, also ubound (11, 10.9)
  where
    jobType = MPIJob 4 128
    jobTime = 24*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 14
    paramNmax = 18
    (dExt, fixedLambda) = MSM.redMid
    (dSig, dEps) = fromV dExt
    -- initialCheckpoint = Just $ Config.config.dataDir </> "saved_checkpoints/beeJn/Object_aPF9dfZMlYyyhuXlMZopdnB2-IYxOo7CURY_Rku1_-c/out"
    search (dualPt, primalPt) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
      remoteGapBinarySearchSameCheckpoint $
      TSigEps3dBinarySearch
      { tse_bs_bound = Bound
        { boundKey =
            -- gaps will be changed
            (tSigEpsFeasibleOddGapsIso 11 (toV (dSig,dEps, 10.9)) (Just fixedLambda) paramNmax)
            { blockParams  = (Params.block3dParamsNmax paramNmax)
              { B3d.precision = 960, B3d.nmax = nmax, B3d.keptPoleOrder = 32, B3d.order = 80 }
            }
        , precision = (Params.block3dParamsNmax nmax).precision
        --, solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = prec , SDPB.findPrimalFeasible = False }
        , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , tse_bs_dualPt = dualPt
      , tse_bs_primalPt = primalPt
      , tse_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 0
          , falsePoint = 1
          }
        , threshold  = 1e-3
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }

boundsProgram "TSigEps_OddOddPrime_scan_OPESearch_nmax14" =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
  mapConcurrently_
  (SNS.remoteComputeAllWithFileTreatment Bound.keepOutAndCheckpoint Bound.defaultBoundFiles initialCheckpoint)
  boundList
  where
    jobType = MPIJob 4 128
    jobTime = 20*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 14
    paramNmax = 18
    mkBound (dExt, fixedLambda) dgap iso = Bound
      { boundKey = let (dSig, dEps) = fromV dExt in
          (tSigEpsFeasibleOddGapsIso dgap (toV (dSig,dEps, iso)) (Just fixedLambda) paramNmax)
          { blockParams  = (Params.block3dParamsNmax paramNmax)
                           { B3d.precision = 960, B3d.nmax = nmax, B3d.keptPoleOrder = 32, B3d.order = 80 }
          }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
      , boundConfig = defaultBoundConfig
      }
    --pointsForEach dgap = mkBound dgap <$> [ 10.85, 10.86 .. 10.97 ]
    pointsHorizontal1 = [ (y,x)
                       | y <- [ 11, 10.95]
                       , x <- [10.83, 10.8, 10.75, 10.65, 10.4, 10.2, 10.014, 9.5, 9, 8, 5] ] :: [(Rational,Rational)]
    pointsHorizontal2 = [ (y,x)
                       | y <- [ 11.1 ]
                       , x <- [10.83, 10.8, 10.75, 10.65, 10.4, 10.2, 10.014, 9.5, 9, 8, 5] ] :: [(Rational,Rational)]
    pointsFuzzy = [ (y,x) | x <- [10.014], y <- [11.16, 11.14 .. 10.8] ] :: [(Rational,Rational)]
    pointsVertical = [ (y,x) | x <- [10.927, 10.921], y <- [14, 15 .. 22] ] :: [(Rational,Rational)]
    --pointsForEach = [ (11.2,27713/2560),(11.2,69283/6400),(11.3,35562683/3276800),(11.3,71125933/6553600),(11.4,1739/160),(11.4,139121/12800),(11.5,891211/81920),(11.5,8912177/819200),(11.6,8709/800),(11.6,27869/2560),(11.7,139413/12800),(11.7,69707/6400),(11.8,139467/12800),(11.8,34867/3200),(11.9,35714311/3276800),(11.9,4464311/409600),(12.0,139543/12800),(12.0,17443/1600),(12.1,89326281/8192000),(12.1,35730703/3276800),(12.2,279193/25600),(12.2,2791947/256000),(12.3,11169369/1024000),(12.3,178711281/16384000),(12.4,5719450869/524288000),(12.4,357468003/32768000),(12.5,349127/32000),(12.5,279303/25600),(12.6,11173233/1024000),(12.6,22346571/2048000),(12.7,715150437/65536000),(12.7,357578001/32768000),(12.8,5721665391/524288000),(12.8,45773606943/4194304000),(12.9,732424540563/67108864000),(12.9,366214115079/33554432000),(13.0,11719593291123/1073741824000),(13.0,23439374751591/2147483648000),(13.1,375049377467991/34359738368000),(13.1,187525723665393/17179869184000),(13.2,1397243/128000),(13.2,5589/512),(13.3,22357071/2048000),(13.3,2794647/256000),(13.4,715462817/65536000),(13.4,357733719/32768000),(13.5,5723956691/524288000),(13.5,22895933047/2097152000),(13.6,11448444797/1048576000),(13.6,45794098037/4194304000) ]
    boundList = [ uncurry (mkBound MSM.redMid) <$> pointsFuzzy
                , uncurry (mkBound MSM.redMid) <$> pointsHorizontal1
                , uncurry (mkBound MSM.redMid) <$> pointsHorizontal2
                , uncurry (mkBound MSM.redMid) <$> pointsVertical
--                , uncurry (mkBound MSM.redUp ) <$> pointsForEach
--                , uncurry (mkBound MSM.redL  ) <$> pointsForEach
--                , uncurry (mkBound MSM.redR  ) <$> pointsForEach
                ]
    initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/UtsMb/Object_ek43VWyBhwuterzfoMa_pxIynxEZvBOWunHCAahU4k8/ck"

boundsProgram "TSigEps_OddOddPrime_shrinkingScan_fixed_nmax14" =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
  mapM_ (mapConcurrently_ (uncurry searchExtAndBrackets))
  [ [ (MSM.redMid, bracketsL1), (MSM.redMid, bracketsL2) ]
--[ (MSM.redL, bracketsLL) , (MSM.redUp, bracketsRU) ] , [ (MSM.redL, bracketsRL) , (MSM.redR, bracketsRR) ]
  ]
  where
    jobType = MPIJob 4 128
    jobTime = 4*hour
    jobMemory = "0"
    jobPartition = "compute"
    tolerance = 0.0001
    initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/UtsMb/Object_ek43VWyBhwuterzfoMa_pxIynxEZvBOWunHCAahU4k8/ck"
    searchExtAndBrackets = remoteComputeBSShrinking Bound.keepOutAndCheckpoint Bound.defaultBoundFiles initialCheckpoint tolerance
    -- ordering is (gap, (allowed,disallowed))
    bracketsL1 = [(11.1,(10.8,10.75))]-- :: [(Rational,(Rational,Rational))]
    bracketsL2 = [(10.95,(10.014,9.5))]-- :: [(Rational,(Rational,Rational))]
    --bracketsL0 = [(13.2,(10.917,10.91)),(13.3,(10.917,10.915)),(13.4,(10.919,10.915)),(13.5,(10.919,10.915)),(13.6,(10.919,10.917))]
    --bracketsR = [(g,(10.925, 10.93)) | g <- [11.2, 11.3 .. 13.6]]

boundsProgram "TSigEps_OddOddPrime_spotCheck_fixed_nmax14" =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
  mapM_ (mapConcurrently_ (uncurry searchExtAndBrackets))
  [ [ (MSM.redUp, bracketsL) , (MSM.redL, bracketsL) , (MSM.redR, bracketsL) ]
  , [ (MSM.redUp, bracketsR) , (MSM.redL, bracketsR) , (MSM.redR, bracketsR) ]
  ]
  where
    jobType = MPIJob 4 128
    jobTime = 12*hour
    jobMemory = "0"
    jobPartition = "compute"
    --jobType = MPIJob 1 2  -- for mockup
    --jobTime = 6*minute
    --jobMemory = "4G"
    --jobPartition = "debug"
    tolerance = 0.0001
    initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/UtsMb/Object_ek43VWyBhwuterzfoMa_pxIynxEZvBOWunHCAahU4k8/ck"
    searchExtAndBrackets = remoteComputeBSShrinking Bound.keepOutAndCheckpoint Bound.defaultBoundFiles initialCheckpoint tolerance
    bracketsL = [(11.5,(10.881,10.877)),(12.0,(10.904,10.900)),(12.5,(10.912,10.908)),(13.0,(10.917,10.913)),(13.5,(10.920,10.916))]
    bracketsR = [(g,(10.925, 10.935)) | g <- [11.2, 12, 12.5, 13, 13.5]]

boundsProgram "TSigEps_island3d_oddGap_OPESearch_nmax14" =
  local (setJobType (MPIJob 4 128) . setSlurmPartition "compute" . setJobTime (20*hour) . setJobMemory "0G") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaVDO initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine boundDeltaVDO initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where--TODO
    -- prec = 960
    nmax = 14
    paramNmax = 18
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseNmax14)
      ]
    initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/cuQPp/Object_I_xTYDH9nm4GmW9KcNCcI1sA9ZWRmO3QMyeSTtZuCkw/out" --14
    initialLambda = Just $ snd $ MSM.redMid
    affine = Affine.AffineTransform
      { affineShift  = toV (0.51814881069177892831,1.41262530254414050358, 7.5)
      , affineLinear = toV ( toV ( 8.4961935183773544e-8,1.0613400209030865e-6, 0)
                           , toV ( -5.7741188683652221e-9,4.622272819035116e-10, 0)
                           , toV (0, 0, 5)
                           )
      }
    bound deltas = Bound
      { boundKey = (tSigEpsFeasibleOddGaps deltas initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.precision = 960, B3d.nmax = nmax, B3d.keptPoleOrder = 32, B3d.order = 80 }
        } --{ B3d.keptPoleOrder = 14 }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax) { SDPB.precision = 960 , SDPB.writeSolution = SDPB.allSolutionParts }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 5 90-- defaultDelaunayConfig 16 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed ] ++
      [(p, Just False) | p <- initialDisallowed ] ++
      [(p, Nothing)    | p <- initialSearch ]
      where
        initialAllowed = (\x -> fst x `snoc` 3) <$> [MSM.redMid, MSM.redUp, MSM.redL, MSM.redR]
        initialDisallowed = (\x -> fst x `snoc` 12) <$> [MSM.redMid, MSM.redUp, MSM.redL, MSM.redR]
        initialSearch = []

boundsProgram "TSigEps_OPEScan_test_nmax14" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ toV (0.5181489, 1.412625) ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint = Nothing
    initialLambda = Just $
      toV ( (-54537050) / 124926441
          , (-242728) / 39635427
          , (-72625868) / 160316979
          , (-43886811) / 66495941
          , (-48814271) / 118892408
          )
    affine = isingAffineNmax6
    nmax = 14
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = Params.jumpFindingParams paramNmax
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_test_nmax14_higher_spin" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound . toV)
    [ -- (0.5181489, 1.412625)
    -- , (0.5181481194, 1.412616539)
    -- , (0.5181484682, 1.412620912)
      (0.5181488170, 1.412625285) -- this point is allowed
    -- , (0.5181485667, 1.412620903)
    -- , (0.5181497117, 1.412634013)
    -- , (0.5181493629, 1.412629640)
    -- , (0.5181490141, 1.412625267)
    -- , (0.5181492643, 1.412629649)
    -- , (0.518148711906445,1.41262529457607)
    -- , (0.518149150580087,1.41262995303994)
    -- , (0.518149499871215,1.41263336070628)
    -- , (0.518149666187551,1.41263460449364)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, opeEllipseNmax10)]
    initialCheckpoint    = Nothing
    initialLambda = Just $ snd $ head allowedPointsNmax14
    affine = isingAffineNmax6
    nmax = 14
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 960
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_block_generation_test_nmax14_higher_spin" =
  local (setJobType (MPIJob 4 128) . setJobTime (2*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  mapConcurrently_ (Bound.remoteCompute . bound . toV)
    [ (0.5181488170, 1.412625285) ]
  where
    initialLambda = Just $ snd $ head allowedPointsNmax14
    nmax = 14
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 960
        , SDPB.maxIterations = 1
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_nmax14_island" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPoints
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, opeEllipseNmax10)]
    initialCheckpoint    = Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-01/RvFEH/Object_ZsUhZGnZd1qsAg3QUGCiCHeYPxnE0BwLqRMTkmSCo0w/ck_bak"
    initialLambda = Just $ snd $ head allowedPointsNmax14
    initialPoints = mkPointMap (fmap fst allowedPointsNmax14) initialDisallowedPtsNmax14 []
    delaunayConfig = defaultDelaunayConfig 8 200
    affine = isingSigEpsAffineNmax22
    nmax = 14
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 960
        }
      , boundConfig = defaultBoundConfig
      }


--boundsProgram "TSigEps_Navigator_BFGS_nmax14" =
--  local (setJobType (MPIJob 4 128) . setJobTime (8*hour) . setJobMemory "0G" ) $
--  do
--    result :: (V 6 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
--      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
--    Log.info "Finished BFGS search" result
--  where
--    bbMin = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
--    bbMax = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
--    bfgsConfig = BFGS.MkConfig
--      { BFGS.epsGradHess         = 1e-9
--      , BFGS.stepResolution      = 1e-32
--      , BFGS.gradNormThreshold   = 1e-20
--      , BFGS.stopOnNegative      = False
--      , BFGS.boundingBoxMin      = bbMin
--      , BFGS.boundingBoxMax      = bbMax
--      , BFGS.initialHessianGuess = Nothing
--      }
--    bjConfig = SDPDeriv.GetBoundJetConfig
--      { centeringIterations = 10
--      , fileTreatment = Bound.keepOutAndCheckpoint
--      , boundClosure = cPtr (static mkBound)
--      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y)))
--      , initialPoint = toV (0.51814879347835422880404490229549010286,
--                            1.41262502927609557615333014104008212578,
--                            1.0632727190522107460749619510476228338,
--                            0.01491529037422555299266770945403541839,
--                            1.10336466371612554872550921095108120208,
--                            1.60748815952494092929964395803218995103)
--      , checkpointMapName = Nothing
--      }
--    mkBound :: V 6 Rational -> Bound Int TSigEps3d
--    mkBound (V6 dSig dEps tttB tttF sse eee) = Bound
--      { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
--        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
--        , spins        = Params.gnyspinsNmax paramNmax
--        }
--      , precision = (Params.block3dParamsNmax paramNmax).precision
--      , solverParams = (Params.optimizationParams paramNmax) { SDPB.precision = 1024}
--      , boundConfig = defaultBoundConfig
--      }
--      where
--        nmax = 14
--        paramNmax = nmax+4
--        dExtInit = toV (dSig, dEps)
--        lambdaInit = toV (tttB, tttF, sse, eee, 1)
--

---------------
--- nmax=18 ---
---------------

boundsProgram "TSigEps_OPEScan_test_nmax18" =
  local (setJobType (MPIJob 10 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound . toV)
    [ -- (0.5181488401972047734375, 1.412625680313401015625) -- disallowed
      (0.5181488078196943, 1.4126252858162218)
    , (0.5181489139681382, 1.4126264507529267)
    , (0.5181490201165821, 1.4126276156896314)
    , (0.5181490096072267, 1.4126276166472385)
    , (0.5181489034587827, 1.4126264517105338)
    , (0.5181488929494272, 1.412626452668141)
    , (0.5181489990978712, 1.4126276176048456)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, opeEllipseNmax14)]
    initialCheckpoint    = Just
      "/expanse/lustre/scratch/dsd/temp_project/data/2024-01/usjqx/Object_aQQqATM5kT9uHKiQC235DhnjBTIUjK7jwuxSkouA1hY/ck"
    -- initialLambda = Just $ toV
    --   ( -1658734190246597/3799621375756884
    --   , -73915869141311/12070247606466893
    --   , -2734123222177418/6035418066030219
    --   , -14317638628504965/21693590972132147
    --   , -4560076580436441/11106569993565242
    --   )
    initialLambda = Just $ toV
      ( 0.6614491590249835444638196055053578046101
      , 0.009278708986398039045055927427331641497957
      , 0.6863897271359756701474230706138940177189
      , 1
      , 0.6220887843676479304074578813626728806692
      )
    affine = isingSigEpsAffineNmax22
    nmax = 18
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 1024
        }
      , boundConfig = defaultBoundConfig
      }


-- PfswO - stopped at mu ~ 10^92
boundsProgram "TSigEps_OPEScan_get_stuck_test_nmax18" =
  local (setJobType (MPIJob 10 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ nmax14NavMinDeltaExt ]
  where
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, opeEllipseNmax14)]
    initialCheckpoint    = Nothing
    initialLambda = Just nmax14NavMinLambda
    -- Actually, it looks like this is a saddle point, not a minimum
    nmax14NavMinLambda = toV
      ( 1.06327271904638662842107062911645802957
      , 0.01491529037692802774798073101691308083
      , 1.10336466371198863731307788763685154523
      , 1.60748815952064793097175788510228383658
      , 1
      )
    nmax14NavMinDeltaExt = toV
      ( 0.51814879347866917322466084702115987315
      , 1.4126250292784055400104292464998535462
      )
    affine = isingSigEpsAffineNmax22
    nmax = 18
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 1024
        }
      , boundConfig = defaultBoundConfig
      }

-- See /expanse/lustre/scratch/dsd/temp_project/logs/2024-03/ApaaR/0.log
--
-- The log seems to follow exactly Aike's tests that eventually
-- stalled in
-- /home/aikeliu/hyperion-lib/stress-tensors-3d/nmax18Mu120/nmax18Stall.out
--
boundsProgram "TSigEps_nav_min_test_nmax18" =
  local (setJobType (MPIJob 10 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  mapConcurrently_ (Bound.remoteCompute . bound)
    [ nmax14NavMinDeltaExt ]
  where
    initialLambda = Just nmax14NavMinLambda
    -- Actually, it looks like this is a saddle point, not a minimum
    nmax14NavMinLambda = toV
      ( 1.06327271904638662842107062911645802957
      , 0.01491529037692802774798073101691308083
      , 1.10336466371198863731307788763685154523
      , 1.60748815952064793097175788510228383658
      , 1
      )
    nmax14NavMinDeltaExt = toV
      ( 0.51814879347866917322466084702115987315
      , 1.4126250292784055400104292464998535462
      )
    nmax = 18
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.optimizationParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 1024
        }
      , boundConfig = defaultBoundConfig
      }

------------------------
-- Debugging nmax18 ----
------------------------
--


boundsProgram "TSigEps_Navigator_test_nmax18" =
  local (setJobType (MPIJob 6 128) . setJobTime (48*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteCompute . bound)
--    [ (toV delta, Just $ toV lambda) | (delta, lambda) <- nmax6DisallowedOPEincluded]
    [ (delta, Just $ lambda) | (delta, lambda) <- nmax14AllowedPoint]
  where
    nmax = 18
    paramNmax = nmax+4
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax = nmax, B3d.keptPoleOrder = 56}
        , spins        = Params.gnyspinsNmax 26
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.optimizationParams paramNmax) { SDPB.precision = 1536
                                                        , SDPB.maxRuntime = SDPB.RunForDuration (48 * 3600)
                                                        , SDPB.maxSharedMemory = Just "35G"
                                                        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_nav_min_test_nmax18_higher_spin" =
  local (setJobType (MPIJob 10 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  mapConcurrently_ (Bound.remoteCompute . bound)
    [ nmax14NavMinDeltaExt ]
  where
    initialLambda = Just nmax14NavMinLambda
    -- Actually, it looks like this is a saddle point, not a minimum
    nmax14NavMinLambda = toV
      ( 1.06327271904638662842107062911645802957
      , 0.01491529037692802774798073101691308083
      , 1.10336466371198863731307788763685154523
      , 1.60748815952064793097175788510228383658
      , 1
      )
    nmax14NavMinDeltaExt = toV
      ( 0.51814879347866917322466084702115987315
      , 1.4126250292784055400104292464998535462
      )
    nmax = 18
    paramNmax = nmax+4
    -- The spin set that led to stalling behavior was:
    -- [0 .. 64] ++ [67, 68, 71, 72, 75, 76, 79, 80, 83, 84, 87, 88]
    --
    -- First let's try incrementing by 16
    spinSet = [0 .. 80] ++ [83, 84, 87, 88, 91, 92, 95, 96, 99, 100, 103, 104]
    bound dExt = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = spinSet
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.optimizationParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 1024
        }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "TSigEps_free_theory_test_nmax18" =
  local (setJobType (MPIJob 10 128) . setJobTime (2*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  void $ Bound.remoteComputeKeepFiles bound
  where
    nmax = 18
    paramNmax = nmax+4
    dExt = toV (0.5, 1)
    bound = Bound
      { boundKey = (tSigEpsNoGaps dExt Nothing paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.optimizationParams paramNmax)
        { SDPB.verbosity            = 2
        , SDPB.precision            = 1024
        , SDPB.dualErrorThreshold   = 1e500
        , SDPB.primalErrorThreshold = 1e500
        , SDPB.dualityGapThreshold  = 1e500
        }
      , boundConfig = defaultBoundConfig
      }


boundsProgram p = unknownProgram p
