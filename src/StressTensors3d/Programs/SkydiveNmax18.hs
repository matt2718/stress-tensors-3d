{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PatternSynonyms     #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeFamilies        #-}

module StressTensors3d.Programs.SkydiveNmax18 where

import Blocks.Blocks3d                           qualified as B3d
import Bootstrap.Math.Linear                     (toV)
import Bootstrap.Math.Linear                     qualified as L
import Control.Monad                             (void)
import Control.Monad.Reader                      (local)
import Data.Text                                 (Text)
import Data.Time.Clock                           (NominalDiffTime)
import Hyperion
import Hyperion.Bootstrap.Bound                  (Bound (..), Precision (..))
import Hyperion.Bootstrap.Bound                  qualified as Bound
import Hyperion.Bootstrap.Main                   (unknownProgram)
import Hyperion.Bootstrap.Params                 qualified as Params
import Hyperion.Log                              qualified as Log
import Hyperion.Util                             (hour, minute)
import Linear.V                                  (V)
import SDPB                                      (BigFloat)
import SDPB qualified
import StressTensors3d.Programs.Defaults         (defaultBoundConfig,
                                                  taskStatsDir)
import StressTensors3d.Programs.SkydiveNmax6To14 (nmax18GuessPointDelta,
                                                  nmax18GuessPointOPE,
                                                  nmax6OptimumHessian)
import StressTensors3d.Programs.TSigEps2023      (remoteBuildBoundsWithScheduler,
                                                  tSigEpsNavigatorDefaultGaps)
import StressTensors3d.Scheduler.TSigEps         qualified as Scheduler
import StressTensors3d.Skydive                   (SkydiveConfig (..),
                                                  SkydiveState (..),
                                                  defaultExactHessianClock,
                                                  defaultSkydiveConfig,
                                                  skydiveBoundLoop)
import StressTensors3d.TSigEps3d                 (TSigEps3d (..))
import System.FilePath.Posix                     ((</>))

nmax18SkydiveStartBound :: Bound (Precision 832) TSigEps3d
nmax18SkydiveStartBound = nmax18SkydiveBound dExt lambda
  where
    -- These are Aike's initial guesses for the nmax=18 skydive run
    -- NB: Actually, these are Rationals, while Aike's actual values
    -- come from acting with realToFrac on some BigFloat's, so they
    -- are slightly different. It's ok, though since we can still use
    -- the initial checkpoint, presumably.
    dExt = toV
      ( 0.5181487934828117585021129060445315744224944967925412000864
      , 1.412625029336926951754398315910984547002009644668004227764
      )
    lambda = toV
      ( 1.0632767158428064591906057004693878523342053470569097769953
      , 0.0149149196415908648552542961324822610074968909736691101731
      , 1.1033695749769327038374102603118944391989746322351478430112
      , 1.6074891682360134069140634254297844889077171079699876658017
      , 1
      )

nmax18SkydiveBound :: V 2 Rational -> V 5 Rational -> Bound (Precision 832) TSigEps3d
nmax18SkydiveBound dExt lambda = Bound
  { boundKey = (tSigEpsNavigatorDefaultGaps dExt (Just lambda) nmax)
    { blockParams = B3d.Block3dParams
      { -- turn off pole shifting by setting keptPoleOrder = order
        B3d.keptPoleOrder = order
      , B3d.order         = order
      , B3d.precision     = precision
      , B3d.nmax          = nmax
      }
    , numPoles     = Just numPoles
    , spins        = spins
    }
  , precision = MkPrecision @832
  , solverParams = (Params.optimizationParams nmax)
    { SDPB.precision                = precision
    -- Use a high dualityGapThreshhold so that we have an initial
    -- point for skydiving
    , SDPB.dualityGapThreshold      = 0.01
    , SDPB.initialMatrixScaleDual   = 1e20
    , SDPB.initialMatrixScalePrimal = 1e20
    }
  , boundConfig = defaultBoundConfig
  }
  where
    -- These are slightly more conservative versions of Vasiliy's
    -- recommended parameters.
    maxSpin   = 75
    spins     = [0 .. maxSpin]
    order     = 106 -- even order is slighty better
    numPoles  = 35
    precision = 832
    nmax      = 18

-- | Convert from external parameters to a pair (deltaExt, lambda),
-- where lambda is normalized so that its last component (lambdaTTe)
-- is 1. The normalization doesn't matter because it appears in a
-- positivity constraint. In our conventions, the external parameters
-- are
--
-- (dSig, dEps, lTTT^B\/lTTE, lTTT^F\/lTTE, lSSE\/lTTE, lEEE\/lTTE)
--
fromExternalParams :: Num a => V 6 a -> (V 2 a, V 5 a)
fromExternalParams p = (deltaExt, lambdaInit `L.snoc` 1)
  where
    (deltaExt, lambdaInit) = L.splitAt @2 p

skydiveNmax18 :: NominalDiffTime -> [FilePath] -> Maybe FilePath -> Maybe FilePath -> Int -> Job (V 6 (BigFloat 512))
skydiveNmax18 reportInterval initialStatFiles initialCheckpoint maybeWorkDir numIters = do
  workDir <- case maybeWorkDir of
    Just dir -> pure dir
    Nothing  -> newWorkDir (mkBound initialExternalParams)
  skydiveBoundLoop reportInterval (initialState workDir) mkBound numIters
  where
    initialExternalParams = nmax18GuessPointDelta L.++ nmax18GuessPointOPE
    initialState workDir = MkSkydiveState
      { statFiles = initialStatFiles
      , boundFiles = (Bound.defaultBoundFiles workDir) { Bound.initialCheckpointDir = initialCheckpoint }
      , skydiveConfig = defaultSkydiveConfig
        { boundingBoxMax       = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
        , boundingBoxMin       = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
        , useExactHessian      = False
        , prevHessian          = Just nmax6OptimumHessian
        , dualityGapUpperLimit = 0.1
        , bfgsPartialUpdate    = 1
        }
      , externalParams = initialExternalParams
      , exactHessianClock = defaultExactHessianClock
      }
    adjustSolverParams bound = bound
      { solverParams = bound.solverParams
        { SDPB.maxRuntime                 = SDPB.RunForDuration (24 * 3600)
        , SDPB.dualityGapThreshold        = 1e-30
        , SDPB.feasibleCenteringParameter = 0.3
        }
      }
    mkBound externalParams = adjustSolverParams (nmax18SkydiveBound dExt lambda)
      where
        (dExt, lambda) = fromExternalParams (fmap toRational externalParams)


boundsProgram :: Text -> Cluster ()

-- nmax=18 scheduler test
boundsProgram "TSigEps_test_nmax18_scheduler" =
  local (setJobType (MPIJob 10 128) . setJobTime (4*hour) . setJobMemory "0G") $
  void $ remoteBuildBoundsWithScheduler (1*minute) statFiles (pure nmax18SkydiveStartBound)
  where
    statFiles = [taskStatsDir </> "TSigEps_test_nmax18_scheduler_expanse_10_128_collected_stats_order_106.json"]

-- nmax=18 skydive initial run
boundsProgram "TSigEps_test_nmax18_skydive_initial_run" =
  local (setJobType (MPIJob 10 128) . setJobTime (12*hour) . setJobMemory "0G") $
  void $ Scheduler.remoteComputeBound reportInterval statFiles Bound.keepAllFiles Nothing nmax18SkydiveStartBound mkBoundFiles
  where
    statFiles = ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0/json/task_stats_collected_lEKhGmDpWY5U3uUd7c0rY6NJC9l-l3QbWnlx3dhcz84.json"]
    mkBoundFiles = const $
      Bound.defaultBoundFiles "/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0"
    reportInterval = 1*minute

boundsProgram "TSigEps_test_nmax18_skydive_nav_min_loop" =
  local (setJobType (MPIJob 10 128) . setJobTime (12*hour) . setJobMemory "0G") $ do
  result <- remoteEvalJob $
    static skydiveNmax18
    `cAp` cPure reportInterval
    `cAp` cPure initialStatFiles
    `cAp` cPure initialCheckpoint
    `cAp` cPure maybeWorkDir
    `cAp` cPure 10
  Log.info "The result is" result
  where
    initialStatFiles = ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0/json/collected_stats_combined_wOpG2_DLXDoIp0bO1naIRd8XJvz811zgwIhucxr_74E.json"]
    initialCheckpoint = Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0/ck"
    maybeWorkDir = Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-07/ofHdy/Object_F9W-3F0u1uSUZDT60bBrGRamCTV-g8MPd4w7sAup1rs"
    reportInterval = 1*minute

boundsProgram p = unknownProgram p
