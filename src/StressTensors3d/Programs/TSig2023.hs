{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE PatternSynonyms     #-}

module StressTensors3d.Programs.TSig2023 where

import Data.Proxy (Proxy)
import Data.Vector (Vector)
import Numeric.Rounded (reifyPrecision)
import Blocks.Blocks3d                   qualified as B3d
import Blocks.Delta                      (Delta (..))
import Bootstrap.Bounds.BoundDirection   (BoundDirection (..))
import Bootstrap.Bounds.Spectrum         qualified as Spectrum
import Bootstrap.Math.AffineTransform    (AffineTransform (..))
import Bootstrap.Math.Linear             (fromV, toV, pattern V2)
import Bootstrap.Math.VectorSpace        qualified as V
import Control.Monad.Reader              (asks, liftIO, local)
import Data.Ratio                        (approxRational)
import Data.Text                         (Text)
import Data.Time.Clock                   (NominalDiffTime)
import Hyperion
import Hyperion.Bootstrap.Bound          (Bound (..))
import Hyperion.Bootstrap.Bound          qualified as Bound
import Hyperion.Bootstrap.DelaunaySearch (DelaunayConfig (..))
import Hyperion.Bootstrap.Main           (unknownProgram)
import Hyperion.Bootstrap.OPESearch      qualified as OPE
import Hyperion.Bootstrap.Params         qualified as Params
import Hyperion.Database                 qualified as DB
import Hyperion.Log                      qualified as Log
import Hyperion.Util                     (hour)
import Linear.V                          (V)
import SDPB qualified
import StressTensors3d.Programs.Defaults (defaultBoundConfig)
import StressTensors3d.TSig3d            (TSig3d (..))
import StressTensors3d.TSig3d            qualified as TSig3d
import StressTensors3d.TSigEpsSetup      qualified as TSigEpsSetup
import StressTensors3d.Z2Rep             (Z2Rep (..))


-- logExtMatLocal :: Bound Int TSig3d -> Bound.BoundFiles -> Cluster ()
-- logExtMatLocal bound files = undefined
--  progInfo <- asks clusterProgramInfo
--  staticConfig <- asks clusterStaticConfig
--  extMat <- liftIO $ runJobLocal staticConfig progInfo $ getExtMat bound files
--  Log.info "External matrix" extMat

externalDimsToDeltaV :: TSigEpsSetup.ExternalDims -> V 2 Rational
externalDimsToDeltaV dims = V2 dims.deltaSig dims.deltaEps


z2EvenParityEvenScalar, z2OddParityEvenScalar, z2EvenParityOddScalar, z2OddParityOddScalar
  :: TSigEpsSetup.SymmetrySector
z2EvenParityEvenScalar = TSigEpsSetup.SymmetrySector 0 B3d.ParityEven Z2Even
z2OddParityEvenScalar  = TSigEpsSetup.SymmetrySector 0 B3d.ParityEven Z2Odd
z2EvenParityOddScalar  = TSigEpsSetup.SymmetrySector 0 B3d.ParityOdd Z2Even
z2OddParityOddScalar   = TSigEpsSetup.SymmetrySector 0 B3d.ParityOdd Z2Odd

z2EvenParityEvenSpin2 :: TSigEpsSetup.SymmetrySector
z2EvenParityEvenSpin2 = TSigEpsSetup.SymmetrySector 2 B3d.ParityEven Z2Even

boundToScalarGapV :: Bound prec TSig3d -> V 2 Rational
boundToScalarGapV Bound { boundKey = TSig3d { spectrum = s } } =
  toV (deltaEven, deltaOdd)
  where
    toScalarDim c = case Spectrum.deltaGap s c of
      Fixed d             -> d
      RelativeUnitarity d -> d + 1/2
    deltaEven = toScalarDim z2EvenParityEvenScalar
    deltaOdd  = toScalarDim z2OddParityEvenScalar

ttssFeasibleDefaultGaps :: Rational -> Maybe (V 2 Rational) -> Int -> TSig3d
ttssFeasibleDefaultGaps deltaSig mLambdaTTT nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 1.2)
    , (z2EvenParityOddScalar, 3 )
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSig3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0

ttssFeasibleTestGaps ::  Rational -> (Rational,Rational) -> Int -> TSig3d
ttssFeasibleTestGaps deltaSig (dEven,dOdd) nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, dEven)
    , (z2EvenParityOddScalar,  dOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSig3d.Feasibility Nothing
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0


ttssFeasibleIsingGaps :: Rational -> Maybe (V 2 Rational) -> Int -> TSig3d
ttssFeasibleIsingGaps dOdd mLambdaTTT nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3)
    , (z2EvenParityOddScalar, dOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.addIsolated z2EvenParityEvenScalar deltaEpsInternal Spectrum.unitarySpectrum
  , objective    = TSig3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0
    deltaEpsInternal = 1.412625
    deltaSig = 0.51820


ttssFeasibleSearchGaps :: Rational -> V 2 Rational -> Int -> TSig3d
ttssFeasibleSearchGaps deltaSig v nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, dEven)
    , (z2EvenParityOddScalar,  dOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSig3d.Feasibility Nothing
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0
    (dEven,dOdd) = fromV v

ttssCTBoundGaps :: Rational -> V 2 Rational -> BoundDirection -> Int -> TSig3d
ttssCTBoundGaps deltaSig lambda direction nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 1.2)
    , (z2EvenParityOddScalar,  3)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSig3d.StressTensorOPEBound lambda direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0

ttssCTOddIsingRelaxedGaps :: Rational -> V 2 Rational -> BoundDirection -> Int -> TSig3d
ttssCTOddIsingRelaxedGaps deltaOdd lambda direction nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [
     (z2EvenParityOddScalar,  deltaOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSig3d.StressTensorOPEBound lambda direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0
    deltaSig = 0.51820



ttssCTIsingGaps :: Rational -> (V 2 Rational) -> BoundDirection -> Int -> TSig3d
ttssCTIsingGaps dOdd lambdaTTT direction nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3)
    , (z2OddParityEvenScalar, 3)
    , (z2EvenParityOddScalar, dOdd)
--    , (z2EvenParityEvenSpin2, 4)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.addIsolated z2EvenParityEvenScalar deltaEpsInternal Spectrum.unitarySpectrum
  , objective    = TSig3d.StressTensorOPEBound lambdaTTT direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0
    deltaEpsInternal = 1.4126250292781875
    deltaSig = 0.51814879347865339


ttssCTIsingRelaxedGaps :: Rational -> (V 2 Rational) -> BoundDirection -> Int -> TSig3d
ttssCTIsingRelaxedGaps dOdd lambdaTTT direction nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 1.2)
    , (z2EvenParityOddScalar, dOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSig3d.StressTensorOPEBound lambdaTTT direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0
    deltaSig = 0.51820

-- keep this as a souvenir of the zeros of the minors of our matrix
ttssCrazyGaps :: Rational -> Maybe (V 2 Rational) -> Int -> TSig3d
ttssCrazyGaps deltaSig mLambda nmax = TSig3d
  { spectrum     = Spectrum.setTwistGap 193 $ Spectrum.unitarySpectrum
  , objective    = TSig3d.Feasibility mLambda
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0


tsigGffNavigatorIsingGaps :: Maybe (V 2 Rational) -> Int -> TSig3d
tsigGffNavigatorIsingGaps mLambdaTTT nmax = TSig3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3)
    , (z2OddParityEvenScalar, 3)
    , (z2EvenParityOddScalar, 3)
    , (z2EvenParityEvenSpin2, 4)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.addIsolated z2EvenParityEvenScalar deltaEpsInternal Spectrum.unitarySpectrum
  , objective    = TSig3d.TSigGffNavigator  mLambdaTTT 
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = TSigEpsSetup.ExternalDims {..}
  , numPoles     = Nothing
  }
  where
    deltaEps = 0
    deltaEpsInternal = 1.4126250292781875
    deltaSig = 0.51814879347865339 


delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

data TTSigSigDelaunaySearchData = TTSigSigDelaunaySearchData
  { nmax              :: Int
  , affine            :: AffineTransform 2 Rational
  , initialCheckpoint :: Maybe FilePath
  , initialDisallowed :: [V 2 Rational]
  , initialAllowed    :: [V 2 Rational]
  , solverPrecision   :: Int
  , delaunayConfig    :: DelaunayConfig
  , jobTime           :: NominalDiffTime
  , jobType           :: MPIJob
  , jobMemory         :: Text
  , slurmPartition    :: Text
  }

--ttssDelaunaySearch :: TTSigSigDelaunaySearchData -> Cluster ()
--ttssDelaunaySearch t =
--  local (setJobType t.jobType . setJobTime t.jobTime . setJobMemory t.jobMemory . setSlurmPartition t.slurmPartition) $ void $ do
--  checkpointMap <- Bound.newCheckpointMap t.affine boundToScalarGapV t.initialCheckpoint
--  let
--    f v =  do
--      result <- Bound.remoteComputeWithCheckpointMap checkpointMap (bound v)
--      return (SDPB.isPrimalFeasible result)
--  delaunaySearchRegionPersistent delaunaySearchPoints t.delaunayConfig t.affine initialPts f
--  where
--    bound v = Bound
--      { boundKey = ttssFeasibleSearchGaps v t.nmax
--      , precision = t.solverPrecision
--      , solverParams = (Params.jumpFindingParams t.nmax) { SDPB.precision = t.solverPrecision }
--      , boundConfig = defaultBoundConfig
--      }
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- t.initialAllowed] ++
--      [(p, Just False) | p <- t.initialDisallowed]

boundsProgram :: Text -> Cluster ()

-----------------
--- nmax = 6 ----
-----------------

-- | Make sure you have the right checksum! And then you won't have to call this :)
-- | here's an sha256 checksum:
-- | 46a742ca0d76901b197874f54d19b9528828462c75f574256592b6ca09ac73da  ttss_blocks_nmax6.tar.zst
boundsProgram "TTSigSig_build_blocks_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (16*hour) . setJobMemory "50G" ) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [(0.5, toV (7,7))
  ]
  where
    nmax = 6
    bound (dSig, lambda) = Bound
      { boundKey = ttssFeasibleDefaultGaps dSig (Just lambda) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_crazy_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (16*hour) . setJobMemory "50G" ) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [(1, toV (7,7))
  ]
  where
    nmax = 6
    bound (dSig, lambda) = Bound
      { boundKey = ttssCrazyGaps dSig (Just lambda) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_test_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (16*hour) . setJobMemory "50G" ) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (1, toV (1,1))
  ]
  where
    nmax = 6
    bound (dSig, lambda) = Bound
      { boundKey = ttssFeasibleDefaultGaps dSig (Just lambda) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_gap_test_nmax6" =
  local (setJobType (MPIJob 1 24) . setJobTime (2*hour) . setJobMemory "20G" ) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [(s,x,y) | s <- [0.5178], x <- [0.6], y <- [0.5,1]
  ]
  where
    nmax = 6
    bound (dSig, deven, dodd) = Bound
      { boundKey = ttssFeasibleTestGaps dSig (deven, dodd) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 768, SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_gap_grid_nmax6" =
  local (setJobType (MPIJob 1 24) . setJobTime (8*hour) . setJobMemory "100G"  ) $
  mapConcurrently_ f
  [(s,x,y) | s <- [0.5180,0.5182..0.5188], x <- [0.6,0.8..2], y <- [0.5,1..8]
  ]
  where
    gridDB :: DB.KeyValMap (V 3 Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f p = do
      out <- Bound.remoteCompute (bound p)
      Log.info "point finished" out
      DB.insert gridDB (toV p) (SDPB.isPrimalFeasible out)
      return ()
    nmax = 6
    bound (dSig, deven, dodd) = Bound
      { boundKey = ttssFeasibleTestGaps dSig (deven, dodd) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 768, SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "TTSigSig_test_functional_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (2*hour) . setJobMemory "50G" ) $ do
  (_, boundFiles) <- Bound.remoteComputeKeepFiles bound
  let
    myJob = reifyPrecision bound.precision $ \(_ :: Proxy p) -> do
      functional :: Vector (Bound.BigFloat p) <- Bound.getBoundFunctional boundFiles
      Log.info "functional" functional
      DB.insert funcDB bound functional
  pInfo <- asks toProgramInfo
  staticConfig <- asks clusterStaticConfig
  liftIO $ runJobLocal staticConfig pInfo myJob
  where
    funcDB = DB.KeyValMap "functional"
    nmax = 6
    dSig = 1
    bound = Bound
      { boundKey = ttssFeasibleDefaultGaps dSig (Just $ toV (1,1)) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 1024, SDPB.dualErrorThreshold = 1e-50 , SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_functional_test_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (5*hour) . setJobMemory "100G" ) $
  mapConcurrently_ (\thresh -> do
      (_, boundFiles) <- Bound.remoteComputeKeepFiles (bound thresh)
      let
        myJob = reifyPrecision (bound thresh).precision $ \(_ :: Proxy p) -> do
          functional :: Vector (Bound.BigFloat p) <- Bound.getBoundFunctional boundFiles
          Log.info "functional" functional
          DB.insert funcDB thresh functional
      pInfo <- asks toProgramInfo
      staticConfig <- asks clusterStaticConfig
      liftIO $ runJobLocal staticConfig pInfo myJob)
    [ thresh | thresh <- [1e-50,1e-60, 1e-70]
    ]
  where
    funcDB = DB.KeyValMap "functional"
    nmax = 6
    bound thresh  = Bound
      { boundKey = ttssFeasibleTestGaps 0.51814 (5, 5) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax)
        { SDPB.precision = 768
        , SDPB.dualityGapThreshold = 1e-200
        , SDPB.findPrimalFeasible = False
        , SDPB.dualErrorThreshold = thresh  }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_OPE_test_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (24*hour) . setJobMemory "100G") $
  mapConcurrently_  (Bound.remoteCompute . bound)
  [(0.51814, OPE.thetaVectorApprox 1e-16 x) | x <- [approxRational 1e-16 (pi/8 * n) | n <-[0..4 :: Double]]
  ]
  where
    nmax = 6
    bound (dSig, lambda) = Bound
      { boundKey = ttssFeasibleDefaultGaps dSig (Just lambda) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "TTSigSig_OPE_Ising_Relaxed_test_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (48*hour) . setJobMemory "100G") $
  mapConcurrently_ f
  [(dir, dOdd, approxRational (pi/8 * n) 1e-16 )| dir <- [UpperBound,LowerBound], dOdd <- [7], n <-[1,1.5..3 :: Double]
  ]
  where
    gridDB :: DB.KeyValMap (BoundDirection, Rational, Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f (direction, dOdd, theta) = do
      out <- Bound.remoteCompute (bound direction (dOdd, theta))
      Log.info "point finished" out
      DB.insert gridDB (direction, dOdd, theta) (SDPB.isPrimalFeasible out)
      return ()
    nmax = 6
    bound dir (dOdd, theta) = Bound
      { boundKey = ttssCTIsingRelaxedGaps dOdd lambda dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = let v = ( OPE.thetaVectorApprox 1e-16 theta) in ( 1/(sum v) V.*^ v)


boundsProgram "TTSigSig_OPE_Ising_Odd_Relaxed_test_nmax6" =
  local (setJobType (MPIJob 2 128) . setJobTime (48*hour) . setJobMemory "100G") $
  mapConcurrently_ f
  [(dir, dOdd, approxRational (pi/8 * n) 1e-16 )| dir <- [UpperBound,LowerBound], dOdd <- [10], n <-  [0.1,0.11..0.14:: Double]
  ]
  where
    gridDB :: DB.KeyValMap (BoundDirection, Rational, Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f (direction, dOdd, theta) = do
      out <- Bound.remoteCompute (bound direction dOdd theta)
      Log.info "point finished" out
      DB.insert gridDB (direction, dOdd, theta) (SDPB.isPrimalFeasible out)
      return ()
    nmax = 6
    bound dir dOdd theta = Bound
      { boundKey = ttssCTOddIsingRelaxedGaps dOdd lambda dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = let v = ( OPE.thetaVectorApprox 1e-16 theta) in ( 1/(sum v) V.*^ v)


boundsProgram "TTSigSig_OPE_Ising_test_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G") $
  mapConcurrently_ f
  [(dir, dOdd, approxRational (pi/8 * n) 1e-16 )| dir <- [UpperBound], dOdd <- [3], n <-[2.1 :: Double]
  ]
  where
    gridDB :: DB.KeyValMap (BoundDirection, Rational, Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f (direction, dOdd, theta) = do
      out <- Bound.remoteCompute (bound direction (dOdd, theta))
      Log.info "point finished" out
      DB.insert gridDB (direction, dOdd, theta) (SDPB.isPrimalFeasible out)
      return ()
    nmax = 6
    paramNmax = 8
    bound dir (dOdd, theta) = Bound
      { boundKey = (ttssCTIsingGaps dOdd lambda dir paramNmax)
         { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
         , spins        = Params.gnyspinsNmax paramNmax
         }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = let v = ( OPE.thetaVectorApprox 1e-16 theta) in ( 1/(sum v) V.*^ v)


boundsProgram "TTSigSig_CT_test_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (4*hour) . setJobMemory "50G" )$
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (0.51814, toV (3,3))
  ]
  where
    nmax = 6
    bound (dSig, lambdaTTT) = Bound
      { boundKey = ttssCTBoundGaps dSig lambdaTTT LowerBound nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_CT_functional_test_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (5*hour) . setJobMemory "100G" ) $
  mapConcurrently_ (\(dir,thresh) -> do
      (_, boundFiles) <- Bound.remoteComputeKeepFiles (bound (dir,thresh))
      let
        myJob = reifyPrecision (bound (dir,thresh)).precision $ \(_ :: Proxy p) -> do
          functional :: Vector (Bound.BigFloat p) <- Bound.getBoundFunctional boundFiles
          Log.info "functional" functional
          DB.insert funcDB (dir,thresh) functional
      pInfo <- asks toProgramInfo
      staticConfig <- asks clusterStaticConfig
      liftIO $ runJobLocal staticConfig pInfo myJob)
    [ (dir,thresh) | dir <- [UpperBound,LowerBound], thresh <- [1e-50,1e-55,1e-60,1e-65,1e-70,1e-75,1e-80]
    ]
  where
    funcDB = DB.KeyValMap "functional"
    nmax = 6
    bound (dir,thresh)  = Bound
      { boundKey = ttssCTBoundGaps 0.51814 (toV (3,3)) dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
        { SDPB.precision = 768
        , SDPB.dualityGapThreshold = 1e-200
        , SDPB.findDualFeasible = True
        , SDPB.dualErrorThreshold = thresh  }
      , boundConfig = defaultBoundConfig
      }





boundsProgram "TSig_GFFNavigator_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G") $
  mapConcurrently_  (Bound.remoteCompute . bound)
    [ Just $ toV (1.06327271904675, 0.014915290376899 )]
  where
    nmax = 6
    paramNmax = nmax+2
    bound mLambda = Bound
      { boundKey = (tsigGffNavigatorIsingGaps mLambda paramNmax )
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 } --{ B3d.nmax = nmax}
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.optimizationParams paramNmax) { SDPB.precision = 1024
                                                             , SDPB.maxRuntime = SDPB.RunForDuration (24 * 3600)
                                      --                       , SDPB.maxSharedMemory = Just "25G"
                                                             }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTSigSig_OPE_Ising_test_nmax18" =
  local (setJobType (MPIJob 5 128) . setJobTime (24*hour) . setJobMemory "0G") $
  mapConcurrently_ f
  [(dir, dOdd, approxRational (pi/8 * n) 1e-16 )| dir <- [UpperBound], dOdd <- [3], n <-[2.1 :: Double]
  ]
  where
    gridDB :: DB.KeyValMap (BoundDirection, Rational, Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f (direction, dOdd, theta) = do
      out <- Bound.remoteCompute (bound direction (dOdd, theta))
      Log.info "point finished" out
      DB.insert gridDB (direction, dOdd, theta) (SDPB.isPrimalFeasible out)
      return ()
    nmax = 18
    paramNmax = nmax+4
    bound dir (dOdd, theta) = Bound
      { boundKey = (ttssCTIsingGaps dOdd lambda dir paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax = nmax}
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024
                                                             , SDPB.maxRuntime = SDPB.RunForDuration (24 * 3600)
                                                             , SDPB.maxSharedMemory = Just "25G"
                                                        } 
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = let v = ( OPE.thetaVectorApprox 1e-16 theta) in ( 1/(sum v) V.*^ v)


boundsProgram "TSig_GFFNavigator_nmax18" =
  local (setJobType (MPIJob 5 128) . setJobTime (48*hour) . setJobMemory "0G") $
  mapConcurrently_  (Bound.remoteCompute . bound)
    [ Just $ toV (1.06327271904675, 0.014915290376899 )]
  where
    nmax = 18
    paramNmax = nmax+4
    bound mLambda = Bound
      { boundKey = (tsigGffNavigatorIsingGaps mLambda paramNmax )
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax = nmax, B3d.keptPoleOrder = 56}
        , spins        = Params.gnyspinsNmax 22
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.optimizationParams paramNmax) { SDPB.precision = 1536
                                                             , SDPB.maxRuntime = SDPB.RunForDuration (48 * 3600)
                                                             , SDPB.maxSharedMemory = Just "25G"
                                                             }
      , boundConfig = defaultBoundConfig
      }

--boundsProgram "TSig_ExternalOPEFormBound_off_diagonal_nmax18" =
--  local (setJobType (MPIJob 5 128) . setJobTime (24*hour) . setJobMemory "0") $ do
--  (_,files) <- Bound.remoteComputeKeepFiles bound
--  logExtMatLocal bound files
--  where
--    nmax = 18
--    paramNmax = nmax+4
--    deltaEps = 0
--    deltaEpsInternal = 1.4126250292781875
--    deltaSig = 0.51814879347865339
--    lambdaMat = toM ((0,0,1,0,0)
--                    ,(0,0,0,0,0)
--                    ,(1,0,0,0,0)
--                    ,(0,0,0,0,0)
--                    ,(0,0,0,0,0))
--    bound = Bound
--      { boundKey = TSig3d
--          { spectrum     = Spectrum.setGaps
--            [ (z2EvenParityEvenScalar, 3)
--            , (z2OddParityEvenScalar, 3)
--            , (z2EvenParityOddScalar, 3)
--            , (z2EvenParityEvenSpin2, 4)
--            ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.addIsolated z2EvenParityEvenScalar deltaEpsInternal Spectrum.unitarySpectrum
--          , objective    = TSig3d.ExternalOPEFormBound lambdaMat 
--          , blockParams  = Params.block3dParamsNmax nmax
--          , spins        = Params.gnyspinsNmax nmax
--          , externalDims = TSigEpsSetup.ExternalDims {..}
--          }
--      , precision = (Params.block3dParamsNmax paramNmax).precision
--      , solverParams = (Params.optimizationParams paramNmax) { SDPB.precision = 1024 
--                                                             , SDPB.maxRuntime = SDPB.RunForDuration (24 * 3600)
--                                                             , SDPB.maxSharedMemory = Just "25G" 
--                                                             }
--      , boundConfig = defaultBoundConfig
--      }

--
--boundsProgram "TTTT_fixed_OPE_test_nmax6" =
--  local (setJobType (MPIJob 1 18) . setJobTime (4*hour) . setJobMemory "30G" . setSlurmPartition "pi_poland,scavenge") $
--  void $ (Bound.remoteCompute bound)
--  where
--    nmax = 10
--    bound = Bound
--      { boundKey = ttttFixedOPE (7,7) (toV (1,1)) nmax
--      , precision = (Params.block3dParamsNmax nmax).precision
--      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "TTTT_delaunay_nmax6" =
--  ttttDelaunaySearch $
--    TTTTDelaunaySearchData
--    { nmax              = 6
--    , affine            = AffineTransform
--                            { affineShift = toV (4.25,7.25)
--                            , affineLinear = toV  ( toV (3.75, 0)
--                                                  , toV (0, 6.75) )
--                            }
--    , initialCheckpoint = Nothing
--    , initialDisallowed = toV <$> [(7,7),(8,10),(8,3), (2,10),(8,1/2)]
--    , initialAllowed    = toV <$> [(1/2,1/2),(3,3),(6,7),(1,11),(6,2),(2,7)]
--    , solverPrecision   = 768
--    , delaunayConfig    = defaultDelaunayConfig 8 200
--    , jobTime           = 3*hour
--    , jobType           = MPIJob 1 9
--    , jobMemory         = "30G"
--    , slurmPartition    = "pi_poland,scavenge"
--    }
--
--------------------
----- nmax = 10 ----
--------------------
--
---- | 23787d361eedf179cccac59358e04ba161d55e805546c6b5868bf753d354cc41  tttt_blocks_nmax10.tar.zst
--boundsProgram "TTTT_test_nmax10" =
--  local (setJobType (MPIJob 4 36) . setJobTime (4*hour) . setJobMemory "0G" . setSlurmPartition "pi_poland") $
--  mapConcurrently_ (Bound.remoteCompute . bound)
--  [ toV (3,3)
--  ]
--  where
--    nmax = 10
--    bound lambdaTTT = Bound
--      { boundKey = ttttFeasibleDefaultGaps (Just lambdaTTT) nmax
--      , precision = (Params.block3dParamsNmax nmax).precision
--      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "TTTT_CT_functional_test_nmax10" =
--  local (setJobType (MPIJob 1 28) . setJobTime (8*hour) . setJobMemory "100G" . setSlurmPartition "pi_poland,scavenge") $
--  mapConcurrently_ (\(dir,thresh) -> do
--      (out, boundFiles) <- Bound.remoteComputeKeepFiles (bound (dir,thresh))
--      let
--        myJob = untag @Job $
--          Bound.reifyBoundWithContext (bound (dir,thresh)) $ \bound' -> do
--          functional <- Bound.getBoundFunctional bound' boundFiles
--          Log.info "functional" functional
--          DB.insert funcDB (dir,thresh) functional
--      pInfo <- asks toProgramInfo
--      staticConfig <- asks clusterStaticConfig
--      liftIO $ runJobLocal staticConfig pInfo myJob)
--    [ (dir,thresh) | dir <- [UpperBound,LowerBound], thresh <- [1e-50,1e-55,1e-60,1e-65,1e-70,1e-75,1e-80]
--    ]
--  where
--    funcDB = DB.KeyValMap "functional"
--    nmax = 10
--    bound (dir,thresh)  = Bound
--      { boundKey = ttttCTBoundGaps (toV (1,1)) dir nmax
--      , precision = (Params.block3dParamsNmax nmax).precision
--      , solverParams = (Params.optimizationParams nmax)
--        { SDPB.precision = 1024
--        , SDPB.dualityGapThreshold = 1e-200
--        , SDPB.findDualFeasible = True
--        , SDPB.dualErrorThreshold = thresh  }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "TTTT_gap_test_nmax10" =
--  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "30G" . setSlurmPartition "scavenge") $ do
--    checkpointMap <- Bound.newCheckpointMap affine deltaExtToV $ Just "/vast/palmer/scratch/poland/rse23/hyperion/data/2023-05/RBwBJ/Object_b8mbkoCsUcrG9erhTI9Me3swFMLOQz3pM4LbIZOS9TI/ck"
--    let
--      f p = do
--        out <- Bound.remoteComputeWithCheckpointMap checkpointMap (bound p)
--        Log.info "point finished" out
--        DB.insert gridDB (toV p) (SDPB.isPrimalFeasible out)
--        return ()
--    mapConcurrently_ f
--      [ (x,y) | x <- [0.6,0.8..8], y <- [0.6,0.8..12] ]
--  where
--    affine = AffineTransform
--      { affineShift = toV @1 (0)
--      , affineLinear = toV @1 ( toV @1 (1) )
--      }
--    deltaExtToV _ = toV @1 1
--    gridDB :: DB.KeyValMap (V 2 Rational) Bool
--    gridDB = DB.KeyValMap "gridPoints"
--    nmax = 10
--    bound deltas = Bound
--      { boundKey = ttttFeasibleTestGaps deltas nmax
--      , precision = (Params.block3dParamsNmax nmax).precision
--      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 768, SDPB.findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "TTTT_OPE_test_nmax10" =
--  local (setJobType (MPIJob 1 28) . setJobTime (24*hour) . setJobMemory "100G") $
--  mapM_ (Bound.remoteCompute . bound)
--  [ toV (1,1)
--  ]
--  where
--    nmax = 10
--    bound lambdaTTT = Bound
--      { boundKey = ttttFeasibleDefaultGaps (Just lambdaTTT) nmax
--      , precision = (Params.block3dParamsNmax nmax).precision
--      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "TTTT_delaunay_nmax10" =
--  ttttDelaunaySearch $
--    TTTTDelaunaySearchData
--    { nmax              = 10
--    , affine            = AffineTransform
--                            { affineShift = toV (4.25,7.25)
--                            , affineLinear = toV  ( toV (3.75, 0)
--                                                  , toV (0, 6.75) )
--                            }
--    , initialCheckpoint = Nothing
--    , initialDisallowed = toV <$> [(1/2,12),(1,12),(2,12),(2,11),(2,10),(2,9),(3,9),(4,9),(5,9),(6,9),(7,9),(7,8),(7,7),(7,6),(7,5),(7,4),(7,3),(7,2),(8,2),(8,1),(8,1/2)]
--    , initialAllowed    = toV <$> [(1/2,11),(1,11),(1,10),(1,9),(1,8),(1,7),(2,7),(3,7),(4,7),(5,7),(6,7),(6,6),(6,5),(6,4),(6,3),(6,2),(6,1),(6,1/2)]
--    , solverPrecision   = 768
--    , delaunayConfig    = defaultDelaunayConfig 8 200
--    , jobTime           = 3*hour
--    , jobType           = MPIJob 1 16
--    , jobMemory         = "30G"
--    , slurmPartition    = "pi_poland,scavenge"
--    }
--
--
------------------
----- nmax=14 ----
------------------
--
--boundsProgram "TTTT_test_nmax14" =
--  local (setJobType (MPIJob 20 48) . setJobTime (1*day) . setJobMemory "0G" . setSlurmPartition "scavenge") $
--  mapConcurrently_ (Bound.remoteCompute . bound)
--  [ (2/3,2/3)
--  ]
--  where
--    nmax = 14
--    bound deltas = Bound
--      { boundKey = (ttttFeasibleTestGaps deltas nmax) { spins = Params.gnyspinsNmax 18 }
--      , precision = (Params.block3dParamsNmax nmax).precision
--      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "TTTT_delaunay_nmax14" =
--  ttttDelaunaySearch $
--    TTTTDelaunaySearchData
--    { nmax              = 14
--    , affine            = AffineTransform
--                            { affineShift = toV (4.25,7.25)
--                            , affineLinear = toV  ( toV (3.75, 0)
--                                                  , toV (0, 6.75) )
--                            }
--    , initialCheckpoint = Nothing
--    , initialDisallowed = toV <$> [(1, 12), (2, 10), (2, 11), (2, 12), (2, 9), (3, 9), (4, 9), (5, 9), (1/2, 12), (6, 9), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (8, 1), (8, 2), (8, 1/2), (13/2, 29/4), (13/2, 13/4), (19/4, 8), (23/4, 8), (11/4, 8), (15/4, 8), (13/2, 17/4), (3/2, 43/4), (13/2, 23/4), (51/8, 37/16), (35/16, 31/4), (45/8, 15/2), (33/8, 15/2), (29/8, 15/2), (25/4, 25/4), (175/64, 119/16), (151/32, 59/8), (25/4, 31/8), (59/8, 13/16), (55/32, 623/64), (25/4, 45/8), (25/4, 35/8), (115/64, 137/16), (447/256, 4615/512), (99/16, 105/32), (923/128, 133/256), (187/32, 59/8), (163/32, 59/8), (209/32, 121/64), (99/32, 59/8), (99/16, 55/8), (14747/2048, 2053/4096), (1027/512, 977/128), (99/16, 77/32), (201/32, 33/16), (235931/32768, 32773/65536), (223/32, 73/64), (23/16, 361/32), (433/64, 201/128), (3774875/524288, 524293/1048576), (233329/32768, 49519/65536), (60397979/8388608, 8388613/16777216), (403/256, 4987/512), (391/64, 485/128), (1201/512, 953/128), (1547/1024, 20403/2048), (1675/1024, 17959/2048), (966367643/134217728, 134217733/268435456), (391/64, 201/32), (6891/4096, 68391/8192), (61/16, 15/2), (6503/4096, 74531/8192), (49/8, 65/16), (15461882267/2147483648, 2147483653/4294967296), (49/8, 191/32), (315/512, 1517/128), (49/8, 89/16), (49/8, 71/16), (783/128, 271/128), (1603/1024, 19499/2048), (247390116251/34359738368, 34359738373/68719476736), (49/8, 5), (779/128, 229/32), (2317/512, 961/128), (3958241859995/549755813888, 549755813893/1099511627776), (4437/2048, 483/64), (63331869759899/8796093022208, 8796093022213/17592186044416), (3113/512, 1753/256), (1013309916158363/140737488355328, 140737488355333/281474976710656), (1555/256, 1673/512), (195/32, 187/64)]
--    , initialAllowed    = toV <$> [(1/2,11),(1,11),(1,10),(1,9),(1,8),(1,7),(2,7),(3,7),(4,7),(5,7),(6,7),(6,6),(6,5),(6,4),(6,3),(6,2),(6,1),(6,1/2)]
--    , solverPrecision   = 768
--    , delaunayConfig    = defaultDelaunayConfig 4 200
--    , jobTime           = 6*hour
--    , jobType           = MPIJob 1 36
--    , jobMemory         = "70G"
--    , slurmPartition    = "pi_poland,scavenge"
--    }

boundsProgram p = unknownProgram p
