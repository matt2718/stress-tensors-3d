{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Programs.OPEScanNmax26 where

import Bootstrap.Math.Linear                      (toV)
import Control.Monad.Reader                       (local)
import Data.Text                                  (Text)
import Hyperion                                   (Cluster, MPIJob (..),
                                                   setJobMemory, setJobTime,
                                                   setJobType)
import Hyperion.Bootstrap.Main                    (unknownProgram)
import Hyperion.Util                              (hour)
import StressTensors3d.OPEScan                    qualified as OPEScan
import StressTensors3d.Programs.Defaults          (defaultDelaunayConfig,
                                                   taskStatsDir)
import StressTensors3d.Programs.OPEScanNmax22Data (disallowedPointsNmax22,
                                                   isingTSigEpsAffineNmax22,
                                                   opeEllipseNmax22)
import StressTensors3d.Programs.OPEScanNmax26Data (allowedPointsNmax26, testIntervalsPKNmax26)
import StressTensors3d.Programs.TSigEpsOPEScan    (TSigEpsDelaunayConfig (..),
                                                   TSigEpsOPEScanConfig (..),
                                                   TSigEpsParams (..),
                                                   TSigEpsStarSearchConfig (..),
                                                   tSigEpsDelaunaySearch,
                                                   tSigEpsOPEScan,
                                                   runStarSearch)
import System.FilePath.Posix                      ((</>))

paramsNmax26 :: TSigEpsParams
paramsNmax26 = MkTSigEpsParams
  { maxSpin   = 100
  , order     = 152
  , numPoles  = 35
  , precision = 896
  , nmax      = 26
  }

initialBilinearFormsNmax26 :: OPEScan.BilinearForms 5
initialBilinearFormsNmax26 = OPEScan.BilinearForms 1e-32 [(Nothing, opeEllipseNmax22)]

opeScanConfigNmax26 :: TSigEpsOPEScanConfig
opeScanConfigNmax26 = MkTSigEpsOPEScanConfig
  { initialBilinearForms = initialBilinearFormsNmax26
  , initialLambda        = Just (snd (head allowedPointsNmax26))
  , affine               = isingTSigEpsAffineNmax22
  , params               = paramsNmax26
  , initialCheckpoint    = initialCheckpointFeasibleNmax26
  , statFiles            = statFilesNmax26
  }

starSearchConfigPKNmax26 :: TSigEpsStarSearchConfig
starSearchConfigPKNmax26 = TSigEpsStarSearchConfig
  { opeScanConfig       = opeScanConfigNmax26
  , bisections          = 5
  , intervals           = testIntervalsPKNmax26
  , maxSimultaneousJobs = Nothing
  }

initialCheckpointFeasibleNmax26 :: Maybe FilePath
initialCheckpointFeasibleNmax26 = Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-12/VUNMG/Object_oPB3Txe8gbFLpa2nUfUP09Pq4qTCzbfiwzHi2y0x12s/ck"

statFilesNmax26 :: [FilePath]
statFilesNmax26 = [taskStatsDir </> "TSigEps_nmax_26_feasible_point_test_collected_stats.json"]

tSigEpsDelaunayConfigNmax26 :: TSigEpsDelaunayConfig
tSigEpsDelaunayConfigNmax26 = MkTSigEpsDelaunayConfig
  { opeScanConfig           = opeScanConfigNmax26
  , initialAllowedPoints    = map fst allowedPointsNmax26
  , initialDisallowedPoints = disallowedPointsNmax22
  , initialTestPoints       = []
  , delaunayConfig          = defaultDelaunayConfig 4 200
  }

setJobNmax26 :: Cluster a -> Cluster a
setJobNmax26 = local (setJobType (MPIJob 16 128) . setJobTime (20*hour) . setJobMemory "0G")

setJobPKNmax26 :: Cluster a -> Cluster a
setJobPKNmax26 = local (setJobType (MPIJob 16 128) . setJobTime (30*hour) . setJobMemory "0G")

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_nmax_26_feasible_point_test" =
  setJobNmax26 $
  tSigEpsOPEScan opeScanConfigNmax26 $
  map toV
  [ (0.5181488047155694554035676, 1.412625265637597937740111)
  , (0.5181488051197578048601144, 1.412625265605242041999645)
  , (0.5181488043113811059470208, 1.412625265669953833480577) -- disallowed
  ]

boundsProgram "TSigEps_nmax_26_PK_star_search" =
  setJobNmax26 $
  runStarSearch starSearchConfigPKNmax26

boundsProgram "TSigEps_nmax_26_delaunay_search" =
  setJobNmax26 $
  tSigEpsDelaunaySearch tSigEpsDelaunayConfigNmax26

boundsProgram p = unknownProgram p
