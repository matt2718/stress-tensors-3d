{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Programs.OPEScanNmax22 where

import Bootstrap.Bounds                           (BoundDirection (..))
import Control.Monad.Reader                       (local)
import Data.Text                                  (Text)
import Hyperion                                   (Cluster, MPIJob (..),
                                                   setJobMemory, setJobTime,
                                                   setJobType)
import Hyperion.Bootstrap.Main                    (unknownProgram)
import Hyperion.Util                              (hour)
import Linear.V                                   (V)
import SDPB qualified
import StressTensors3d.OPEScan                    qualified as OPEScan
import StressTensors3d.Programs.Defaults          (defaultDelaunayConfig)
import StressTensors3d.Programs.OPEScanNmax18Data (disallowedPointsNmax18,
                                                   opeEllipseNmax18)
import StressTensors3d.Programs.OPEScanNmax22Data (allowedPointsNmax22,
                                                   disallowedPointsNmax22,
                                                   isingTSigEpsAffineNmax22)
import StressTensors3d.Programs.TSigEpsOPEScan    (OpeMinMax (..),
                                                   TSigEpsDelaunayConfig (..),
                                                   TSigEpsOPEScanConfig (..),
                                                   TSigEpsParams (..),
                                                   runOpeMinMax,
                                                   tSigEpsDelaunaySearch,
                                                   tSigEpsOPEBound,
                                                   tSigEpsOPEScan)
import StressTensors3d.TSigEps3d                  (TSigEps3d (..))

paramsNmax22 :: TSigEpsParams
paramsNmax22 = MkTSigEpsParams
  { maxSpin   = 90
  , order     = 130
  , numPoles  = 35
  , precision = 832
  , nmax      = 22
  }

initialBilinearFormsNmax22 :: OPEScan.BilinearForms 5
initialBilinearFormsNmax22 = OPEScan.BilinearForms 1e-32 [(Nothing, opeEllipseNmax18)]

opeScanConfigNmax22 :: TSigEpsOPEScanConfig
opeScanConfigNmax22 = MkTSigEpsOPEScanConfig
  { initialBilinearForms = initialBilinearFormsNmax22
  , initialLambda        = Just $ snd $ head allowedPointsNmax22
  , affine               = isingTSigEpsAffineNmax22
  , params               = paramsNmax22
  , initialCheckpoint    = initialCheckpointFeasibleNmax22
  , statFiles            = statFilesNmax22
  }

tSigEpsDelaunayConfigNmax22 :: TSigEpsDelaunayConfig
tSigEpsDelaunayConfigNmax22 = MkTSigEpsDelaunayConfig
  { opeScanConfig           = opeScanConfigNmax22
  , initialAllowedPoints    = fmap fst allowedPointsNmax22
  , initialDisallowedPoints = disallowedPointsNmax18 <> disallowedPointsNmax22
  , initialTestPoints       = []
  , delaunayConfig          = defaultDelaunayConfig 4 200
  }

defaultOpeMinMaxNmax22 :: OpeMinMax (V 2 Rational, V 5 Rational) TSigEps3d
defaultOpeMinMaxNmax22 = MkOpeMinMax
  { opeBound                 = tSigEpsOPEBound paramsNmax22
  , maxSimultaneousJobs      = Just 4
  , directionsAndCheckpoints = [(LowerBound, Nothing), (UpperBound, Nothing)]
  , statFiles                = statFilesNmax22
  , modifyParams             = id
  , modifyBoundFiles         = id
  , centeringIterations      = Nothing
  , allowedPoints            = []
  , dualityGap               = 1e-45
  }

initialCheckpointFeasibleNmax22 :: Maybe FilePath
initialCheckpointFeasibleNmax22 = Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-08/AFTlh/Object_ao7LHSwzLTJFMxF0sy2Hz0yIYz0bt9QGnokZiYzXvOE/ck"

statFilesNmax22 :: [FilePath]
statFilesNmax22 = ["/home/dsd/projects/stress-tensors-3d/task_stats/TSigEps_nmax_22_feasible_point_test_collected_stats.json"]

directionsAndCheckpoints_mu1e8 :: [(BoundDirection, Maybe FilePath)]
directionsAndCheckpoints_mu1e8 =
  [ (LowerBound, Just "/expanse/lustre/scratch/dsd/temp_project/checkpoints/nmax_22_mu_1e8_lower_bound_ck")
  , (UpperBound, Just "/expanse/lustre/scratch/dsd/temp_project/checkpoints/nmax_22_mu_1e8_upper_bound_ck")
  ]

setJobNmax22 :: Cluster a -> Cluster a
setJobNmax22 = local (setJobType (MPIJob 16 128) . setJobTime (20*hour) . setJobMemory "0G")

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_nmax_22_feasible_point_test" =
  setJobNmax22 $
  tSigEpsOPEScan opeScanConfigNmax22 (map fst (take 1 allowedPointsNmax22))

boundsProgram "TSigEps_nmax_22_delaunay_search" =
  setJobNmax22 $
  tSigEpsDelaunaySearch tSigEpsDelaunayConfigNmax22

-- | Generate checkpoints with mu~1e8. WARNING: You should kill the
-- master after getting this job started, otherwise it will resubmit
-- new jobs after this finishes, and continue to decrease mu.
boundsProgram "TSigEps_nmax22_ope_bounds_generate_checkpoints_mu1e8" =
  setJobNmax22 $
  runOpeMinMax $ defaultOpeMinMaxNmax22
  { directionsAndCheckpoints = [(LowerBound, Nothing), (UpperBound, Nothing)]
  , modifyParams             = \p -> p { SDPB.maxIterations = 240 }
  , allowedPoints            = take 1 allowedPointsNmax22
  }

-- | Test the checkpoints with mu=1e8 for both upper and lower bounds,
-- with 5 centering iterations at the end.
boundsProgram "TSigEps_nmax22_ope_bounds_checkpoint_1e8_with_centering" =
  setJobNmax22 $
  runOpeMinMax $ defaultOpeMinMaxNmax22
  { directionsAndCheckpoints = directionsAndCheckpoints_mu1e8
  , centeringIterations      = Just 5
  , allowedPoints            = drop 1 allowedPointsNmax22
  }


boundsProgram p = unknownProgram p
