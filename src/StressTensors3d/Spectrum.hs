{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE NoFieldSelectors    #-}
{-# LANGUAGE OverloadedRecordDot #-}

module StressTensors3d.Spectrum where

import Data.Binary     (Binary)
import Data.Scientific (Scientific)
import GHC.Generics    (Generic)
import SDPB            (makeNsvFile)
import System.Process  (callProcess)

data Params = MkParams
  { threshold     :: Scientific
  , meshThreshold :: Scientific
  , precision     :: Int
  , computeLambda :: Bool
  , verbosity     :: Int
  } deriving (Eq, Ord, Show, Generic, Binary)

defaultParams :: Params
defaultParams = MkParams
  { threshold     = 1e-8
  , meshThreshold = 1e-3
  , precision     = 1024
  , computeLambda = True
  , verbosity     = 1
  }

data Input = MkInput
  { jsonFiles   :: [FilePath]
  , solutionDir :: FilePath
  , outFile     :: FilePath
  }

run :: FilePath -> Params -> Input -> IO ()
run spectrumExecutable params input = do
  nsvFile <- makeNsvFile input.jsonFiles
  callProcess spectrumExecutable
    [ "--input",         nsvFile
    , "--output",        input.outFile
    , "--solution",      input.solutionDir
    , "--threshold",     show params.threshold
    , "--meshThreshold", show params.meshThreshold
    , "--precision",     show params.precision
    , "--lambda",        show @Int $ if params.computeLambda then 1 else 0
    , "--verbosity",     show params.verbosity
    ]
