{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module StressTensors3d.TTTT3d where

import Blocks                         (BlockFetchContext, Coordinate (XT),
                                       CrossingMat, Delta (..), Derivative (..),
                                       TaylorCoeff (..), Taylors)
import Blocks.Blocks3d                (ConformalRep (..), Parity (..))
import Blocks.Blocks3d                qualified as B3d
import Blocks.Blocks3d.Build          (block3dBuildLink)
import Blocks.Blocks3d.CompositeBlock (CompositeBlock (..),
                                       CompositeBlockKey (..),
                                       compositeBlockBuildLink,
                                       readCompositeBlockTable)
import Blocks.Blocks3d.ThreePtStruct  (ThreePtStruct)
import Bootstrap.Bounds               (BoundDirection (..),
                                       FourPointFunctionTerm,
                                       HasBlockParams (..),
                                       HasConstraintConfig (..), HasRep (..),
                                       OPECoefficient, OPECoefficientExternal,
                                       Spectrum, boundDirSign, crossingMatrix,
                                       crossingMatrixExternal,
                                       defaultConstraintConfig, listDeltas,
                                       mapBlocks, opeCoeffExternalSimple,
                                       opeCoeffIdentical_)
import Bootstrap.Bounds               qualified as Bounds
import Bootstrap.Build                (FChain (..), FetchConfig (..), HasForce,
                                       SomeBuildChain (..), noDeps)
import Bootstrap.Math.FreeVect        (FreeVect, vec)
import Bootstrap.Math.HalfInteger     (HalfInteger, pattern AnInteger,
                                       pattern EvenInteger)
import Bootstrap.Math.Linear          (toV)
import Bootstrap.Math.VectorSpace     (Tensor, mapTensor, zero, (*^), (^+^))
import Bootstrap.Math.VectorSpace     qualified as VS
import Config qualified
import Control.DeepSeq                (NFData)
import Data.Aeson                     (FromJSON, FromJSONKey, ToJSON, ToJSONKey)
import Data.Binary                    (Binary)
import Data.Data                      (Typeable)
import Data.Maybe                     (maybeToList)
import Data.Proxy                     (Proxy (..))
import Data.Reflection                (Reifies, reflect)
import Data.Tagged                    (Tagged)
import Data.Text                      qualified as Text
import Data.Vector                    qualified as Vector
import GHC.Generics                   (Generic)
import GHC.TypeNats                   (KnownNat)
import Hyperion                       (Dict (..), HyperionConfig (..),
                                       Static (..))
import Hyperion.Bootstrap.Bound       (SDPFetchBuildConfig (..), ToSDP (..),
                                       blockDir)
import Hyperion.Util.ToPath           (ToPath (..), dirScatteredHashBasePath)
import Linear.V                       (V, reifyVectorNat)
import SDPB qualified
import StressTensors3d.CrossingEqs    (HasT (..), crossingEqsStructSet,
                                       structsTTTT)
import StressTensors3d.TTOStructLabel (TTOStructLabel (..))
import System.FilePath.Posix          ((<.>), (</>))

-------------------- Setup ---------------------

data ExternalOp = T
  deriving (Show, Eq, Ord, Enum, Bounded)

instance HasT ExternalOp where
  stressTensor = T

instance HasRep ExternalOp (ConformalRep Rational) where
  rep T = ConformalRep 3 2

data SymmetrySector = SymmetrySector HalfInteger Parity
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

instance Static (Binary SymmetrySector) where
  closureDict = static Dict

symSectorLabel :: SymmetrySector -> Text.Text
symSectorLabel (SymmetrySector j parity) =
  Text.pack $ show parity <> "_" <> show j

data TTTT3d = TTTT3d
  { spectrum    :: Spectrum SymmetrySector
  , objective   :: Objective
  , spins       :: [HalfInteger]
  , blockParams :: B3d.Block3dParams
  , numPoles    :: Maybe Int
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, HasBlockParams B3d.Block3dParams)

data Objective
  = Feasibility (Maybe (V 2 Rational))
  | StressTensorOPEBound (V 2 Rational) BoundDirection
  | FeasibilityFixedOPE (V 2 Rational)
  | FeasibilityLowerBoundOPE (V 2 Rational)
  | TTeOPEBound Rational BoundDirection
  | TTeOPEBoundWithLambda Rational BoundDirection (V 2 Rational)
  | TTsOPEBound Rational BoundDirection
  | TTTTNavigatorBound (Maybe (V 2 Rational))
  -- | CTBound BoundDirection -- | convention is that UpperBound is a cT upper bound
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance HasConstraintConfig TTTTNumCrossing TTTT3d where
  type Deriv TTTT3d = TaylorCoeff (Derivative 'XT)
  getConstraintConfig = defaultConstraintConfig (Proxy @B3d.Block3dParams) crossingEqsTTTT3d

type TTOStruct = ThreePtStruct Delta TTOStructLabel
type TTTTBlock = CompositeBlock TTOStructLabel

-------------------- Crossing equations and OPEs ---------------------

type TTTTNumCrossing = 18

-- | Crossing equations for a four-point function of 3d stress tensors
crossingEqsTTTT3d
  :: forall a b . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm ExternalOp B3d.Q4Struct b a
  -> V TTTTNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqsTTTT3d = crossingEqsStructSet [((T,T,T,T), structsTTTT)]

crossingMatTTTT3d
  :: forall k j a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient ExternalOp TTOStruct a)
  -> (Tagged k `Tensor` CrossingMat j TTTTNumCrossing TTTTBlock) a
crossingMatTTTT3d ope =
  Bounds.tagVec $ mapBlocks CompositeBlock $
  crossingMatrix ope crossingEqsTTTT3d

unit :: (Fractional a, Eq a) => (Tagged k `Tensor` CrossingMat 1 TTTTNumCrossing B3d.IdentityBlock3d) a
unit = Bounds.tagVec $ B3d.identityCrossingMat crossingEqsTTTT3d

opeTTT :: (Eq a, Fractional a) => V 2 (OPECoefficientExternal ExternalOp TTOStruct a)
opeTTT =
  fmap (opeCoeffExternalSimple T T T . vec) $
  toV (StressTensorB, StressTensorF)

ext :: (Fractional a, Eq a) => (Tagged k `Tensor` CrossingMat 2 TTTTNumCrossing TTTTBlock) a
ext =
  Bounds.tagVec $ mapBlocks CompositeBlock $
  crossingMatrixExternal opeTTT crossingEqsTTTT3d [T]

opeTT
  :: (Fractional a, Eq a)
  => ConformalRep Delta
  -> Parity
  -> [OPECoefficient ExternalOp TTOStruct a]
opeTT r@(ConformalRep _ j) parity =
  map (opeCoeffIdentical_ T r . vec) $
  case (parity, j) of
    (ParityEven, 0)                    -> [ScalarParityEven]
    (ParityOdd,  0)                    -> [ScalarParityOdd]
    (ParityEven, 2)                    -> [Spin2ParityEven]
    (ParityOdd,  2)                    -> [Spin2ParityOdd]
    (ParityEven, EvenInteger l)        -> [GenericParityEven1 l, GenericParityEven2 l]
    (ParityOdd,  AnInteger l) | l >= 4 -> [GenericParityOdd l]
    _                                  -> []

gff :: (Eq a, Floating a) => (Tagged s `Tensor` CrossingMat 1 TTTTNumCrossing TTTTBlock) a
gff = VS.sum $
  map (crossingMatTTTT3d . toV . uncurry Bounds.scaleOPECoeff) opeData
  where
    dT = 3 :: Rational
    opeData =
      [ (sqrt (32/375),    opeCoeffIdentical_ T   (ConformalRep (2*dT+1) 0) (vec ScalarParityOdd))
      , (sqrt (144/4459),  opeCoeffIdentical_ T   (ConformalRep (2*dT+3) 0) (vec ScalarParityOdd))
      ]

matTTeven :: (Fractional a, Eq a) => Rational -> (Tagged k `Tensor` CrossingMat 1 TTTTNumCrossing TTTTBlock) a
matTTeven delta = crossingMatTTTT3d (toV opeTTeven)
  where
    scalarRep = ConformalRep (Fixed delta) 0
    opeTTeven = head $ opeTT scalarRep ParityEven

matTTodd :: (Fractional a, Eq a) => Rational -> (Tagged k `Tensor` CrossingMat 1 TTTTNumCrossing TTTTBlock) a
matTTodd delta = crossingMatTTTT3d (toV opeTTodd)
  where
    scalarRep = ConformalRep (Fixed delta) 0
    opeTTodd = head $ opeTT scalarRep ParityOdd

bulk
  :: forall k m a .
     ( Reifies k TTTT3d
     , Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m
     , BlockFetchContext TTTTBlock a m
     )
  => [Tagged k (SDPB.PositiveConstraint m a)]
bulk = do
  let key = reflect @k Proxy
  parity <- [ParityEven, ParityOdd]
  j <- key.spins
  let
    sym = SymmetrySector j parity
    label = symSectorLabel sym
  (delta, range) <- listDeltas sym key.spectrum
  let ope = opeTT (ConformalRep delta j) parity
  maybeToList $ withNonEmptyVec ope $
    Bounds.constraint range label . crossingMatTTTT3d

-------------------- SDP Definition ---------------------

withNonEmptyVec :: [a] -> (forall n . KnownNat n => V n a -> r) -> Maybe r
withNonEmptyVec [] _  = Nothing
withNonEmptyVec xs go = Just $ reifyVectorNat (Vector.fromList xs) go

tttt3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m, BlockFetchContext TTTTBlock a m)
  => TTTT3d
  -> SDPB.SDP m a
tttt3dSDP key = Bounds.runTagged key $
  case key.objective of
    Feasibility mLambdaTTT ->
      Bounds.sdp (zero `asTypeOf` unit) unit (stressConstraint : bulk)
      where
        stressConstraint = case mLambdaTTT of
          Just lambdaTTT -> Bounds.constConstraint "ext" (Bounds.pair lambdaTTT ext)
          Nothing        -> Bounds.constConstraint "ext" ext

    StressTensorOPEBound lambdaTTT direction ->
      Bounds.sdp unit matLambdaTTT bulk
      where
        matLambdaTTT = boundDirSign direction *^ Bounds.pair lambdaTTT ext

    TTeOPEBound deltaEven direction ->
      Bounds.sdp unit matLambdaTTe (stressConstraint : bulk)
      where
        stressConstraint = Bounds.constConstraint "ext" ext
        matLambdaTTe = boundDirSign direction *^ matTTeven deltaEven

    TTeOPEBoundWithLambda deltaEven direction lambdaTTT ->
      Bounds.sdp unit matLambdaTTe (stressConstraint : bulk)
      where
        stressConstraint = Bounds.constConstraint "ext" (Bounds.pair lambdaTTT ext)
        matLambdaTTe = boundDirSign direction *^ matTTeven deltaEven

    TTsOPEBound deltaOdd direction ->
      Bounds.sdp unit matLambdaTTs (stressConstraint : bulk)
      where
        stressConstraint = Bounds.constConstraint "ext" ext
        matLambdaTTs = boundDirSign direction *^ matTTodd deltaOdd

    FeasibilityFixedOPE lambdaTTT ->
      Bounds.sdp (zero `asTypeOf` unit) stressPlusId bulk
      where
        stressPlusId =
          mapTensor (mapBlocks Left) (Bounds.pair lambdaTTT ext) ^+^
          mapTensor (mapBlocks Right) unit

    FeasibilityLowerBoundOPE lambdaTTT ->
      Bounds.sdp (zero `asTypeOf` unit) stressPlusId (stressConstraint : bulk)
      where
        stress = mapTensor (mapBlocks Left) $ Bounds.pair lambdaTTT ext
        stressPlusId = stress ^+^ mapTensor (mapBlocks Right) unit
        stressConstraint = Bounds.constConstraint "ext" ext

    TTTTNavigatorBound mLambdaTTT ->
      Bounds.sdp unit ((-1) *^ gff) (stressConstraint : bulk)
      where
        stressConstraint = case mLambdaTTT of
          Just lambdaTTT -> Bounds.constConstraint "ext" (Bounds.pair lambdaTTT ext)
          Nothing        -> Bounds.constConstraint "ext" ext

instance ToSDP TTTT3d where
  type SDPFetchKeys TTTT3d = '[ CompositeBlockKey TTOStructLabel ]
  toSDP = tttt3dSDP

instance SDPFetchBuildConfig TTTT3d where
  sdpFetchConfig _ _ cftBoundFiles =
    readCompositeBlockTable cftBoundFiles.blockDir :&:
    FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $
    (compositeBlockBuildLink cftBoundFiles.blockDir) :<
    (noDeps $ block3dBuildLink bConfig cftBoundFiles)

instance Static (Binary TTTT3d)              where closureDict = static Dict
instance Static (Show TTTT3d)                where closureDict = static Dict
instance Static (ToSDP TTTT3d)               where closureDict = static Dict
instance Static (ToJSON TTTT3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TTTT3d) where closureDict = static Dict

----------- Block files -------------

ttttBlockDir :: FilePath
ttttBlockDir = Config.config.dataDir </> "tttt_blocks"

-- | Ensure tttt blocks are placed in a central location by defining a
-- custom ToPath instance
instance ToPath (CompositeBlockKey TTOStructLabel) where
  toPath _ k =
    ttttBlockDir
    </> paramDir "nmax"          k.params.nmax
    </> paramDir "order"         k.params.order
    </> paramDir "keptPoleOrder" k.params.keptPoleOrder
    </> paramDir "precision"     k.params.precision
    </> dirScatteredHashBasePath "tttt_block" k <.> "dat"
    where
      paramDir label i = label <> "_" <> show i

instance Static (ToPath (CompositeBlockKey TTOStructLabel)) where
  closureDict = static Dict
