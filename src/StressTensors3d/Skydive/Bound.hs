{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module StressTensors3d.Skydive.Bound
  ( skydiveBoundLoop
  , SkydiveState(..)
  , Clock(..)
  , defaultExactHessianClock
  ) where

import Bootstrap.Math.Linear              (V)
import Control.Monad.IO.Class             (liftIO)
import Data.Aeson                         (FromJSON, ToJSON)
import Data.Binary                        (Binary)
import Data.List.NonEmpty                 (NonEmpty (..))
import Data.Scientific                    (toRealFloat)
import Data.Time.Clock                    (NominalDiffTime)
import GHC.Generics                       (Generic)
import GHC.TypeNats                       (KnownNat)
import Hyperion                           (Job)
import Hyperion.Bootstrap.Bound           (Bound (..), BoundFiles, Precision)
import Hyperion.Bootstrap.Bound           qualified as Bound
import Hyperion.Database                  qualified as DB
import Hyperion.Log                       qualified as Log
import SDPB                               qualified as SDPB
import StressTensors3d.Scheduler.TSigEps  (CanBuildBound, buildBounds,
                                           pmp2sdpBound)
import StressTensors3d.Skydive.Config     (SkydiveConfig (..))
import StressTensors3d.Skydive.Output     (SkydiveOutput (..))
import StressTensors3d.Skydive.ParamShift (allParamShifts, applyParamShift,
                                           paramShiftString)
import StressTensors3d.Skydive.Run        (SkydiveFiles (..), runSkydive)
import System.Directory                   (createDirectoryIfMissing,
                                           removePathForcibly)
import System.FilePath.Posix              ((</>))
import Type.Reflection                    (Typeable)

-- | Build an SDP at 'centralPoint', and other SDPs arranged around
-- 'centralPoint' by a distance 'stepSize'. Run skydive on the
-- group of SDPs.
runSkydiveBound
  :: (RealFloat a, Show a, KnownNat n, CanBuildBound p b)
  => FilePath
  -> NominalDiffTime
  -> [FilePath]
  -> BoundFiles
  -> SkydiveConfig n a
  -> V n a
  -> (V n a -> Bound (Precision p) b)
  -> Job (SkydiveOutput n a, FilePath)
runSkydiveBound skydiveScriptName reportInterval statsFiles boundFiles skydiveConfig centralPoint mkBound = do
  -- | Delete the sdpDir. We may want to re-use blocks and json files
  -- from previous computations, so we leave those alone.
  liftIO $ removePathForcibly boundFiles.sdpDir
  liftIO $ createDirectoryIfMissing True boundFiles.sdpDir
  newStatsFile <- buildBounds reportInterval statsFiles shiftedBounds
  mapM_ (uncurry pmp2sdpBound) shiftedBounds
  result <- liftIO $ runSkydive skydiveExecutable skydiveFiles sdpbParams skydiveConfig
  pure (result, newStatsFile)
  where
    stepsize = toRealFloat skydiveConfig.externalParamInfinitestimal
    maybeShifts = Nothing :| map Just (allParamShifts skydiveConfig.useExactHessian)

    newSdpDir Nothing      = boundFiles.sdpDir </> "zero"
    newSdpDir (Just shift) = boundFiles.sdpDir </> paramShiftString shift

    shiftedBounds = do
      maybeShift <- maybeShifts
      pure ( mkBound (applyParamShift stepsize maybeShift centralPoint)
           , boundFiles { Bound.sdpDir = newSdpDir maybeShift }
           )

    skydiveExecutable = (mkBound centralPoint).boundConfig.scriptsDir </> skydiveScriptName
    sdpbParams = (mkBound centralPoint).solverParams
    skydiveFiles = MkSkydiveFiles
      { sdpDir               = newSdpDir Nothing
      , newSdpDirs           = map (newSdpDir . Just)
                               (allParamShifts skydiveConfig.useExactHessian)
      , outDir               = boundFiles.outDir
      , checkpointDir        = boundFiles.checkpointDir
      , initialCheckpointDir = boundFiles.initialCheckpointDir
      }

srunSkydiveScriptName :: FilePath
srunSkydiveScriptName = "srun_dynamical_sdp.sh"

data SkydiveState n a = MkSkydiveState
  { statFiles         :: [FilePath]
  , boundFiles        :: BoundFiles
  , skydiveConfig     :: SkydiveConfig n a
  , externalParams    :: V n a
  , exactHessianClock :: Clock
  } deriving (Show, Generic, Binary, ToJSON, FromJSON)

data Clock = MkClock
  { period :: Int
  , state  :: Int
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

tickClock :: Clock -> Clock
tickClock (MkClock period i) = MkClock period (i + 1 `mod` period)

clockAtZero :: Clock -> Bool
clockAtZero (MkClock _ i) = i == 0

defaultExactHessianClock :: Clock
defaultExactHessianClock = MkClock 10 1

skydiveBoundLoop
  :: (KnownNat n, RealFloat a, Show a, ToJSON a, FromJSON a, Typeable a, CanBuildBound p b)
  => NominalDiffTime
  -> SkydiveState n a
  -> (V n a -> Bound (Precision p) b)
  -> Int
  -> Job (V n a)
skydiveBoundLoop reportInterval initialState mkBound numIters =
  DB.lookup skydiveStateMap dbKey >>= \case
  Nothing    -> go numIters initialState
  Just state -> go numIters state
  where
    -- Use the initial central bound as the database key.
    dbKey = mkBound initialState.externalParams
    
    skydiveStateMap :: DB.KeyValMap (Bound (Precision p) b) (SkydiveState n a)
    skydiveStateMap = DB.KeyValMap "SkydiveState"

    go 0 state = pure state.externalParams
    go n state = do
      (skydiveOut, newStatsFile) <-
        runSkydiveBound
        srunSkydiveScriptName
        reportInterval
        state.statFiles
        state.boundFiles
        state.skydiveConfig
        state.externalParams
        mkBound
      let
        newState = state
          { skydiveConfig = state.skydiveConfig
            { totalIterationCount = skydiveOut.totalIterations
            , prevExternalStep    = Just skydiveOut.externalParamStep
            , prevGradient        = Just skydiveOut.gradientBFGS
            , prevHessian         = Just skydiveOut.hessianBFGS
            , useExactHessian     = clockAtZero state.exactHessianClock
            }
          , externalParams    = state.externalParams + skydiveOut.externalParamStep
          , statFiles         = [newStatsFile]
          , boundFiles        = state.boundFiles { Bound.initialCheckpointDir = Nothing }
          , exactHessianClock = tickClock state.exactHessianClock
          }
      Log.info "skydiveBoundLoop: Computed" skydiveOut
      Log.info "skydiveBoundLoop: New stats file" newStatsFile
      Log.info "skydiveBoundLoop: New point" newState.externalParams
      DB.insert skydiveStateMap dbKey newState
      if SDPB.terminateReason skydiveOut.sdpbOutput == SDPB.UpdateSDPs
        then go (n-1) newState
        else pure state.externalParams

