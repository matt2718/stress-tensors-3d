{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Skydive.Config
  ( SkydiveConfig (..)
  , defaultSkydiveConfig
  ) where

import Bootstrap.Math.Linear (V)
import Data.Aeson            (FromJSON, ToJSON)
import Data.Binary           (Binary)
import Data.Matrix.Static    (Matrix)
import Data.Scientific       (Scientific)
import GHC.Generics          (Generic)
import GHC.TypeNats          (KnownNat)

data SkydiveConfig n a = SkydiveConfig
  { externalParamInfinitestimal :: Scientific
  , centeringRThreshold         :: Scientific
  , dualityGapUpperLimit        :: Scientific
  , betaScanMin                 :: Scientific
  , betaScanMax                 :: Scientific
  , betaScanStep                :: Scientific
  , stepMinThreshold            :: Scientific
  , stepMaxThreshold            :: Scientific
  , primalDualObjWeight         :: Scientific
  , maxClimbingSteps            :: Int
  , betaClimbing                :: Scientific
  , navigatorWithLogDetX        :: Bool
  , gradientWithLogDetX         :: Bool
  , finiteDualityGapTarget      :: Scientific
  , findBoundaryDirection       :: Maybe (V n a)
  , findBoundaryObjThreshold    :: Maybe Scientific
  , useExactHessian             :: Bool
  , prevExternalStep            :: Maybe (V n a)
  , prevGradient                :: Maybe (V n a)
  , prevHessian                 :: Maybe (Matrix n n a)
  , totalIterationCount         :: Int
  , boundingBoxMax              :: V n a
  , boundingBoxMin              :: V n a
  , bfgsPartialUpdate           :: Int
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

defaultSkydiveConfig :: (KnownNat n, Num a) => SkydiveConfig n a
defaultSkydiveConfig = SkydiveConfig
  { externalParamInfinitestimal = 1e-20
  , centeringRThreshold         = 1e-20
  , dualityGapUpperLimit        = 0.1
  , betaScanMin                 = 0.1     --Same as sdpb-skydiving default input
  , betaScanMax                 = 1.01    --Same as sdpb-skydiving default input
  , betaScanStep                = 0.1     --Same as sdpb-skydiving default input
  , stepMinThreshold            = 0.1     --Same as sdpb-skydiving default input
  , stepMaxThreshold            = 0.6     --Same as sdpb-skydiving default input
  , primalDualObjWeight         = 0.2     --Same as sdpb-skydiving default input
  , maxClimbingSteps            = 1       --Same as sdpb-skydiving default input
  , betaClimbing                = 2       --Same as sdpb-skydiving default input
  , navigatorWithLogDetX        = False   --Same as sdpb-skydiving default input
  , gradientWithLogDetX         = True    --Same as sdpb-skydiving default input
  , finiteDualityGapTarget      = 0       --Same as sdpb-skydiving default input
  , findBoundaryDirection       = Nothing
  , findBoundaryObjThreshold    = Nothing
  , useExactHessian             = False
  , prevExternalStep            = Nothing
  , prevGradient                = Nothing
  , prevHessian                 = Nothing
  , totalIterationCount         = 0
  , boundingBoxMax              = pure 1
  , boundingBoxMin              = pure 1
  , bfgsPartialUpdate           = -1      --Same as sdpb-skydiving default input
  }
