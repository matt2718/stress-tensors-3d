{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}

module StressTensors3d.Skydive.ParamShift
  ( ParamShift(..)
  , applyParamShift
  , paramShiftString
  , allParamShifts
  ) where

import Bootstrap.Math.Linear      (V)
import Bootstrap.Math.Linear      qualified as L
import Bootstrap.Math.VectorSpace ((*^))
import Data.Aeson                 (FromJSON, ToJSON)
import Data.Binary                (Binary)
import Data.Proxy                 (Proxy (..))
import GHC.Generics               (Generic)
import GHC.TypeNats               (KnownNat, natVal)
import Linear.Matrix              qualified as L

data ParamShift n = Plus Int | Minus Int | Sum Int Int
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

paramShiftString :: ParamShift n -> String
paramShiftString = \case
  Plus i  -> "plus_" ++ show i
  Minus i -> "minus_" ++ show i
  Sum i j -> "sum_" ++ show i ++ "_" ++ show j

allParamShifts :: forall n . KnownNat n => Bool -> [ParamShift n]
allParamShifts useExactHess =
  [Plus i | i <- [0 .. n'-1]] ++
  case useExactHess of
    False -> []
    True -> [Minus i | i <- [0 .. n'-1]] ++
            [Sum i j | i <- [0 .. n'-1], j <- [i+1 .. n'-1]]
  where
    n' = fromIntegral (natVal @n Proxy)

paramShiftToVec :: (KnownNat n, Num a) => ParamShift n -> V n a
paramShiftToVec = \case
  Plus  i -> e i
  Minus i -> -e i
  Sum i j -> e i + e j
  where
    e i = L.identity L.! i

applyParamShift :: (KnownNat n, Eq a, Num a) => a -> Maybe (ParamShift n) -> V n a -> V n a
applyParamShift stepSize shift v = (stepSize *^ maybe 0 paramShiftToVec shift) + v
