{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeFamilies          #-}

module StressTensors3d.Skydive.Run
  ( runSkydive
  , SkydiveFiles(..)
  ) where

import Data.Aeson                     (FromJSON, ToJSON)
import Data.Binary                    (Binary)
import Data.Foldable                  qualified as Foldable
import Data.Maybe                     (fromJust)
import Data.Proxy                     (Proxy (..))
import GHC.Generics                   (Generic)
import GHC.TypeNats                   (KnownNat, natVal)
import SDPB                           (Params (..))
import SDPB                           qualified as SDPB
import StressTensors3d.Skydive.Config (SkydiveConfig (..))
import StressTensors3d.Skydive.Output (SkydiveOutput (..), readSkydiveOutput)
import System.Process                 (callProcess)

data SkydiveFiles = MkSkydiveFiles
  { sdpDir               :: FilePath
  , newSdpDirs           :: [FilePath]
  , outDir               :: FilePath
  , checkpointDir        :: FilePath
  , initialCheckpointDir :: Maybe FilePath
  } deriving (Show, Generic, Binary, ToJSON, FromJSON)

getSkydiveArgs
  :: forall n a . (KnownNat n, Show a)
  => SkydiveFiles
  -> SDPB.Params
  -> SkydiveConfig n a
  -> IO [String]
getSkydiveArgs skydiveFiles sdpbParams cfg = do
  paramArgs <- SDPB.getParamArgs sdpbParams
  newSdpDirsNsv <- SDPB.makeNsvFile skydiveFiles.newSdpDirs
  pure $
    paramArgs ++
    [ "--sdpDir"                     , skydiveFiles.sdpDir
    , "--outDir"                     , skydiveFiles.outDir
    , "--checkpointDir"              , skydiveFiles.checkpointDir
    , "--newSdpDirs"                 , newSdpDirsNsv
    , "--numExternalParams"          , show (natVal @n Proxy)
    , "--externalParamInfinitestimal", show cfg.externalParamInfinitestimal
    , "--centeringRThreshold"        , show cfg.centeringRThreshold
    , "--dualityGapUpperLimit"       , show cfg.dualityGapUpperLimit
    , "--betaScanMin"                , show cfg.betaScanMin
    , "--betaScanMax"                , show cfg.betaScanMax
    , "--betaScanStep"               , show cfg.betaScanStep
    , "--stepMinThreshold"           , show cfg.stepMinThreshold
    , "--stepMaxThreshold"           , show cfg.stepMaxThreshold
    , "--primalDualObjWeight"        , show cfg.primalDualObjWeight
    , "--maxClimbingSteps"           , show cfg.maxClimbingSteps
    , "--betaClimbing"               , show cfg.betaClimbing
    , "--navigatorWithLogDetX"       , show cfg.navigatorWithLogDetX
    , "--gradientWithLogDetX"        , show cfg.gradientWithLogDetX
    , "--finiteDualityGapTarget"     , show cfg.finiteDualityGapTarget
    , "--totalIterationCount"        , show cfg.totalIterationCount
    , "--BFGSPartialUpdate"          , show cfg.bfgsPartialUpdate
    ]
    ++
    [ "--boundingBoxMax"]  ++ [show x | x <- Foldable.toList cfg.boundingBoxMax]
    ++
    [ "--boundingBoxMin"]  ++ [show x | x <- Foldable.toList cfg.boundingBoxMin]
    ++
    case (cfg.findBoundaryDirection, cfg.findBoundaryObjThreshold) of
      (Just v, Just thresh)  -> [ "--findBoundaryObjThreshold", show thresh] ++
                                [ "--findBoundaryDirection"]  ++ [ show x | x <- Foldable.toList v]
      (Just v, Nothing)      -> [ "--findBoundaryDirection"]  ++ [ show x | x <- Foldable.toList v]
      (Nothing,_) -> []
    ++
    [ "--useExactHessian", show cfg.useExactHessian]
    ++
    case (cfg.useExactHessian, cfg.prevExternalStep, cfg.prevHessian) of
      (True, _, _)              -> []
      (False, Nothing, Nothing) -> []
      (False, Nothing, Just mt) ->
        [ "--prevHessianBFGS"] ++ [show x | x <- Foldable.toList mt]
      (False, Just prevExtStep, _) ->
        [ "--prevExternalStep"] ++ [show x | x <- Foldable.toList prevExtStep] ++
        [ "--prevGradientBFGS"] ++ [show x | x <- Foldable.toList (fromJust cfg.prevGradient)] ++
        [ "--prevHessianBFGS"]  ++ [show x | x <- Foldable.toList (fromJust cfg.prevHessian)]
    ++
    case skydiveFiles.initialCheckpointDir of
      Just dir -> ["--initialCheckpointDir", dir]
      Nothing  -> []

runSkydive
  :: (KnownNat n, RealFloat a, Show a)
  => FilePath
  -> SkydiveFiles
  -> SDPB.Params
  -> SkydiveConfig n a
  -> IO (SkydiveOutput n a)
runSkydive skydiveExecutable input sdpbParams skydiveConfig = do
  args <- getSkydiveArgs input sdpbParams skydiveConfig
  callProcess skydiveExecutable args
  readSkydiveOutput input.outDir
