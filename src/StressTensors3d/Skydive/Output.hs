{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module StressTensors3d.Skydive.Output
  ( SkydiveOutput(..)
  , SkydiveSolverData(..)
  , readSkydiveOutput
  ) where

import Bootstrap.Math.Linear (V)
import Bootstrap.Math.Linear qualified as L
import Control.Applicative   ((<|>))
import Data.Aeson            (FromJSON, ToJSON)
import Data.Attoparsec.Text  (Parser, decimal, double, scientific, skipSpace)
import Data.Binary           (Binary)
import Data.Matrix.Static    (Matrix)
import Data.Matrix.Static    qualified as M
import Data.Scientific       (Scientific)
import Data.Text             (Text)
import Data.Time.Clock       (NominalDiffTime)
import Data.Vector           qualified as V
import GHC.Generics          (Generic)
import GHC.TypeNats          (KnownNat)
import Hyperion              (Dict (..), Static (..), cAp)
import Numeric.Rounded       (Rounded, RoundingMode (..))
import SDPB                  qualified as SDPB
import System.FilePath.Posix ((</>))

data SkydiveSolverData = MkSkydiveSolverData
  { dualStepSize       :: Scientific
  , primalStepSize     :: Scientific
  , bfgsHessianUpdated :: Scientific
  , navigatorValue     :: Scientific
  , findMinimumQ       :: Scientific
  , beta               :: Scientific
  , mulogdetX          :: Scientific
  , climbedQ           :: Scientific
  , runtime            :: NominalDiffTime
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

data SkydiveOutput n a = SkydiveOutput
  { sdpbOutput        :: SDPB.Output
  , skydiveSolverData :: SkydiveSolverData
  , externalParamStep :: V n a
  , totalIterations   :: Int
  , gradientBFGS      :: V n a
  , hessianBFGS       :: Matrix n n a
  , hessianExact      :: Matrix n n a
  } deriving (Show, Generic, Binary, ToJSON, FromJSON, Functor)

instance (KnownNat p, KnownNat n) => Static (Binary (SkydiveOutput n (Rounded 'TowardZero p))) where
  closureDict = static (\Dict -> Dict) `cAp` closureDict @(KnownNat p, KnownNat n)

readTotalIterations :: (Integral a) => FilePath -> IO a
readTotalIterations outDir = SDPB.readParse decimal (outDir </> "iterations.txt")

readExtParamStep :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (V n a)
readExtParamStep outDir =
  L.fromRawVector <$>
  SDPB.readParse SDPB.vectorParser (outDir </> "externalParamStep.txt")

readGradientBFGS :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (V n a)
readGradientBFGS outDir =
  L.fromRawVector <$>
  SDPB.readParse SDPB.vectorParser (outDir </> "gradient.txt")

readHessianBFGS :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (Matrix n n a)
readHessianBFGS outDir =
  M.fromListUnsafe <$> (V.toList
  <$> SDPB.readParse SDPB.vectorParser (outDir </> "hessBFGS.txt"))

readHessian :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (Matrix n n a)
readHessian outDir =
  M.fromListUnsafe <$> (V.toList
  <$> SDPB.readParse SDPB.vectorParser (outDir </> "hessExact.txt"))

readSkydiveOutFile :: FilePath -> IO SkydiveSolverData
readSkydiveOutFile outDir = SDPB.readParse skydiveSolverDataParser (outDir </> "skydiving_out.txt")

skydiveSolverDataParser :: Parser SkydiveSolverData
skydiveSolverDataParser = MkSkydiveSolverData
  <$> val "dualStepSize"       scientific
  <*> val "primalStepSize"     scientific
  <*> val "BFGSHessianUpdated" scientific
  <*> val "NavigatorValue"     scientific
  <*> val "findMinimumQ"       scientific
  <*> val "beta"               scientific
  <*> val "mulogdetX"          scientific
  <*> val "climbedQ"           scientific
  <*> val ("runtime" <|> "Solver runtime") (fmap realToFrac double)
  where
    val :: Parser Text -> Parser a -> Parser a
    val k v = skipSpace >> k >> skipSpace >> "=" >> skipSpace >> v <* ";"

readSkydiveOutput :: (KnownNat n, RealFloat a) => FilePath -> IO (SkydiveOutput n a)
readSkydiveOutput outDir = SkydiveOutput
  <$> SDPB.readOutFile outDir
  <*> readSkydiveOutFile outDir
  <*> readExtParamStep outDir
  <*> readTotalIterations outDir
  <*> readGradientBFGS outDir
  <*> readHessianBFGS outDir
  <*> readHessian outDir
