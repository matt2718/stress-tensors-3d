{-# OPTIONS_GHC -fno-warn-orphans #-}

module StressTensors3d.Test.MemoryTest where

import Blocks.Block                   (Block (..))
import Blocks.Blocks3d.CompositeBlock (CompositeBlockKey (..),
                                       writeCompositeBlockTable)
import Blocks.Blocks3d.Get            (ConformalRep (..), Q4Struct (..))
import Blocks.Blocks3d.ThreePtStruct  (ThreePtStruct (..))
import Blocks.Coordinate              (Coordinate (..))
import Blocks.Sign                    (Sign (..))
import Hyperion                       (Dict (..))
import Hyperion.Bootstrap.Params      qualified as Params
import Hyperion.Util.ToPath           (ToPath (..), hashBasePath)
import StressTensors3d.TTOStructLabel (TTOStructLabel (..))
import System.FilePath.Posix          ((<.>), (</>))

instance ToPath (CompositeBlockKey TTOStructLabel) where
  toPath _ k =
    "/home/rse23/project/stress-tensors-3d/blocks" </> hashBasePath "tttt_block" k <.> "dat"

memTest :: IO ()
memTest = do
  writeCompositeBlockTable Dict "/home/rse23/project/stress-tensors-3d/blocks/" pcbk
  where
    stressRep = ConformalRep 3 2
    exchangeRep = ConformalRep () 8
    struct3pt = ThreePtStruct
      { rep1 = stressRep
      , rep2 = stressRep
      , rep3 = exchangeRep
      , label = GenericParityEven1 8
      }
    struct4pt = Q4Struct
      { q4qs = (2,2,2,2)
      , q4Sign = Plus
      }
    block' = Block
      { struct12 = struct3pt
      , struct43 = struct3pt
      , fourPtFunctional = struct4pt
      }
    pcbk = CompositeBlockKey
      { block = block'
      , coordinate = XT
      , params = Params.block3dParamsNmax 14
      }
