{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}

module StressTensors3d.Test.PartChunkWriting where

import Blocks.Blocks3d                          qualified as B3d
import Bootstrap.Build                          (ComputeDependencies)
import GHC.TypeNats                             (KnownNat)
import Hyperion                                 (Dict (..))
import Hyperion.Bootstrap.Bound                 (Bound (..), BoundFiles (..),
                                                 BoundKeyVals, Precision (..),
                                                 ToSDP (..))
import Hyperion.Bootstrap.Bound                 qualified as Bound
import Hyperion.Bootstrap.Params                qualified as Params
import SDPB                                     (BigFloat, Label (..),
                                                 PartChunk, PartType (..),
                                                 SDPPart (..))
import SDPB qualified
import StressTensors3d.Programs.Defaults        (defaultBoundConfig)
import StressTensors3d.Programs.TSigEps2023     (tSigEpsFeasibleDefaultGaps)
import StressTensors3d.Programs.TSigEps2023Data (disallowedPointsNmax10)
import StressTensors3d.TSigEps3d                (TSigEps3d (..))
import Linear.V (V)

myBound' :: V 2 Rational -> V 5 Rational -> Bound (Precision 768) TSigEps3d
myBound' dExt lambda = Bound
  { boundKey = (tSigEpsFeasibleDefaultGaps dExt (Just lambda) paramNmax)
    { blockParams  = (Params.block3dParamsNmax nmax)
      {  -- turn off pole shifting by setting keptPoleOrder = order
        B3d.keptPoleOrder = order
      , B3d.order         = order
      , B3d.precision     = 1024
      }
    , numPoles     = Just nPoles
    , spins        = Params.gnyspinsNmax paramNmax
    }
  , precision = MkPrecision -- (Params.block3dParamsNmax nmax).precision
  , solverParams = (Params.jumpFindingParams nmax)
    { SDPB.precision           = 1024
    , SDPB.dualityGapThreshold = 1e-30
    }
  , boundConfig = defaultBoundConfig
  }
  where
    nmax      = 6
    order     = 80
    nPoles    = 40
    paramNmax = nmax + 4

dExtNmax10 :: V 2 Rational
lambdaNmax10 :: V 5 Rational
(dExtNmax10, lambdaNmax10) = case disallowedPointsNmax10 of
  (a,b) : _ -> (a,b)
  _ -> error "Impossible"

myBound :: Bound (Precision 768) TSigEps3d
myBound = myBound' dExtNmax10 lambdaNmax10

-- | To run this test, you have to run the program
-- "TSigEps_test_nmax6_sample_points" to compute all the relevant
-- files, and then copy the work directory name here. Otherwise the
-- program will crash because it will be unable to find the necessary
-- blocks.
myBoundFiles :: BoundFiles
myBoundFiles =
  Bound.defaultBoundFiles "/expanse/lustre/scratch/dsd/temp_project/data/2024-06/CGjWG/Object__xhRHbEESxvukYqmqXWs-iz02iDet0XbAqhAYUGIzr8"

-- | This is the most expensive PartChunk to compute, taking about 22
-- seconds.
myPartChunk :: PartChunk
myPartChunk = SDPPart
  { ty = SDPPositiveConstraint 28
  , label = MkLabel
    { name = "continuum_Z2Even_ParityEven_50"
    , hash = "dm-6dFwLRTEIU4BLYsKztLNCjy9sjKUfuJx5oGcGfDM"
    }
  , chunkIndex = 2
  }

writeMyPartChunk :: IO ()
writeMyPartChunk = do
  --Log.info "Number of part chunks" (length myPartChunks)
  Bound.writePartChunk Dict myBound myBoundFiles myPartChunk

boundToSDPDeps
  :: (ToSDP b, KnownNat p)
  => Bound (Precision p) b
  -> SDPB.SDP (ComputeDependencies (BoundKeyVals b p)) (BigFloat p)
boundToSDPDeps = toSDP . boundKey

myPartChunks :: [PartChunk]
myPartChunks = SDPB.parts sdp >>= SDPB.partDependencies sdp
  where
    sdp = boundToSDPDeps myBound
