{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DefaultSignatures    #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE DerivingStrategies   #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.Test.BuildCrossingMat  where

import Bootstrap.Build.Fetches   (FetchConfig (..), Fetches, fetch)
import Bootstrap.Build.HasForce  (HasForce (..))
import Bootstrap.Build.MemoFetch (runMemoFetchT)
import Control.DeepSeq           (NFData)
import Data.Map.Strict           (Map)
import Data.Map.Strict           qualified as Map
import Data.Matrix               (Matrix)
import Data.Matrix               qualified as Matrix
import Data.Vector               (Vector)
import Data.Vector               qualified as Vector
import Debug.Trace               qualified as Debug
import Hyperion.Log              qualified as Log

newtype Block = MkBlock Integer
  deriving (Eq, Ord, Show)

newtype BlockKey = MkBlockKey Bool
  deriving (Eq, Ord, Show)

newtype BlockTable = MkBlockTable (Map Integer (Vector Integer))
  deriving (Eq, Ord, Show)

newtype BlockVector = MkBlockVector (Vector Integer)
  deriving (Eq, Ord, Show)
  deriving newtype NFData

blockToBlockKey :: Block -> BlockKey
blockToBlockKey (MkBlock i) = MkBlockKey (even i)

lookupBlockVector :: Block -> BlockTable -> BlockVector
lookupBlockVector (MkBlock i) (MkBlockTable t) = MkBlockVector (t Map.! i)

getBlockVector
  :: (Fetches BlockKey BlockTable f, Applicative f, HasForce f)
  => Block
  -> f BlockVector
getBlockVector block = forceM $
  lookupBlockVector block <$>
  fetch (blockToBlockKey block)

getCrossingMat
  :: (Fetches BlockKey BlockTable f, Applicative f, HasForce f)
  => Matrix Block
  -> f (Matrix BlockVector)
getCrossingMat mat = traverse getBlockVector mat

factorial :: Integer -> Integer
factorial 0 = 1
factorial i = i * factorial (i-1)

readBlockTable :: BlockKey -> IO BlockTable
readBlockTable key = do
  Log.info "Reading block table" key
  pure . MkBlockTable . Map.fromList $ case key of
    MkBlockKey False -> Debug.trace "BlockTable False" $ do
      i <- [1,3 .. 11]
      pure $ (i,  Vector.fromList [i, i + 5, Debug.trace ("BlockVector " <> show i) $ factorial (i)])
    MkBlockKey True -> Debug.trace "BlockTable True" $ do
      i <- [0,2 .. 10]
      pure $ (i,  Vector.fromList [i, i + 3, Debug.trace ("BlockVector " <> show i) $ factorial (i)])

myCrossingMat :: Matrix Block
myCrossingMat = fmap MkBlock $ Matrix.fromLists
  [ [ 0, 1, 2 ]
  , [ 4, 5, 4 ]
  , [ 4, 9, 1 ]
  ]

myFetchConfig :: FetchConfig IO '[ '(BlockKey, BlockTable ) ]
myFetchConfig = readBlockTable :&: FetchNil

buildCrossingMat'' :: Matrix Block -> IO (Matrix BlockVector)
buildCrossingMat'' mat = runMemoFetchT (getCrossingMat mat) myFetchConfig
