module Main where

import StressTensors3d.Test.OOMJob qualified
import StressTensors3d.Test.CompositeBlockMemory qualified
import StressTensors3d.Test.MemoryTest qualified
import StressTensors3d.Test.PartChunkWriting qualified
import StressTensors3d.Test.Scheduler qualified
import System.Environment                        (getArgs)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [ "memTest" ]                 -> StressTensors3d.Test.MemoryTest.memTest
    [ "writeMyCompositeBlock" ]   -> StressTensors3d.Test.CompositeBlockMemory.writeMyCompositeBlock
    [ "writeMyPartChunk" ]        -> StressTensors3d.Test.PartChunkWriting.writeMyPartChunk
    -- [ "testRunTasks" ]         -> StressTensors3d.Test.Scheduler.testRunTasks
    [ "testSimulateBuild" ]       -> StressTensors3d.Test.Scheduler.testSimulateBuild
    [ "testSimulateBuildNmax18" ] -> StressTensors3d.Test.Scheduler.testSimulateBuildNmax18
    [ "testCriticalPathNmax18" ]  -> StressTensors3d.Test.Scheduler.testCriticalPathNmax18
    _ -> StressTensors3d.Test.OOMJob.test -- To run, do: ./path/to/executable master -p "oomJob"
