#!/usr/bin/sh

# example long job
# srun --partition=shared --qos=shared-oneweek --account=yun124 --nodes=1 --ntasks-per-node=1 -c 1 --mem=8G -t 168:00:00 --pty /bin/sh
srun --partition=shared --account=yun124 --nodes=1 --ntasks-per-node=1 -c 1 --mem=8G -t 24:00:00 --pty /bin/sh
