-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion

flatSpaceFunctionalsDir :: FilePath
flatSpaceFunctionalsDir = "/central/groups/dssimmon/dsd/flat-space-functionals"

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "caltech-hpc-dsd" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/central/groups/dssimmon/dsd/hyperion3")
  { emailAddr = Just "dsd@caltech.edu" }

staticConfig :: HyperionStaticConfig
staticConfig = defaultHyperionStaticConfig