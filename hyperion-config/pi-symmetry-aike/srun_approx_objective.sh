#!/bin/bash

module load cmake/3.18.1 gcc/9 openmpi boost/1.68.0.a slurm/19.05.8

mpirun /home/aliu/install/bin/approx_objective $@
