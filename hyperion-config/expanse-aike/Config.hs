-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "expanse-aike2" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/expanse/lustre/scratch/aikeliu/temp_project")
  { emailAddr             = Just "aliu7@caltech.edu"
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "compute"
    , Slurm.account   = Just "yun124"
    }
  , maxSlurmJobs          = Just 64
  }

staticConfig :: HyperionStaticConfig
staticConfig = defaultHyperionStaticConfig { hostNameStrategy = GetHostEntriesExternal }

