#!/bin/bash

module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7 boost/1.74.0 slurm
ulimit -c unlimited
echo /home/vdommes/install/sdpb-spectrum-mpsolve/bin/pmp2sdp $@
/home/vdommes/install/sdpb-spectrum-mpsolve/bin/pmp2sdp $@
