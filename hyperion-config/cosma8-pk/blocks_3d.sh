#!/bin/bash

module restore bootstrap_modules
ulimit -c unlimited

# Chat GPT extracts the param_file argument:                                             
command_args=("$@")
# Initialize an empty variable for the parameter file
param_file=""
# Loop through all arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --param-file) # Check for the --param-file argument
            param_file="$2" # Assign the next argument as the value of param_file
            shift # Move past the argument value
            ;;
        *) # Ignore any other arguments
           # Optionally, you can add code here to handle other arguments
            ;;
    esac
    shift # Move to the next argument
done

ulimit -c unlimited

echo cat $param_file
cat $param_file
echo /usr/bin/time -v /cosma/apps/dp284/dc-krav1/installs/blocks_3d/build/blocks_3d "${command_args[@]}"
/usr/bin/time -v /cosma/apps/dp284/dc-krav1/installs/blocks_3d/build/blocks_3d "${command_args[@]}"

