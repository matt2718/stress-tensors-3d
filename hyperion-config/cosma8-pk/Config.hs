-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "cosma8-pk" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/cosma8/data/dp328/dc-krav1/hyperion_stress_tensors_3d")
  { defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "cosma8"
    , Slurm.account   = Just "dp328"
    }
  , emailAddr = Just "petr.kravchuk@kcl.ac.uk" }

staticConfig :: HyperionStaticConfig
staticConfig = defaultHyperionStaticConfig {
    commandTransport = SRun (Just ("srun", ["--overlap", "--nodes=1", "--ntasks=1", "--immediate"]))
  }
