#!/bin/bash

# This is a fake version of srun_sdpb.sh that uses a predetermined checkpoint the real one is empty

module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7 boost/1.74.0 slurm

# Initialize an array to hold the new arguments
new_args=()
checkpoint_dir=""

# 4 128
default_checkpoint="/expanse/lustre/scratch/mmitchell/temp_project/data/2024-10/tmTmh/Object_DVSd5dnh3hMcoxs-1iewN1kKQOeYJSpY1ZF2jnyrgIM/ck"
#default_checkpoint="/expanse/lustre/scratch/mmitchell/temp_project/data/saved_checkpoints/beeJn/Object_aPF9dfZMlYyyhuXlMZopdnB2-IYxOo7CURY_Rku1_-c/out"

# Loop over all arguments
while [[ "$#" -gt 0 ]]; do
  if [[ $1 == "--initialCheckpointDirRRRRR" ]]; then
    # Flag to remove the --initialCheckpointDir if the checkpoint directory exists
    myCpDir="$2"
    if [[ -d "$2" ]]; then
      echo 'checkpoint dir exists, leaving in place'
    else
      if [[ "$2" == *"CEbxV"* ]]; then
        myCpDir="$default_checkpoint"
      else
        echo 'checkpoint dir does not exist, but does not match code. leaving in place.'
      fi  
    fi
    new_args+=("--initialCheckpointDir")
    new_args+=("$myCpDir")
    shift 2  # Skip the --initialCheckpointDir and its value
  else
    # Add all other arguments to the new array
    new_args+=("$1")
    shift  # Move to the next argument
  fi
done

echo srun /home/vdommes/install/sdpb-spectrum-mpsolve/bin/sdpb "${new_args[@]}"
srun /home/vdommes/install/sdpb-spectrum-mpsolve/bin/sdpb "${new_args[@]}"
