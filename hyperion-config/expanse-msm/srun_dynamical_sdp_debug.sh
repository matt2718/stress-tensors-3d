#!/bin/bash

module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7 boost/1.74.0 slurm

#module purge
#module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/0.3.10-openmp ncurses slurm

echo srun -v /home/aikeliu/install/skydiving-bigint-syrk-blas/bin/dynamical_sdp_RV1B $@
srun -v /home/aikeliu/install/skydiving-bigint-syrk-blas/bin/dynamical_sdp_RV1B $@

#echo srun -v /home/vdommes/install/sdpb-skydiving-bigint-syrk-blas/bin/dynamical_sdp_RV1B $@
#srun -v /home/vdommes/install/sdpb-skydiving-bigint-syrk-blas/bin/dynamical_sdp_RV1B $@

#echo srun -v /home/vdommes/install/sdpb-skydiving-master/bin/dynamical_sdp_RV1B $@
#srun -v /home/vdommes/install/sdpb-skydiving-master/bin/dynamical_sdp_RV1B $@ 
