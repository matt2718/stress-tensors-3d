-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "expanse-msm" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/expanse/lustre/scratch/mmitchell/temp_project")
  { emailAddr             = Just "matthew.mitchell@yale.edu"
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "shared"
    , Slurm.account   = Just "yun124"
    }
  , maxSlurmJobs          = Just 64
  }

staticConfig :: HyperionStaticConfig
staticConfig = defaultHyperionStaticConfig { hostNameStrategy = GetHostEntriesExternal }

