# About 

This is an example haskell program for numerical conformal bootstrap
computations involving stress tensors in 3d.

Note: this is everyday working code, and it may change without notice.

# Usage

- Before compiling, you must create `hyperion-config/src/Config.hs`
  and prepare the appropriate scripts so that `stress-tensors-3d` can use
  `sdpb` and other programs on your machine.  The recommended way to
  do this is (here and below replace my-special-machine with some meaningful name):

      cp -r hyperion-config/caltech-hpc-dsd hyperion-config/my-special-machine
      # edit 'hyperion-config/my-special-machine/Config.hs' as necessary
      # edit the scripts in 'hyperion-config/my-special-machine' as necessary

- Run `configure.py` script (requires Python 3).

   For Caltech HPC:

      ./configure.py -c hyperion-config/my-special-machine
   
   For Expanse HPC:

      ./configure.py -c hyperion-config/my-special-machine --use-external-hostname true

   This script generates `stack.yaml` and creates a symlink `hyperion-config/src/Config.hs` pointing to `hyperion-config/my-special-machine/Config.hs`.
   In addition, it generates `build.sh` (see below). For more details, call `./configure.py --help`.

   **NB:** Do not edit `stack.yaml` manually! Edit contents of `configure.py` instead - see function `create_stack_yaml_text()`.

- After that, run

      ./build.sh

   to compile the project. This scripts runs `configure.py` to update `stack.yaml` file and then calls `stack build`.
   
   All parameters passed to the script, e.g. `./build.sh -j1`, are passed to the `stack build` command.

- For `hyperion` to work properly, your nodes should be able to `ssh` into
  other nodes without a password promt or any other interactive steps. This might
  require you to set up a key pair without a passphrase. Also see the [documentation
  for SSHCommand](https://davidsd.github.io/hyperion/hyperion/Hyperion-WorkerCpuPool.html#t:SSHCommand)
  for details on how `ssh` is called. You might need to change it in your `Config.hs`
  file (described above).
  
- To test, try

      ./hyp TSigEps_test_nmax6
  
  This will run one computation requiring 6 cores, testing feasibility
   of a point in dimension space (Delta_sigma, Delta_epsilon) for
  mixed correlators of sigma and epsilon, and T in the 3d Ising model.
  
  The output of `./hyp` is printed to a file `nohup.out`, and that includes a path
  to a more detailed log file which you can inspect to monitor the computation.
  
  The computation "TSigEps_test_nmax6" is defined in the file
  `StressTensors3d/Programs/TSigEps2023.hs`. 
  
  Results of the calculations are written to log files and a database (specified
  in `nohup.out`). For more details on how this happens, see the code of this
  repository and the libraries, as well as the haddock to `hyperion` (available
  as standalone [here](https://davidsd.github.io/hyperion)).
  
  For the general logic of cluster management, the `hyperion` library
  contains [examples](https://github.com/davidsd/hyperion/tree/master/example) which may be instructive.

# Installation notes

- If you want to use Delaunay search, you need the [qhull](http://www.qhull.org/download/) package. It's probably best to follow the instructions
	for installation using `cmake`.

- To compile the executable, run `build.sh`. Note that the login nodes for some clusters may not have enough memory to do the compilation. In this case, it's advised to run `build.sh -j1`
	on a compute node as necessary. For reference, Yale's Grace cluster took this command: `srun -N 1 -t 04:00:00 --mem=220G build.sh -j1`.

- One of the dependencies needs a libblas.o and liblapack.o. If this is causing compile problems and your cluster uses OpenBLAS or something similar, run something like
	`ln -s /path/to/lib/libopenblas.o /another/path/to/lib/libblas.o` and `ln -s /path/to/lib/libopenblas.o /another/path/to/lib/liblapack.o`,
	and add `/another/path/to/lib` to the extra-lib-dirs of `stack.yaml`.

# Other comments

- Configuration is compiled into the program for the following reason:
  Many copies of the executable are run on different machines over the
  course of a computation. If the configuration were read when the
  program started up, one could have different instances of the program
  with different configurations, all communicating with each other.
